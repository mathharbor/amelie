/* This script dynamically loads MathJax from either
 * the notebook server or the CDN
 * 
 * $.ajax and $.getScript don't work with the way MathJax
 * picks up it's own url
 */

(function() {
	var mathjax_config = "?config=TeX-AMS-MML_HTMLorMML";

	var mathjax_url;
	if(window.location.hostname.indexOf("localhost") > -1) {
		mathjax_url = "/data/mathjax-MathJax-07669ac/MathJax.js";
	} else {
		mathjax_url = "http://cdn.mathjax.org/mathjax/latest/MathJax.js";
	}
	
	var head = document.getElementsByTagName("head")[0];
	var config_script = document.createElement("script");
	config_script.type = "text/x-mathjax-config";
	config_script[(window.opera ? "innerHTML" : "text")] =
		"MathJax.Hub.Config({\n" +
		"  tex2jax: { inlineMath: [['$','$'], ['\\\\(','\\\\)']] }\n" +
		"});";
	head.appendChild(config_script);

	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src  = mathjax_url + mathjax_config;
	head.appendChild(script);
})();
// CodeMirror version 3.0
//
// CodeMirror is the only global var we claim
window.CodeMirror = (function() {
  "use strict";

  // BROWSER SNIFFING

  // Crude, but necessary to handle a number of hard-to-feature-detect
  // bugs and behavior differences.
  var gecko = /gecko\/\d/i.test(navigator.userAgent);
  var ie = /MSIE \d/.test(navigator.userAgent);
  var ie_lt8 = /MSIE [1-7]\b/.test(navigator.userAgent);
  var ie_lt9 = /MSIE [1-8]\b/.test(navigator.userAgent);
  var webkit = /WebKit\//.test(navigator.userAgent);
  var qtwebkit = webkit && /Qt\/\d+\.\d+/.test(navigator.userAgent);
  var chrome = /Chrome\//.test(navigator.userAgent);
  var opera = /Opera\//.test(navigator.userAgent);
  var safari = /Apple Computer/.test(navigator.vendor);
  var khtml = /KHTML\//.test(navigator.userAgent);
  var mac_geLion = /Mac OS X 1\d\D([7-9]|\d\d)\D/.test(navigator.userAgent);
  var mac_geMountainLion = /Mac OS X 1\d\D([8-9]|\d\d)\D/.test(navigator.userAgent);
  var phantom = /PhantomJS/.test(navigator.userAgent);

  var ios = /AppleWebKit/.test(navigator.userAgent) && /Mobile\/\w+/.test(navigator.userAgent);
  // This is woefully incomplete. Suggestions for alternative methods welcome.
  var mobile = ios || /Android|webOS|BlackBerry|Opera Mini|IEMobile/i.test(navigator.userAgent);
  var mac = ios || /Mac/.test(navigator.platform);

  // Optimize some code when these features are not used
  var sawReadOnlySpans = false, sawCollapsedSpans = false;

  // CONSTRUCTOR

  function CodeMirror(place, options) {
    if (!(this instanceof CodeMirror)) return new CodeMirror(place, options);
    
    this.options = options = options || {};
    // Determine effective options based on given values and defaults.
    for (var opt in defaults) if (!options.hasOwnProperty(opt) && defaults.hasOwnProperty(opt))
      options[opt] = defaults[opt];
    setGuttersForLineNumbers(options);

    var display = this.display = makeDisplay(place);
    display.wrapper.CodeMirror = this;
    updateGutters(this);
    if (options.autofocus && !mobile) focusInput(this);

    this.view = makeView(new BranchChunk([new LeafChunk([makeLine("", null, textHeight(display))])]));
    this.nextOpId = 0;
    loadMode(this);
    themeChanged(this);
    if (options.lineWrapping)
      this.display.wrapper.className += " CodeMirror-wrap";

    // Initialize the content.
    this.setValue(options.value || "");
    // Override magic textarea content restore that IE sometimes does
    // on our hidden textarea on reload
    if (ie) setTimeout(bind(resetInput, this, true), 20);
    this.view.history = makeHistory();

    registerEventHandlers(this);
    // IE throws unspecified error in certain cases, when
    // trying to access activeElement before onload
    var hasFocus; try { hasFocus = (document.activeElement == display.input); } catch(e) { }
    if (hasFocus || (options.autofocus && !mobile)) setTimeout(bind(onFocus, this), 20);
    else onBlur(this);

    operation(this, function() {
      for (var opt in optionHandlers)
        if (optionHandlers.propertyIsEnumerable(opt))
          optionHandlers[opt](this, options[opt], Init);
      for (var i = 0; i < initHooks.length; ++i) initHooks[i](this);
    })();
  }

  // DISPLAY CONSTRUCTOR

  function makeDisplay(place) {
    var d = {};
    var input = d.input = elt("textarea", null, null, "position: absolute; padding: 0; width: 1px; height: 1em; outline: none;");
    input.setAttribute("wrap", "off"); input.setAttribute("autocorrect", "off"); input.setAttribute("autocapitalize", "off");
    // Wraps and hides input textarea
    d.inputDiv = elt("div", [input], null, "overflow: hidden; position: relative; width: 3px; height: 0px;");
    // The actual fake scrollbars.
    d.scrollbarH = elt("div", [elt("div", null, null, "height: 1px")], "CodeMirror-hscrollbar");
    d.scrollbarV = elt("div", [elt("div", null, null, "width: 1px")], "CodeMirror-vscrollbar");
    d.scrollbarFiller = elt("div", null, "CodeMirror-scrollbar-filler");
    // DIVs containing the selection and the actual code
    d.lineDiv = elt("div");
    d.selectionDiv = elt("div", null, null, "position: relative; z-index: 1");
    // Blinky cursor, and element used to ensure cursor fits at the end of a line
    d.cursor = elt("pre", "\u00a0", "CodeMirror-cursor");
    // Secondary cursor, shown when on a 'jump' in bi-directional text
    d.otherCursor = elt("pre", "\u00a0", "CodeMirror-cursor CodeMirror-secondarycursor");
    // Used to measure text size
    d.measure = elt("div", null, "CodeMirror-measure");
    // Wraps everything that needs to exist inside the vertically-padded coordinate system
    d.lineSpace = elt("div", [d.measure, d.selectionDiv, d.lineDiv, d.cursor, d.otherCursor],
                         null, "position: relative; outline: none");
    // Moved around its parent to cover visible view
    d.mover = elt("div", [elt("div", [d.lineSpace], "CodeMirror-lines")], null, "position: relative");
    // Set to the height of the text, causes scrolling
    d.sizer = elt("div", [d.mover], "CodeMirror-sizer");
    // D is needed because behavior of elts with overflow: auto and padding is inconsistent across browsers
    d.heightForcer = elt("div", "\u00a0", null, "position: absolute; height: " + scrollerCutOff + "px");
    // Will contain the gutters, if any
    d.gutters = elt("div", null, "CodeMirror-gutters");
    d.lineGutter = null;
    // Helper element to properly size the gutter backgrounds
    var scrollerInner = elt("div", [d.sizer, d.heightForcer, d.gutters], null, "position: relative; min-height: 100%");
    // Provides scrolling
    d.scroller = elt("div", [scrollerInner], "CodeMirror-scroll");
    d.scroller.setAttribute("tabIndex", "-1");
    // The element in which the editor lives.
    d.wrapper = elt("div", [d.inputDiv, d.scrollbarH, d.scrollbarV,
                            d.scrollbarFiller, d.scroller], "CodeMirror");
    // Work around IE7 z-index bug
    if (ie_lt8) { d.gutters.style.zIndex = -1; d.scroller.style.paddingRight = 0; }
    if (place.appendChild) place.appendChild(d.wrapper); else place(d.wrapper);

    // Needed to hide big blue blinking cursor on Mobile Safari
    if (ios) input.style.width = "0px";
    if (!webkit) d.scroller.draggable = true;
    // Needed to handle Tab key in KHTML
    if (khtml) { d.inputDiv.style.height = "1px"; d.inputDiv.style.position = "absolute"; }
    // Need to set a minimum width to see the scrollbar on IE7 (but must not set it on IE8).
    else if (ie_lt8) d.scrollbarH.style.minWidth = d.scrollbarV.style.minWidth = "18px";

    // Current visible range (may be bigger than the view window).
    d.viewOffset = d.showingFrom = d.showingTo = d.lastSizeC = 0;

    // Used to only resize the line number gutter when necessary (when
    // the amount of lines crosses a boundary that makes its width change)
    d.lineNumWidth = d.lineNumInnerWidth = d.lineNumChars = null;
    // See readInput and resetInput
    d.prevInput = "";
    // Set to true when a non-horizontal-scrolling widget is added. As
    // an optimization, widget aligning is skipped when d is false.
    d.alignWidgets = false;
    // Flag that indicates whether we currently expect input to appear
    // (after some event like 'keypress' or 'input') and are polling
    // intensively.
    d.pollingFast = false;
    // Self-resetting timeout for the poller
    d.poll = new Delayed();
    // True when a drag from the editor is active
    d.draggingText = false;

    d.cachedCharWidth = d.cachedTextHeight = null;
    d.measureLineCache = [];
    d.measureLineCachePos = 0;

    // Tracks when resetInput has punted to just putting a short
    // string instead of the (large) selection.
    d.inaccurateSelection = false;

    // Used to adjust overwrite behaviour when a paste has been
    // detected
    d.pasteIncoming = false;

    return d;
  }

  // VIEW CONSTRUCTOR

  function makeView(doc) {
    var selPos = {line: 0, ch: 0};
    return {
      doc: doc,
      // frontier is the point up to which the content has been parsed,
      frontier: 0, highlight: new Delayed(),
      sel: {from: selPos, to: selPos, head: selPos, anchor: selPos, shift: false, extend: false},
      scrollTop: 0, scrollLeft: 0,
      overwrite: false, focused: false,
      // Tracks the maximum line length so that
      // the horizontal scrollbar can be kept
      // static when scrolling.
      maxLine: getLine(doc, 0),
      maxLineLength: 0,
      maxLineChanged: false,
      suppressEdits: false,
      goalColumn: null,
      cantEdit: false,
      keyMaps: []
    };
  }

  // STATE UPDATES

  // Used to get the editor into a consistent state again when options change.

  function loadMode(cm) {
    var doc = cm.view.doc;
    cm.view.mode = CodeMirror.getMode(cm.options, cm.options.mode);
    doc.iter(0, doc.size, function(line) { line.stateAfter = null; });
    cm.view.frontier = 0;
    startWorker(cm, 100);
  }

  function wrappingChanged(cm) {
    var doc = cm.view.doc, th = textHeight(cm.display);
    if (cm.options.lineWrapping) {
      cm.display.wrapper.className += " CodeMirror-wrap";
      var perLine = cm.display.scroller.clientWidth / charWidth(cm.display) - 3;
      doc.iter(0, doc.size, function(line) {
        if (line.height == 0) return;
        var guess = Math.ceil(line.text.length / perLine) || 1;
        if (guess != 1) updateLineHeight(line, guess * th);
      });
      cm.display.sizer.style.minWidth = "";
    } else {
      cm.display.wrapper.className = cm.display.wrapper.className.replace(" CodeMirror-wrap", "");
      computeMaxLength(cm.view);
      doc.iter(0, doc.size, function(line) {
        if (line.height != 0) updateLineHeight(line, th);
      });
    }
    regChange(cm, 0, doc.size);
    clearCaches(cm);
    setTimeout(function(){updateScrollbars(cm.display, cm.view.doc.height);}, 100);
  }

  function keyMapChanged(cm) {
    var style = keyMap[cm.options.keyMap].style;
    cm.display.wrapper.className = cm.display.wrapper.className.replace(/\s*cm-keymap-\S+/g, "") +
      (style ? " cm-keymap-" + style : "");
  }

  function themeChanged(cm) {
    cm.display.wrapper.className = cm.display.wrapper.className.replace(/\s*cm-s-\S+/g, "") +
      cm.options.theme.replace(/(^|\s)\s*/g, " cm-s-");
    clearCaches(cm);
  }

  function guttersChanged(cm) {
    updateGutters(cm);
    updateDisplay(cm, true);
  }

  function updateGutters(cm) {
    var gutters = cm.display.gutters, specs = cm.options.gutters;
    removeChildren(gutters);
    for (var i = 0; i < specs.length; ++i) {
      var gutterClass = specs[i];
      var gElt = gutters.appendChild(elt("div", null, "CodeMirror-gutter " + gutterClass));
      if (gutterClass == "CodeMirror-linenumbers") {
        cm.display.lineGutter = gElt;
        gElt.style.width = (cm.display.lineNumWidth || 1) + "px";
      }
    }
    gutters.style.display = i ? "" : "none";
  }

  function lineLength(doc, line) {
    if (line.height == 0) return 0;
    var len = line.text.length, merged, cur = line;
    while (merged = collapsedSpanAtStart(cur)) {
      var found = merged.find();
      cur = getLine(doc, found.from.line);
      len += found.from.ch - found.to.ch;
    }
    cur = line;
    while (merged = collapsedSpanAtEnd(cur)) {
      var found = merged.find();
      len -= cur.text.length - found.from.ch;
      cur = getLine(doc, found.to.line);
      len += cur.text.length - found.to.ch;
    }
    return len;
  }

  function computeMaxLength(view) {
    view.maxLine = getLine(view.doc, 0);
    view.maxLineLength = lineLength(view.doc, view.maxLine);
    view.maxLineChanged = true;
    view.doc.iter(1, view.doc.size, function(line) {
      var len = lineLength(view.doc, line);
      if (len > view.maxLineLength) {
        view.maxLineLength = len;
        view.maxLine = line;
      }
    });
  }

  // Make sure the gutters options contains the element
  // "CodeMirror-linenumbers" when the lineNumbers option is true.
  function setGuttersForLineNumbers(options) {
    var found = false;
    for (var i = 0; i < options.gutters.length; ++i) {
      if (options.gutters[i] == "CodeMirror-linenumbers") {
        if (options.lineNumbers) found = true;
        else options.gutters.splice(i--, 1);
      }
    }
    if (!found && options.lineNumbers)
      options.gutters.push("CodeMirror-linenumbers");
  }

  // SCROLLBARS

  // Re-synchronize the fake scrollbars with the actual size of the
  // content. Optionally force a scrollTop.
  function updateScrollbars(d /* display */, docHeight) {
    var totalHeight = docHeight + 2 * paddingTop(d);
    d.sizer.style.minHeight = d.heightForcer.style.top = totalHeight + "px";
    var scrollHeight = Math.max(totalHeight, d.scroller.scrollHeight);
    var needsH = d.scroller.scrollWidth > d.scroller.clientWidth;
    var needsV = scrollHeight > d.scroller.clientHeight;
    if (needsV) {
      d.scrollbarV.style.display = "block";
      d.scrollbarV.style.bottom = needsH ? scrollbarWidth(d.measure) + "px" : "0";
      d.scrollbarV.firstChild.style.height = 
        (scrollHeight - d.scroller.clientHeight + d.scrollbarV.clientHeight) + "px";
    } else d.scrollbarV.style.display = "";
    if (needsH) {
      d.scrollbarH.style.display = "block";
      d.scrollbarH.style.right = needsV ? scrollbarWidth(d.measure) + "px" : "0";
      d.scrollbarH.firstChild.style.width =
        (d.scroller.scrollWidth - d.scroller.clientWidth + d.scrollbarH.clientWidth) + "px";
    } else d.scrollbarH.style.display = "";
    if (needsH && needsV) {
      d.scrollbarFiller.style.display = "block";
      d.scrollbarFiller.style.height = d.scrollbarFiller.style.width = scrollbarWidth(d.measure) + "px";
    } else d.scrollbarFiller.style.display = "";

    if (mac_geLion && scrollbarWidth(d.measure) === 0)
      d.scrollbarV.style.minWidth = d.scrollbarH.style.minHeight = mac_geMountainLion ? "18px" : "12px";
  }

  function visibleLines(display, doc, viewPort) {
    var top = display.scroller.scrollTop, height = display.wrapper.clientHeight;
    if (typeof viewPort == "number") top = viewPort;
    else if (viewPort) {top = viewPort.top; height = viewPort.bottom - viewPort.top;}
    top = Math.floor(top - paddingTop(display));
    var bottom = Math.ceil(top + height);
    return {from: lineAtHeight(doc, top), to: lineAtHeight(doc, bottom)};
  }

  // LINE NUMBERS

  function alignHorizontally(cm) {
    var display = cm.display;
    if (!display.alignWidgets && !display.gutters.firstChild) return;
    var comp = compensateForHScroll(display) - display.scroller.scrollLeft + cm.view.scrollLeft;
    var gutterW = display.gutters.offsetWidth, l = comp + "px";
    for (var n = display.lineDiv.firstChild; n; n = n.nextSibling) if (n.alignable) {
      for (var i = 0, a = n.alignable; i < a.length; ++i) a[i].style.left = l;
    }
    display.gutters.style.left = (comp + gutterW) + "px";
  }

  function maybeUpdateLineNumberWidth(cm) {
    if (!cm.options.lineNumbers) return false;
    var doc = cm.view.doc, last = lineNumberFor(cm.options, doc.size - 1), display = cm.display;
    if (last.length != display.lineNumChars) {
      var test = display.measure.appendChild(elt("div", [elt("div", last)],
                                                 "CodeMirror-linenumber CodeMirror-gutter-elt"));
      var innerW = test.firstChild.offsetWidth, padding = test.offsetWidth - innerW;
      display.lineGutter.style.width = "";
      display.lineNumInnerWidth = Math.max(innerW, display.lineGutter.offsetWidth - padding);
      display.lineNumWidth = display.lineNumInnerWidth + padding;
      display.lineNumChars = display.lineNumInnerWidth ? last.length : -1;
      display.lineGutter.style.width = display.lineNumWidth + "px";
      return true;
    }
    return false;
  }

  function lineNumberFor(options, i) {
    return String(options.lineNumberFormatter(i + options.firstLineNumber));
  }
  function compensateForHScroll(display) {
    return display.scroller.getBoundingClientRect().left - display.sizer.getBoundingClientRect().left;
  }

  // DISPLAY DRAWING

  function updateDisplay(cm, changes, viewPort) {
    var oldFrom = cm.display.showingFrom, oldTo = cm.display.showingTo;
    var updated = updateDisplayInner(cm, changes, viewPort);
    if (updated) {
      signalLater(cm, cm, "update", cm);
      if (cm.display.showingFrom != oldFrom || cm.display.showingTo != oldTo)
        signalLater(cm, cm, "viewportChange", cm, cm.display.showingFrom, cm.display.showingTo);
    }
    updateSelection(cm);
    updateScrollbars(cm.display, cm.view.doc.height);

    return updated;
  }

  // Uses a set of changes plus the current scroll position to
  // determine which DOM updates have to be made, and makes the
  // updates.
  function updateDisplayInner(cm, changes, viewPort) {
    var display = cm.display, doc = cm.view.doc;
    if (!display.wrapper.clientWidth) {
      display.showingFrom = display.showingTo = display.viewOffset = 0;
      return;
    }

    // Compute the new visible window
    // If scrollTop is specified, use that to determine which lines
    // to render instead of the current scrollbar position.
    var visible = visibleLines(display, doc, viewPort);
    // Bail out if the visible area is already rendered and nothing changed.
    if (changes !== true && changes.length == 0 &&
        visible.from > display.showingFrom && visible.to < display.showingTo)
      return;

    if (changes && maybeUpdateLineNumberWidth(cm))
      changes = true;
    display.sizer.style.marginLeft = display.scrollbarH.style.left = display.gutters.offsetWidth + "px";

    // When merged lines are present, the line that needs to be
    // redrawn might not be the one that was changed.
    if (changes !== true && sawCollapsedSpans)
      for (var i = 0; i < changes.length; ++i) {
        var ch = changes[i], merged;
        while (merged = collapsedSpanAtStart(getLine(doc, ch.from))) {
          var from = merged.find().from.line;
          if (ch.diff) ch.diff -= ch.from - from;
          ch.from = from;
        }
      }

    // Used to determine which lines need their line numbers updated
    var positionsChangedFrom = changes === true ? 0 : Infinity;
    if (cm.options.lineNumbers && changes && changes !== true)
      for (var i = 0; i < changes.length; ++i)
        if (changes[i].diff) { positionsChangedFrom = changes[i].from; break; }

    var from = Math.max(visible.from - cm.options.viewportMargin, 0);
    var to = Math.min(doc.size, visible.to + cm.options.viewportMargin);
    if (display.showingFrom < from && from - display.showingFrom < 20) from = display.showingFrom;
    if (display.showingTo > to && display.showingTo - to < 20) to = Math.min(doc.size, display.showingTo);
    if (sawCollapsedSpans) {
      from = lineNo(visualLine(doc, getLine(doc, from)));
      while (to < doc.size && lineIsHidden(getLine(doc, to))) ++to;
    }

    // Create a range of theoretically intact lines, and punch holes
    // in that using the change info.
    var intact = changes === true ? [] :
      computeIntact([{from: display.showingFrom, to: display.showingTo}], changes);
    // Clip off the parts that won't be visible
    var intactLines = 0;
    for (var i = 0; i < intact.length; ++i) {
      var range = intact[i];
      if (range.from < from) range.from = from;
      if (range.to > to) range.to = to;
      if (range.from >= range.to) intact.splice(i--, 1);
      else intactLines += range.to - range.from;
    }
    if (intactLines == to - from && from == display.showingFrom && to == display.showingTo)
      return;
    intact.sort(function(a, b) {return a.from - b.from;});

    if (intactLines < (to - from) * .7) display.lineDiv.style.display = "none";
    patchDisplay(cm, from, to, intact, positionsChangedFrom);
    display.lineDiv.style.display = "";

    var different = from != display.showingFrom || to != display.showingTo ||
      display.lastSizeC != display.wrapper.clientHeight;
    // This is just a bogus formula that detects when the editor is
    // resized or the font size changes.
    if (different) display.lastSizeC = display.wrapper.clientHeight;
    display.showingFrom = from; display.showingTo = to;
    startWorker(cm, 100);

    var prevBottom = display.lineDiv.offsetTop;
    for (var node = display.lineDiv.firstChild, height; node; node = node.nextSibling) if (node.lineObj) {
      if (ie_lt8) {
        var bot = node.offsetTop + node.offsetHeight;
        height = bot - prevBottom;
        prevBottom = bot;
      } else {
        var box = node.getBoundingClientRect();
        height = box.bottom - box.top;
      }
      var diff = node.lineObj.height - height;
      if (height < 2) height = textHeight(display);
      if (diff > .001 || diff < -.001)
        updateLineHeight(node.lineObj, height);
    }
    display.viewOffset = heightAtLine(cm, getLine(doc, from));
    // Position the mover div to align with the current virtual scroll position
    display.mover.style.top = display.viewOffset + "px";
    return true;
  }

  function computeIntact(intact, changes) {
    for (var i = 0, l = changes.length || 0; i < l; ++i) {
      var change = changes[i], intact2 = [], diff = change.diff || 0;
      for (var j = 0, l2 = intact.length; j < l2; ++j) {
        var range = intact[j];
        if (change.to <= range.from && change.diff) {
          intact2.push({from: range.from + diff, to: range.to + diff});
        } else if (change.to <= range.from || change.from >= range.to) {
          intact2.push(range);
        } else {
          if (change.from > range.from)
            intact2.push({from: range.from, to: change.from});
          if (change.to < range.to)
            intact2.push({from: change.to + diff, to: range.to + diff});
        }
      }
      intact = intact2;
    }
    return intact;
  }

  function getDimensions(cm) {
    var d = cm.display, left = {}, width = {};
    for (var n = d.gutters.firstChild, i = 0; n; n = n.nextSibling, ++i) {
      left[cm.options.gutters[i]] = n.offsetLeft;
      width[cm.options.gutters[i]] = n.offsetWidth;
    }
    return {fixedPos: compensateForHScroll(d),
            gutterTotalWidth: d.gutters.offsetWidth,
            gutterLeft: left,
            gutterWidth: width,
            wrapperWidth: d.wrapper.clientWidth};
  }

  function patchDisplay(cm, from, to, intact, updateNumbersFrom) {
    var dims = getDimensions(cm);
    var display = cm.display, lineNumbers = cm.options.lineNumbers;
    // IE does bad things to nodes when .innerHTML = "" is used on a parent
    // we still need widgets and markers intact to add back to the new content later
    if (!intact.length && !ie && (!webkit || !cm.display.currentWheelTarget))
      removeChildren(display.lineDiv);
    var container = display.lineDiv, cur = container.firstChild;

    function rm(node) {
      var next = node.nextSibling;
      if (webkit && mac && cm.display.currentWheelTarget == node) {
        node.style.display = "none";
        node.lineObj = null;
      } else {
        container.removeChild(node);
      }
      return next;
    }

    var nextIntact = intact.shift(), lineNo = from;
    cm.view.doc.iter(from, to, function(line) {
      if (nextIntact && nextIntact.to == lineNo) nextIntact = intact.shift();
      if (lineIsHidden(line)) {
        if (line.height != 0) updateLineHeight(line, 0);
      } else if (nextIntact && nextIntact.from <= lineNo && nextIntact.to > lineNo) {
        // This line is intact. Skip to the actual node. Update its
        // line number if needed.
        while (cur.lineObj != line) cur = rm(cur);
        if (lineNumbers && updateNumbersFrom <= lineNo && cur.lineNumber)
          setTextContent(cur.lineNumber, lineNumberFor(cm.options, lineNo));
        cur = cur.nextSibling;
      } else {
        // This line needs to be generated.
        var lineNode = buildLineElement(cm, line, lineNo, dims);
        container.insertBefore(lineNode, cur);
        lineNode.lineObj = line;
      }
      ++lineNo;
    });
    while (cur) cur = rm(cur);
  }

  function buildLineElement(cm, line, lineNo, dims) {
    var lineElement = lineContent(cm, line);
    var markers = line.gutterMarkers, display = cm.display;

    if (!cm.options.lineNumbers && !markers && !line.bgClass && !line.wrapClass &&
        (!line.widgets || !line.widgets.length)) return lineElement;

    // Lines with gutter elements or a background class need
    // to be wrapped again, and have the extra elements added
    // to the wrapper div

    var wrap = elt("div", null, line.wrapClass, "position: relative");
    if (cm.options.lineNumbers || markers) {
      var gutterWrap = wrap.appendChild(elt("div", null, null, "position: absolute; left: " +
                                            dims.fixedPos + "px"));
      wrap.alignable = [gutterWrap];
      if (cm.options.lineNumbers && (!markers || !markers["CodeMirror-linenumbers"]))
        wrap.lineNumber = gutterWrap.appendChild(
          elt("div", lineNumberFor(cm.options, lineNo),
              "CodeMirror-linenumber CodeMirror-gutter-elt",
              "left: " + dims.gutterLeft["CodeMirror-linenumbers"] + "px; width: "
              + display.lineNumInnerWidth + "px"));
      if (markers)
        for (var k = 0; k < cm.options.gutters.length; ++k) {
          var id = cm.options.gutters[k], found = markers.hasOwnProperty(id) && markers[id];
          if (found)
            gutterWrap.appendChild(elt("div", [found], "CodeMirror-gutter-elt", "left: " +
                                       dims.gutterLeft[id] + "px; width: " + dims.gutterWidth[id] + "px"));
        }
    }
    // Kludge to make sure the styled element lies behind the selection (by z-index)
    if (line.bgClass)
      wrap.appendChild(elt("div", "\u00a0", line.bgClass + " CodeMirror-linebackground"));
    wrap.appendChild(lineElement);
    if (line.widgets)
      for (var i = 0, ws = line.widgets; i < ws.length; ++i) {
        var widget = ws[i], node = elt("div", [widget.node], "CodeMirror-linewidget");
        node.widget = widget;
        if (widget.noHScroll) {
          (wrap.alignable || (wrap.alignable = [])).push(node);
          var width = dims.wrapperWidth;
          node.style.left = dims.fixedPos + "px";
          if (!widget.coverGutter) {
            width -= dims.gutterTotalWidth;
            node.style.paddingLeft = dims.gutterTotalWidth + "px";
          }
          node.style.width = width + "px";
        }
        if (widget.coverGutter) {
          node.style.zIndex = 5;
          node.style.position = "relative";
          if (!widget.noHScroll) node.style.marginLeft = -dims.gutterTotalWidth + "px";
        }
        if (widget.above)
          wrap.insertBefore(node, cm.options.lineNumbers && line.height != 0 ? gutterWrap : lineElement);
        else
          wrap.appendChild(node);
      }

    if (ie_lt8) wrap.style.zIndex = 2;
    return wrap;
  }

  // SELECTION / CURSOR

  function updateSelection(cm) {
    var display = cm.display;
    var collapsed = posEq(cm.view.sel.from, cm.view.sel.to);
    if (collapsed || cm.options.showCursorWhenSelecting)
      updateSelectionCursor(cm);
    else
      display.cursor.style.display = display.otherCursor.style.display = "none";
    if (!collapsed)
      updateSelectionRange(cm);
    else
      display.selectionDiv.style.display = "none";

    // Move the hidden textarea near the cursor to prevent scrolling artifacts
    var headPos = cursorCoords(cm, cm.view.sel.head, "div");
    var wrapOff = display.wrapper.getBoundingClientRect(), lineOff = display.lineDiv.getBoundingClientRect();
    display.inputDiv.style.top = Math.max(0, Math.min(display.wrapper.clientHeight - 10,
                                                      headPos.top + lineOff.top - wrapOff.top)) + "px";
    display.inputDiv.style.left = Math.max(0, Math.min(display.wrapper.clientWidth - 10,
                                                       headPos.left + lineOff.left - wrapOff.left)) + "px";
  }

  // No selection, plain cursor
  function updateSelectionCursor(cm) {
    var display = cm.display, pos = cursorCoords(cm, cm.view.sel.head, "div");
    display.cursor.style.left = pos.left + "px";
    display.cursor.style.top = pos.top + "px";
    display.cursor.style.height = Math.max(0, pos.bottom - pos.top) * cm.options.cursorHeight + "px";
    display.cursor.style.display = "";

    if (pos.other) {
      display.otherCursor.style.display = "";
      display.otherCursor.style.left = pos.other.left + "px";
      display.otherCursor.style.top = pos.other.top + "px";
      display.otherCursor.style.height = (pos.other.bottom - pos.other.top) * .85 + "px";
    } else { display.otherCursor.style.display = "none"; }
  }

  // Highlight selection
  function updateSelectionRange(cm) {
    var display = cm.display, doc = cm.view.doc, sel = cm.view.sel;
    var fragment = document.createDocumentFragment();
    var clientWidth = display.lineSpace.offsetWidth, pl = paddingLeft(cm.display);

    function add(left, top, width, bottom) {
      if (top < 0) top = 0;
      fragment.appendChild(elt("div", null, "CodeMirror-selected", "position: absolute; left: " + left +
                               "px; top: " + top + "px; width: " + (width == null ? clientWidth - left : width) +
                               "px; height: " + (bottom - top) + "px"));
    }

    function drawForLine(line, fromArg, toArg, retTop) {
      var lineObj = getLine(doc, line);
      var lineLen = lineObj.text.length, rVal = retTop ? Infinity : -Infinity;
      function coords(ch) {
        return charCoords(cm, {line: line, ch: ch}, "div", lineObj);
      }

      iterateBidiSections(getOrder(lineObj), fromArg || 0, toArg == null ? lineLen : toArg, function(from, to, dir) {
        var leftPos = coords(dir == "rtl" ? to - 1 : from);
        var rightPos = coords(dir == "rtl" ? from : to - 1);
        var left = leftPos.left, right = rightPos.right;
        if (rightPos.top - leftPos.top > 3) { // Different lines, draw top part
          add(left, leftPos.top, null, leftPos.bottom);
          left = pl;
          if (leftPos.bottom < rightPos.top) add(left, leftPos.bottom, null, rightPos.top);
        }
        if (toArg == null && to == lineLen) right = clientWidth;
        if (fromArg == null && from == 0) left = pl;
        rVal = retTop ? Math.min(rightPos.top, rVal) : Math.max(rightPos.bottom, rVal);
        if (left < pl + 1) left = pl;
        add(left, rightPos.top, right - left, rightPos.bottom);
      });
      return rVal;
    }

    if (sel.from.line == sel.to.line) {
      drawForLine(sel.from.line, sel.from.ch, sel.to.ch);
    } else {
      var fromObj = getLine(doc, sel.from.line);
      var cur = fromObj, merged, path = [sel.from.line, sel.from.ch], singleLine;
      while (merged = collapsedSpanAtEnd(cur)) {
        var found = merged.find();
        path.push(found.from.ch, found.to.line, found.to.ch);
        if (found.to.line == sel.to.line) {
          path.push(sel.to.ch);
          singleLine = true;
          break;
        }
        cur = getLine(doc, found.to.line);
      }

      // This is a single, merged line
      if (singleLine) {
        for (var i = 0; i < path.length; i += 3)
          drawForLine(path[i], path[i+1], path[i+2]);
      } else {
        var middleTop, middleBot, toObj = getLine(doc, sel.to.line);
        if (sel.from.ch)
          // Draw the first line of selection.
          middleTop = drawForLine(sel.from.line, sel.from.ch, null, false);
        else
          // Simply include it in the middle block.
          middleTop = heightAtLine(cm, fromObj) - display.viewOffset;

        if (!sel.to.ch)
          middleBot = heightAtLine(cm, toObj) - display.viewOffset;
        else
          middleBot = drawForLine(sel.to.line, collapsedSpanAtStart(toObj) ? null : 0, sel.to.ch, true);

        if (middleTop < middleBot) add(pl, middleTop, null, middleBot);
      }
    }

    removeChildrenAndAdd(display.selectionDiv, fragment);
    display.selectionDiv.style.display = "";
  }

  // Cursor-blinking
  function restartBlink(cm) {
    var display = cm.display;
    clearInterval(display.blinker);
    var on = true;
    display.cursor.style.visibility = display.otherCursor.style.visibility = "";
    display.blinker = setInterval(function() {
      if (!display.cursor.offsetHeight) return;
      display.cursor.style.visibility = display.otherCursor.style.visibility = (on = !on) ? "" : "hidden";
    }, cm.options.cursorBlinkRate);
  }

  // HIGHLIGHT WORKER

  function startWorker(cm, time) {
    if (cm.view.frontier < cm.display.showingTo)
      cm.view.highlight.set(time, bind(highlightWorker, cm));
  }

  function highlightWorker(cm) {
    var view = cm.view, doc = view.doc;
    if (view.frontier >= cm.display.showingTo) return;
    var end = +new Date + cm.options.workTime;
    var state = copyState(view.mode, getStateBefore(cm, view.frontier));
    var changed = [], prevChange;
    doc.iter(view.frontier, Math.min(doc.size, cm.display.showingTo + 500), function(line) {
      if (view.frontier >= cm.display.showingFrom) { // Visible
        if (highlightLine(cm, line, state) && view.frontier >= cm.display.showingFrom) {
          if (prevChange && prevChange.end == view.frontier) prevChange.end++;
          else changed.push(prevChange = {start: view.frontier, end: view.frontier + 1});
        }
        line.stateAfter = copyState(view.mode, state);
      } else {
        processLine(cm, line, state);
        line.stateAfter = view.frontier % 5 == 0 ? copyState(view.mode, state) : null;
      }
      ++view.frontier;
      if (+new Date > end) {
        startWorker(cm, cm.options.workDelay);
        return true;
      }
    });
    if (changed.length)
      operation(cm, function() {
        for (var i = 0; i < changed.length; ++i)
          regChange(this, changed[i].start, changed[i].end);
      })();
  }

  // Finds the line to start with when starting a parse. Tries to
  // find a line with a stateAfter, so that it can start with a
  // valid state. If that fails, it returns the line with the
  // smallest indentation, which tends to need the least context to
  // parse correctly.
  function findStartLine(cm, n) {
    var minindent, minline, doc = cm.view.doc;
    for (var search = n, lim = n - 100; search > lim; --search) {
      if (search == 0) return 0;
      var line = getLine(doc, search-1);
      if (line.stateAfter) return search;
      var indented = countColumn(line.text, null, cm.options.tabSize);
      if (minline == null || minindent > indented) {
        minline = search - 1;
        minindent = indented;
      }
    }
    return minline;
  }

  function getStateBefore(cm, n) {
    var view = cm.view;
    var pos = findStartLine(cm, n), state = pos && getLine(view.doc, pos-1).stateAfter;
    if (!state) state = startState(view.mode);
    else state = copyState(view.mode, state);
    view.doc.iter(pos, n, function(line) {
      processLine(cm, line, state);
      var save = pos == n - 1 || pos % 5 == 0 || pos >= view.showingFrom && pos < view.showingTo;
      line.stateAfter = save ? copyState(view.mode, state) : null;
      ++pos;
    });
    return state;
  }

  // POSITION MEASUREMENT
  
  function paddingTop(display) {return display.lineSpace.offsetTop;}
  function paddingLeft(display) {
    var e = removeChildrenAndAdd(display.measure, elt("pre")).appendChild(elt("span", "x"));
    return e.offsetLeft;
  }

  function measureChar(cm, line, ch, data) {
    var data = data || measureLine(cm, line), dir = -1;
    for (var pos = ch;; pos += dir) {
      var r = data[pos];
      if (r) break;
      if (dir < 0 && pos == 0) dir = 1;
    }
    return {left: pos < ch ? r.right : r.left,
            right: pos > ch ? r.left : r.right,
            top: r.top, bottom: r.bottom};
  }

  function measureLine(cm, line) {
    // First look in the cache
    var display = cm.display, cache = cm.display.measureLineCache;
    for (var i = 0; i < cache.length; ++i) {
      var memo = cache[i];
      if (memo.text == line.text && memo.markedSpans == line.markedSpans &&
          display.scroller.clientWidth == memo.width)
        return memo.measure;
    }
    
    var measure = measureLineInner(cm, line);
    // Store result in the cache
    var memo = {text: line.text, width: display.scroller.clientWidth,
                markedSpans: line.markedSpans, measure: measure};
    if (cache.length == 16) cache[++display.measureLineCachePos % 16] = memo;
    else cache.push(memo);
    return measure;
  }

  function measureLineInner(cm, line) {
    var display = cm.display, measure = emptyArray(line.text.length);
    var pre = lineContent(cm, line, measure);

    // IE does not cache element positions of inline elements between
    // calls to getBoundingClientRect. This makes the loop below,
    // which gathers the positions of all the characters on the line,
    // do an amount of layout work quadratic to the number of
    // characters. When line wrapping is off, we try to improve things
    // by first subdividing the line into a bunch of inline blocks, so
    // that IE can reuse most of the layout information from caches
    // for those blocks. This does interfere with line wrapping, so it
    // doesn't work when wrapping is on, but in that case the
    // situation is slightly better, since IE does cache line-wrapping
    // information and only recomputes per-line.
    if (ie && !ie_lt8 && !cm.options.lineWrapping && pre.childNodes.length > 100) {
      var fragment = document.createDocumentFragment();
      var chunk = 10, n = pre.childNodes.length;
      for (var i = 0, chunks = Math.ceil(n / chunk); i < chunks; ++i) {
        var wrap = elt("div", null, null, "display: inline-block");
        for (var j = 0; j < chunk && n; ++j) {
          wrap.appendChild(pre.firstChild);
          --n;
        }
        fragment.appendChild(wrap);
      }
      pre.appendChild(fragment);
    }

    removeChildrenAndAdd(display.measure, pre);

    var outer = display.lineDiv.getBoundingClientRect();
    var vranges = [], data = emptyArray(line.text.length), maxBot = pre.offsetHeight;
    for (var i = 0, cur; i < measure.length; ++i) if (cur = measure[i]) {
      var size = cur.getBoundingClientRect();
      var top = Math.max(0, size.top - outer.top), bot = Math.min(size.bottom - outer.top, maxBot);
      for (var j = 0; j < vranges.length; j += 2) {
        var rtop = vranges[j], rbot = vranges[j+1];
        if (rtop > bot || rbot < top) continue;
        if (rtop <= top && rbot >= bot ||
            top <= rtop && bot >= rbot ||
            Math.min(bot, rbot) - Math.max(top, rtop) >= (bot - top) >> 1) {
          vranges[j] = Math.min(top, rtop);
          vranges[j+1] = Math.max(bot, rbot);
          break;
        }
      }
      if (j == vranges.length) vranges.push(top, bot);
      data[i] = {left: size.left - outer.left, right: size.right - outer.left, top: j};
    }
    for (var i = 0, cur; i < data.length; ++i) if (cur = data[i]) {
      var vr = cur.top;
      cur.top = vranges[vr]; cur.bottom = vranges[vr+1];
    }
    return data;
  }

  function clearCaches(cm) {
    cm.display.measureLineCache.length = cm.display.measureLineCachePos = 0;
    cm.display.cachedCharWidth = cm.display.cachedTextHeight = null;
    cm.view.maxLineChanged = true;
  }

  // Context is one of "line", "div" (display.lineDiv), "local"/null (editor), or "page"
  function intoCoordSystem(cm, lineObj, rect, context) {
    if (lineObj.widgets) for (var i = 0; i < lineObj.widgets.length; ++i) if (lineObj.widgets[i].above) {
      var size = lineObj.widgets[i].node.offsetHeight;
      rect.top += size; rect.bottom += size;
    }
    if (context == "line") return rect;
    if (!context) context = "local";
    var yOff = heightAtLine(cm, lineObj);
    if (context != "local") yOff -= cm.display.viewOffset;
    if (context == "page") {
      var lOff = cm.display.lineSpace.getBoundingClientRect();
      yOff += lOff.top + (window.pageYOffset || (document.documentElement || document.body).scrollTop);
      var xOff = lOff.left + (window.pageXOffset || (document.documentElement || document.body).scrollLeft);
      rect.left += xOff; rect.right += xOff;
    }
    rect.top += yOff; rect.bottom += yOff;
    return rect;
  }

  function charCoords(cm, pos, context, lineObj) {
    if (!lineObj) lineObj = getLine(cm.view.doc, pos.line);
    return intoCoordSystem(cm, lineObj, measureChar(cm, lineObj, pos.ch), context);
  }

  function cursorCoords(cm, pos, context, lineObj, measurement) {
    lineObj = lineObj || getLine(cm.view.doc, pos.line);
    if (!measurement) measurement = measureLine(cm, lineObj);
    function get(ch, right) {
      var m = measureChar(cm, lineObj, ch, measurement);
      if (right) m.left = m.right; else m.right = m.left;
      return intoCoordSystem(cm, lineObj, m, context);
    }
    var order = getOrder(lineObj), ch = pos.ch;
    if (!order) return get(ch);
    var main, other, linedir = order[0].level;
    for (var i = 0; i < order.length; ++i) {
      var part = order[i], rtl = part.level % 2, nb, here;
      if (part.from < ch && part.to > ch) return get(ch, rtl);
      var left = rtl ? part.to : part.from, right = rtl ? part.from : part.to;
      if (left == ch) {
        // Opera and IE return bogus offsets and widths for edges
        // where the direction flips, but only for the side with the
        // lower level. So we try to use the side with the higher
        // level.
        if (i && part.level < (nb = order[i-1]).level) here = get(nb.level % 2 ? nb.from : nb.to - 1, true);
        else here = get(rtl && part.from != part.to ? ch - 1 : ch);
        if (rtl == linedir) main = here; else other = here;
      } else if (right == ch) {
        var nb = i < order.length - 1 && order[i+1];
        if (!rtl && nb && nb.from == nb.to) continue;
        if (nb && part.level < nb.level) here = get(nb.level % 2 ? nb.to - 1 : nb.from);
        else here = get(rtl ? ch : ch - 1, true);
        if (rtl == linedir) main = here; else other = here;
      }
    }
    if (linedir && !ch) other = get(order[0].to - 1);
    if (!main) return other;
    if (other) main.other = other;
    return main;
  }

  // Coords must be lineSpace-local
  function coordsChar(cm, x, y) {
    var doc = cm.view.doc;
    y += cm.display.viewOffset;
    if (y < 0) return {line: 0, ch: 0, outside: true};
    var lineNo = lineAtHeight(doc, y);
    if (lineNo >= doc.size) return {line: doc.size - 1, ch: getLine(doc, doc.size - 1).text.length};
    if (x < 0) x = 0;

    for (;;) {
      var lineObj = getLine(doc, lineNo);
      var found = coordsCharInner(cm, lineObj, lineNo, x, y);
      var merged = collapsedSpanAtEnd(lineObj);
      if (merged && found.ch == lineRight(lineObj))
        lineNo = merged.find().to.line;
      else
        return found;
    }
  }

  function coordsCharInner(cm, lineObj, lineNo, x, y) {
    var innerOff = y - heightAtLine(cm, lineObj);
    var wrongLine = false, cWidth = cm.display.wrapper.clientWidth;
    var measurement = measureLine(cm, lineObj);

    function getX(ch) {
      var sp = cursorCoords(cm, {line: lineNo, ch: ch}, "line",
                            lineObj, measurement);
      wrongLine = true;
      if (innerOff > sp.bottom) return Math.max(0, sp.left - cWidth);
      else if (innerOff < sp.top) return sp.left + cWidth;
      else wrongLine = false;
      return sp.left;
    }

    var bidi = getOrder(lineObj), dist = lineObj.text.length;
    var from = lineLeft(lineObj), to = lineRight(lineObj);
    var fromX = paddingLeft(cm.display), toX = getX(to);

    if (x > toX) return {line: lineNo, ch: to, outside: wrongLine};
    // Do a binary search between these bounds.
    for (;;) {
      if (bidi ? to == from || to == moveVisually(lineObj, from, 1) : to - from <= 1) {
        var after = x - fromX < toX - x, ch = after ? from : to;
        while (isExtendingChar.test(lineObj.text.charAt(ch))) ++ch;
        return {line: lineNo, ch: ch, after: after, outside: wrongLine};
      }
      var step = Math.ceil(dist / 2), middle = from + step;
      if (bidi) {
        middle = from;
        for (var i = 0; i < step; ++i) middle = moveVisually(lineObj, middle, 1);
      }
      var middleX = getX(middle);
      if (middleX > x) {to = middle; toX = middleX; if (wrongLine) toX += 1000; dist -= step;}
      else {from = middle; fromX = middleX; dist = step;}
    }
  }

  var measureText;
  function textHeight(display) {
    if (display.cachedTextHeight != null) return display.cachedTextHeight;
    if (measureText == null) {
      measureText = elt("pre");
      // Measure a bunch of lines, for browsers that compute
      // fractional heights.
      for (var i = 0; i < 49; ++i) {
        measureText.appendChild(document.createTextNode("x"));
        measureText.appendChild(elt("br"));
      }
      measureText.appendChild(document.createTextNode("x"));
    }
    removeChildrenAndAdd(display.measure, measureText);
    var height = measureText.offsetHeight / 50;
    if (height > 3) display.cachedTextHeight = height;
    removeChildren(display.measure);
    return height || 1;
  }

  function charWidth(display) {
    if (display.cachedCharWidth != null) return display.cachedCharWidth;
    var anchor = elt("span", "x");
    var pre = elt("pre", [anchor]);
    removeChildrenAndAdd(display.measure, pre);
    var width = anchor.offsetWidth;
    if (width > 2) display.cachedCharWidth = width;
    return width || 10;
  }

  // OPERATIONS

  // Operations are used to wrap changes in such a way that each
  // change won't have to update the cursor and display (which would
  // be awkward, slow, and error-prone), but instead updates are
  // batched and then all combined and executed at once.

  function startOperation(cm) {
    if (cm.curOp) ++cm.curOp.depth;
    else cm.curOp = {
      // Nested operations delay update until the outermost one
      // finishes.
      depth: 1,
      // An array of ranges of lines that have to be updated. See
      // updateDisplay.
      changes: [],
      delayedCallbacks: [],
      updateInput: null,
      userSelChange: null,
      textChanged: null,
      selectionChanged: false,
      updateMaxLine: false,
      id: ++cm.nextOpId
    };
  }

  function endOperation(cm) {
    var op = cm.curOp;
    if (--op.depth) return;
    cm.curOp = null;
    var view = cm.view, display = cm.display;
    if (op.updateMaxLine) computeMaxLength(view);
    if (view.maxLineChanged && !cm.options.lineWrapping) {
      var width = measureChar(cm, view.maxLine, view.maxLine.text.length).right;
      display.sizer.style.minWidth = (width + 3 + scrollerCutOff) + "px";
      view.maxLineChanged = false;
    }
    var newScrollPos, updated;
    if (op.selectionChanged) {
      var coords = cursorCoords(cm, view.sel.head);
      newScrollPos = calculateScrollPos(cm, coords.left, coords.top, coords.left, coords.bottom);
    }
    if (op.changes.length || newScrollPos && newScrollPos.scrollTop != null)
      updated = updateDisplay(cm, op.changes, newScrollPos && newScrollPos.scrollTop);
    if (!updated && op.selectionChanged) updateSelection(cm);
    if (newScrollPos) scrollCursorIntoView(cm);
    if (op.selectionChanged) restartBlink(cm);

    if (view.focused && op.updateInput)
      resetInput(cm, op.userSelChange);

    if (op.textChanged)
      signal(cm, "change", cm, op.textChanged);
    if (op.selectionChanged) signal(cm, "cursorActivity", cm);
    for (var i = 0; i < op.delayedCallbacks.length; ++i) op.delayedCallbacks[i](cm);
  }

  // Wraps a function in an operation. Returns the wrapped function.
  function operation(cm1, f) {
    return function() {
      var cm = cm1 || this;
      startOperation(cm);
      try {var result = f.apply(cm, arguments);}
      finally {endOperation(cm);}
      return result;
    };
  }

  function regChange(cm, from, to, lendiff) {
    cm.curOp.changes.push({from: from, to: to, diff: lendiff});
  }

  // INPUT HANDLING

  function slowPoll(cm) {
    if (cm.view.pollingFast) return;
    cm.display.poll.set(cm.options.pollInterval, function() {
      readInput(cm);
      if (cm.view.focused) slowPoll(cm);
    });
  }

  function fastPoll(cm) {
    var missed = false;
    cm.display.pollingFast = true;
    function p() {
      var changed = readInput(cm);
      if (!changed && !missed) {missed = true; cm.display.poll.set(60, p);}
      else {cm.display.pollingFast = false; slowPoll(cm);}
    }
    cm.display.poll.set(20, p);
  }

  // prevInput is a hack to work with IME. If we reset the textarea
  // on every change, that breaks IME. So we look for changes
  // compared to the previous content instead. (Modern browsers have
  // events that indicate IME taking place, but these are not widely
  // supported or compatible enough yet to rely on.)
  function readInput(cm) {
    var input = cm.display.input, prevInput = cm.display.prevInput, view = cm.view, sel = view.sel;
    if (!view.focused || hasSelection(input) || isReadOnly(cm)) return false;
    var text = input.value;
    if (text == prevInput && posEq(sel.from, sel.to)) return false;
    startOperation(cm);
    view.sel.shift = false;
    var same = 0, l = Math.min(prevInput.length, text.length);
    while (same < l && prevInput[same] == text[same]) ++same;
    var from = sel.from, to = sel.to;
    if (same < prevInput.length)
      from = {line: from.line, ch: from.ch - (prevInput.length - same)};
    else if (view.overwrite && posEq(from, to) && !cm.display.pasteIncoming)
      to = {line: to.line, ch: Math.min(getLine(cm.view.doc, to.line).text.length, to.ch + (text.length - same))};
    var updateInput = cm.curOp.updateInput;
    updateDoc(cm, from, to, splitLines(text.slice(same)), "end",
              cm.display.pasteIncoming ? "paste" : "input", {from: from, to: to});
    cm.curOp.updateInput = updateInput;
    if (text.length > 1000) input.value = cm.display.prevInput = "";
    else cm.display.prevInput = text;
    endOperation(cm);
    cm.display.pasteIncoming = false;
    return true;
  }

  function resetInput(cm, user) {
    var view = cm.view, minimal, selected;
    if (!posEq(view.sel.from, view.sel.to)) {
      cm.display.prevInput = "";
      minimal = hasCopyEvent &&
        (view.sel.to.line - view.sel.from.line > 100 || (selected = cm.getSelection()).length > 1000);
      if (minimal) cm.display.input.value = "-";
      else cm.display.input.value = selected || cm.getSelection();
      if (view.focused) selectInput(cm.display.input);
    } else if (user) cm.display.prevInput = cm.display.input.value = "";
    cm.display.inaccurateSelection = minimal;
  }

  function focusInput(cm) {
    if (cm.options.readOnly != "nocursor" && (ie || document.activeElement != cm.display.input))
      cm.display.input.focus();
  }

  function isReadOnly(cm) {
    return cm.options.readOnly || cm.view.cantEdit;
  }

  // EVENT HANDLERS

  function registerEventHandlers(cm) {
    var d = cm.display;
    on(d.scroller, "mousedown", operation(cm, onMouseDown));
    on(d.scroller, "dblclick", operation(cm, e_preventDefault));
    on(d.lineSpace, "selectstart", function(e) {
      if (!mouseEventInWidget(d, e)) e_preventDefault(e);
    });
    // Gecko browsers fire contextmenu *after* opening the menu, at
    // which point we can't mess with it anymore. Context menu is
    // handled in onMouseDown for Gecko.
    if (!gecko) on(d.scroller, "contextmenu", function(e) {onContextMenu(cm, e);});

    on(d.scroller, "scroll", function() {
      setScrollTop(cm, d.scroller.scrollTop);
      setScrollLeft(cm, d.scroller.scrollLeft, true);
      signal(cm, "scroll", cm);
    });
    on(d.scrollbarV, "scroll", function() {
      setScrollTop(cm, d.scrollbarV.scrollTop);
    });
    on(d.scrollbarH, "scroll", function() {
      setScrollLeft(cm, d.scrollbarH.scrollLeft);
    });

    on(d.scroller, "mousewheel", function(e){onScrollWheel(cm, e);});
    on(d.scroller, "DOMMouseScroll", function(e){onScrollWheel(cm, e);});

    function reFocus() { if (cm.view.focused) setTimeout(bind(focusInput, cm), 0); }
    on(d.scrollbarH, "mousedown", reFocus);
    on(d.scrollbarV, "mousedown", reFocus);
    // Prevent wrapper from ever scrolling
    on(d.wrapper, "scroll", function() { d.wrapper.scrollTop = d.wrapper.scrollLeft = 0; });
    on(window, "resize", function resizeHandler() {
      // Might be a text scaling operation, clear size caches.
      d.cachedCharWidth = d.cachedTextHeight = null;
      clearCaches(cm);
      if (d.wrapper.parentNode) updateDisplay(cm, true);
      else off(window, "resize", resizeHandler);
    });

    on(d.input, "keyup", operation(cm, function(e) {
      if (cm.options.onKeyEvent && cm.options.onKeyEvent(cm, addStop(e))) return;
      if (e_prop(e, "keyCode") == 16) cm.view.sel.shift = false;
    }));
    on(d.input, "input", bind(fastPoll, cm));
    on(d.input, "keydown", operation(cm, onKeyDown));
    on(d.input, "keypress", operation(cm, onKeyPress));
    on(d.input, "focus", bind(onFocus, cm));
    on(d.input, "blur", bind(onBlur, cm));

    function drag_(e) {
      if (cm.options.onDragEvent && cm.options.onDragEvent(cm, addStop(e))) return;
      e_stop(e);
    }
    if (cm.options.dragDrop) {
      on(d.scroller, "dragstart", function(e){onDragStart(cm, e);});
      on(d.scroller, "dragenter", drag_);
      on(d.scroller, "dragover", drag_);
      on(d.scroller, "drop", operation(cm, onDrop));
    }
    on(d.scroller, "paste", function(){focusInput(cm); fastPoll(cm);});
    on(d.input, "paste", function() {
      d.pasteIncoming = true;
      fastPoll(cm);
    });

    function prepareCopy() {
      if (d.inaccurateSelection) {
        d.prevInput = "";
        d.inaccurateSelection = false;
        d.input.value = cm.getSelection();
        selectInput(d.input);
      }
    }
    on(d.input, "cut", prepareCopy);
    on(d.input, "copy", prepareCopy);

    // Needed to handle Tab key in KHTML
    if (khtml) on(d.sizer, "mouseup", function() {
        if (document.activeElement == d.input) d.input.blur();
        focusInput(cm);
    });
  }

  function mouseEventInWidget(display, e) {
    for (var n = e_target(e); n != display.wrapper; n = n.parentNode)
      if (/\bCodeMirror-(?:line)?widget\b/.test(n.className) ||
          n.parentNode == display.sizer && n != display.mover) return true;
  }

  function posFromMouse(cm, e, liberal) {
    var display = cm.display;
    if (!liberal) {
      var target = e_target(e);
      if (target == display.scrollbarH || target == display.scrollbarH.firstChild ||
          target == display.scrollbarV || target == display.scrollbarV.firstChild ||
          target == display.scrollbarFiller) return null;
    }
    var x, y, space = display.lineSpace.getBoundingClientRect();
    // Fails unpredictably on IE[67] when mouse is dragged around quickly.
    try { x = e.clientX; y = e.clientY; } catch (e) { return null; }
    return coordsChar(cm, x - space.left, y - space.top);
  }

  var lastClick, lastDoubleClick;
  function onMouseDown(e) {
    var cm = this, display = cm.display, view = cm.view, sel = view.sel, doc = view.doc;
    sel.shift = e_prop(e, "shiftKey");

    if (mouseEventInWidget(display, e)) {
      if (!webkit) {
        display.scroller.draggable = false;
        setTimeout(function(){display.scroller.draggable = true;}, 100);
      }
      return;
    }
    if (clickInGutter(cm, e)) return;
    var start = posFromMouse(cm, e);

    switch (e_button(e)) {
    case 3:
      if (gecko) onContextMenu.call(cm, cm, e);
      return;
    case 2:
      if (start) extendSelection(cm, start);
      setTimeout(bind(focusInput, cm), 20);
      e_preventDefault(e);
      return;
    }
    // For button 1, if it was clicked inside the editor
    // (posFromMouse returning non-null), we have to adjust the
    // selection.
    if (!start) {if (e_target(e) == display.scroller) e_preventDefault(e); return;}

    if (!view.focused) onFocus(cm);

    var now = +new Date, type = "single";
    if (lastDoubleClick && lastDoubleClick.time > now - 400 && posEq(lastDoubleClick.pos, start)) {
      type = "triple";
      e_preventDefault(e);
      setTimeout(bind(focusInput, cm), 20);
      selectLine(cm, start.line);
    } else if (lastClick && lastClick.time > now - 400 && posEq(lastClick.pos, start)) {
      type = "double";
      lastDoubleClick = {time: now, pos: start};
      e_preventDefault(e);
      var word = findWordAt(getLine(doc, start.line).text, start);
      extendSelection(cm, word.from, word.to);
    } else { lastClick = {time: now, pos: start}; }

    var last = start;
    if (cm.options.dragDrop && dragAndDrop && !isReadOnly(cm) && !posEq(sel.from, sel.to) &&
        !posLess(start, sel.from) && !posLess(sel.to, start) && type == "single") {
      var dragEnd = operation(cm, function(e2) {
        if (webkit) display.scroller.draggable = false;
        view.draggingText = false;
        off(document, "mouseup", dragEnd);
        off(display.scroller, "drop", dragEnd);
        if (Math.abs(e.clientX - e2.clientX) + Math.abs(e.clientY - e2.clientY) < 10) {
          e_preventDefault(e2);
          extendSelection(cm, start);
          focusInput(cm);
        }
      });
      // Let the drag handler handle this.
      if (webkit) display.scroller.draggable = true;
      view.draggingText = dragEnd;
      // IE's approach to draggable
      if (display.scroller.dragDrop) display.scroller.dragDrop();
      on(document, "mouseup", dragEnd);
      on(display.scroller, "drop", dragEnd);
      return;
    }
    e_preventDefault(e);
    if (type == "single") extendSelection(cm, clipPos(doc, start));

    var startstart = sel.from, startend = sel.to;

    function doSelect(cur) {
      if (type == "single") {
        extendSelection(cm, clipPos(doc, start), cur);
        return;
      }

      startstart = clipPos(doc, startstart);
      startend = clipPos(doc, startend);
      if (type == "double") {
        var word = findWordAt(getLine(doc, cur.line).text, cur);
        if (posLess(cur, startstart)) extendSelection(cm, word.from, startend);
        else extendSelection(cm, startstart, word.to);
      } else if (type == "triple") {
        if (posLess(cur, startstart)) extendSelection(cm, startend, clipPos(doc, {line: cur.line, ch: 0}));
        else extendSelection(cm, startstart, clipPos(doc, {line: cur.line + 1, ch: 0}));
      }
    }

    var editorSize = display.wrapper.getBoundingClientRect();
    // Used to ensure timeout re-tries don't fire when another extend
    // happened in the meantime (clearTimeout isn't reliable -- at
    // least on Chrome, the timeouts still happen even when cleared,
    // if the clear happens after their scheduled firing time).
    var counter = 0;

    function extend(e) {
      var curCount = ++counter;
      var cur = posFromMouse(cm, e, true);
      if (!cur) return;
      if (!posEq(cur, last)) {
        if (!view.focused) onFocus(cm);
        last = cur;
        doSelect(cur);
        var visible = visibleLines(display, doc);
        if (cur.line >= visible.to || cur.line < visible.from)
          setTimeout(operation(cm, function(){if (counter == curCount) extend(e);}), 150);
      } else {
        var outside = e.clientY < editorSize.top ? -20 : e.clientY > editorSize.bottom ? 20 : 0;
        if (outside) setTimeout(operation(cm, function() {
          if (counter != curCount) return;
          display.scroller.scrollTop += outside;
          extend(e);
        }), 50);
      }
    }

    function done(e) {
      counter = Infinity;
      var cur = posFromMouse(cm, e);
      if (cur) doSelect(cur);
      e_preventDefault(e);
      focusInput(cm);
      off(document, "mousemove", move);
      off(document, "mouseup", up);
    }

    var move = operation(cm, function(e) {
      if (!ie && !e_button(e)) done(e);
      else extend(e);
    });
    var up = operation(cm, done);
    on(document, "mousemove", move);
    on(document, "mouseup", up);
  }

  function onDrop(e) {
    var cm = this;
    if (cm.options.onDragEvent && cm.options.onDragEvent(cm, addStop(e))) return;
    e_preventDefault(e);
    var pos = posFromMouse(cm, e, true), files = e.dataTransfer.files;
    if (!pos || isReadOnly(cm)) return;
    if (files && files.length && window.FileReader && window.File) {
      var n = files.length, text = Array(n), read = 0;
      var loadFile = function(file, i) {
        var reader = new FileReader;
        reader.onload = function() {
          text[i] = reader.result;
          if (++read == n) {
            pos = clipPos(cm.view.doc, pos);
            operation(cm, function() {
              var end = replaceRange(cm, text.join(""), pos, pos, "paste");
              setSelection(cm, pos, end);
            })();
          }
        };
        reader.readAsText(file);
      };
      for (var i = 0; i < n; ++i) loadFile(files[i], i);
    } else {
      // Don't do a replace if the drop happened inside of the selected text.
      if (cm.view.draggingText && !(posLess(pos, cm.view.sel.from) || posLess(cm.view.sel.to, pos))) {
        cm.view.draggingText(e);
        if (ie) setTimeout(bind(focusInput, cm), 50);
        return;
      }
      try {
        var text = e.dataTransfer.getData("Text");
        if (text) {
          var curFrom = cm.view.sel.from, curTo = cm.view.sel.to;
          setSelection(cm, pos, pos);
          if (cm.view.draggingText) replaceRange(cm, "", curFrom, curTo, "paste");
          cm.replaceSelection(text, null, "paste");
          focusInput(cm);
          onFocus(cm);
        }
      }
      catch(e){}
    }
  }

  function clickInGutter(cm, e) {
    var display = cm.display;
    try { var mX = e.clientX, mY = e.clientY; }
    catch(e) { return false; }

    if (mX >= Math.floor(display.gutters.getBoundingClientRect().right)) return false;
    e_preventDefault(e);
    if (!hasHandler(cm, "gutterClick")) return true;

    var lineBox = display.lineDiv.getBoundingClientRect();
    if (mY > lineBox.bottom) return true;
    mY -= lineBox.top - display.viewOffset;

    for (var i = 0; i < cm.options.gutters.length; ++i) {
      var g = display.gutters.childNodes[i];
      if (g && g.getBoundingClientRect().right >= mX) {
        var line = lineAtHeight(cm.view.doc, mY);
        var gutter = cm.options.gutters[i];
        signalLater(cm, cm, "gutterClick", cm, line, gutter, e);
        break;
      }
    }
    return true;
  }

  function onDragStart(cm, e) {
    var txt = cm.getSelection();
    e.dataTransfer.setData("Text", txt);

    // Use dummy image instead of default browsers image.
    // Recent Safari (~6.0.2) have a tendency to segfault when this happens, so we don't do it there.
    if (e.dataTransfer.setDragImage && !safari)
      e.dataTransfer.setDragImage(elt('img'), 0, 0);
  }

  function setScrollTop(cm, val) {
    if (Math.abs(cm.view.scrollTop - val) < 2) return;
    cm.view.scrollTop = val;
    if (!gecko) updateDisplay(cm, [], val);
    if (cm.display.scroller.scrollTop != val) cm.display.scroller.scrollTop = val;
    if (cm.display.scrollbarV.scrollTop != val) cm.display.scrollbarV.scrollTop = val;
    if (gecko) updateDisplay(cm, []);
  }
  function setScrollLeft(cm, val, isScroller) {
    if (isScroller ? val == cm.view.scrollLeft : Math.abs(cm.view.scrollLeft - val) < 2) return;
    cm.view.scrollLeft = val;
    alignHorizontally(cm);
    if (cm.display.scroller.scrollLeft != val) cm.display.scroller.scrollLeft = val;
    if (cm.display.scrollbarH.scrollLeft != val) cm.display.scrollbarH.scrollLeft = val;
  }

  // Since the delta values reported on mouse wheel events are
  // unstandardized between browsers and even browser versions, and
  // generally horribly unpredictable, this code starts by measuring
  // the scroll effect that the first few mouse wheel events have,
  // and, from that, detects the way it can convert deltas to pixel
  // offsets afterwards.
  //
  // The reason we want to know the amount a wheel event will scroll
  // is that it gives us a chance to update the display before the
  // actual scrolling happens, reducing flickering.

  var wheelSamples = 0, wheelDX, wheelDY, wheelStartX, wheelStartY, wheelPixelsPerUnit = null;
  // Fill in a browser-detected starting value on browsers where we
  // know one. These don't have to be accurate -- the result of them
  // being wrong would just be a slight flicker on the first wheel
  // scroll (if it is large enough).
  if (ie) wheelPixelsPerUnit = -.53;
  else if (gecko) wheelPixelsPerUnit = 15;
  else if (chrome) wheelPixelsPerUnit = -.7;
  else if (safari) wheelPixelsPerUnit = -1/3;

  function onScrollWheel(cm, e) {
    var dx = e.wheelDeltaX, dy = e.wheelDeltaY;
    if (dx == null && e.detail && e.axis == e.HORIZONTAL_AXIS) dx = e.detail;
    if (dy == null && e.detail && e.axis == e.VERTICAL_AXIS) dy = e.detail;
    else if (dy == null) dy = e.wheelDelta;

    // Webkit browsers on OS X abort momentum scrolls when the target
    // of the scroll event is removed from the scrollable element.
    // This hack (see related code in patchDisplay) makes sure the
    // element is kept around.
    if (dy && mac && webkit) {
      for (var cur = e.target; cur != scroll; cur = cur.parentNode) {
        if (cur.lineObj) {
          cm.display.currentWheelTarget = cur;
          break;
        }
      }
    }

    var scroll = cm.display.scroller;
    // On some browsers, horizontal scrolling will cause redraws to
    // happen before the gutter has been realigned, causing it to
    // wriggle around in a most unseemly way. When we have an
    // estimated pixels/delta value, we just handle horizontal
    // scrolling entirely here. It'll be slightly off from native, but
    // better than glitching out.
    if (dx && !gecko && !opera && wheelPixelsPerUnit != null) {
      if (dy)
        setScrollTop(cm, Math.max(0, Math.min(scroll.scrollTop + dy * wheelPixelsPerUnit, scroll.scrollHeight - scroll.clientHeight)));
      setScrollLeft(cm, Math.max(0, Math.min(scroll.scrollLeft + dx * wheelPixelsPerUnit, scroll.scrollWidth - scroll.clientWidth)));
      e_preventDefault(e);
      wheelStartX = null; // Abort measurement, if in progress
      return;
    }

    if (dy && wheelPixelsPerUnit != null) {
      var pixels = dy * wheelPixelsPerUnit;
      var top = cm.view.scrollTop, bot = top + cm.display.wrapper.clientHeight;
      if (pixels < 0) top = Math.max(0, top + pixels - 50);
      else bot = Math.min(cm.view.doc.height, bot + pixels + 50);
      updateDisplay(cm, [], {top: top, bottom: bot});
    }

    if (wheelSamples < 20) {
      if (wheelStartX == null) {
        wheelStartX = scroll.scrollLeft; wheelStartY = scroll.scrollTop;
        wheelDX = dx; wheelDY = dy;
        setTimeout(function() {
          if (wheelStartX == null) return;
          var movedX = scroll.scrollLeft - wheelStartX;
          var movedY = scroll.scrollTop - wheelStartY;
          var sample = (movedY && wheelDY && movedY / wheelDY) ||
            (movedX && wheelDX && movedX / wheelDX);
          wheelStartX = wheelStartY = null;
          if (!sample) return;
          wheelPixelsPerUnit = (wheelPixelsPerUnit * wheelSamples + sample) / (wheelSamples + 1);
          ++wheelSamples;
        }, 200);
      } else {
        wheelDX += dx; wheelDY += dy;
      }
    }
  }

  function doHandleBinding(cm, bound, dropShift) {
    if (typeof bound == "string") {
      bound = commands[bound];
      if (!bound) return false;
    }
    // Ensure previous input has been read, so that the handler sees a
    // consistent view of the document
    if (cm.display.pollingFast && readInput(cm)) cm.display.pollingFast = false;
    var view = cm.view, prevShift = view.sel.shift;
    try {
      if (isReadOnly(cm)) view.suppressEdits = true;
      if (dropShift) view.sel.shift = false;
      bound(cm);
    } catch(e) {
      if (e != Pass) throw e;
      return false;
    } finally {
      view.sel.shift = prevShift;
      view.suppressEdits = false;
    }
    return true;
  }

  function allKeyMaps(cm) {
    var maps = cm.view.keyMaps.slice(0);
    maps.push(cm.options.keyMap);
    if (cm.options.extraKeys) maps.unshift(cm.options.extraKeys);
    return maps;
  }

  var maybeTransition;
  function handleKeyBinding(cm, e) {
    // Handle auto keymap transitions
    var startMap = getKeyMap(cm.options.keyMap), next = startMap.auto;
    clearTimeout(maybeTransition);
    if (next && !isModifierKey(e)) maybeTransition = setTimeout(function() {
      if (getKeyMap(cm.options.keyMap) == startMap)
        cm.options.keyMap = (next.call ? next.call(null, cm) : next);
    }, 50);

    var name = keyNames[e_prop(e, "keyCode")], handled = false;
    var flipCtrlCmd = mac && (opera || qtwebkit);
    if (name == null || e.altGraphKey) return false;
    if (e_prop(e, "altKey")) name = "Alt-" + name;
    if (e_prop(e, flipCtrlCmd ? "metaKey" : "ctrlKey")) name = "Ctrl-" + name;
    if (e_prop(e, flipCtrlCmd ? "ctrlKey" : "metaKey")) name = "Cmd-" + name;

    var stopped = false;
    function stop() { stopped = true; }
    var keymaps = allKeyMaps(cm);

    if (e_prop(e, "shiftKey")) {
      handled = lookupKey("Shift-" + name, keymaps,
                          function(b) {return doHandleBinding(cm, b, true);}, stop)
        || lookupKey(name, keymaps, function(b) {
          if (typeof b == "string" && /^go[A-Z]/.test(b)) return doHandleBinding(cm, b);
        }, stop);
    } else {
      handled = lookupKey(name, keymaps,
                          function(b) { return doHandleBinding(cm, b); }, stop);
    }
    if (stopped) handled = false;
    if (handled) {
      e_preventDefault(e);
      restartBlink(cm);
      if (ie_lt9) { e.oldKeyCode = e.keyCode; e.keyCode = 0; }
    }
    return handled;
  }

  function handleCharBinding(cm, e, ch) {
    var handled = lookupKey("'" + ch + "'", allKeyMaps(cm),
                            function(b) { return doHandleBinding(cm, b, true); });
    if (handled) {
      e_preventDefault(e);
      restartBlink(cm);
    }
    return handled;
  }

  var lastStoppedKey = null;
  function onKeyDown(e) {
    var cm = this;
    if (!cm.view.focused) onFocus(cm);
    if (ie && e.keyCode == 27) { e.returnValue = false; }
    if (cm.options.onKeyEvent && cm.options.onKeyEvent(cm, addStop(e))) return;
    var code = e_prop(e, "keyCode");
    // IE does strange things with escape.
    cm.view.sel.shift = code == 16 || e_prop(e, "shiftKey");
    // First give onKeyEvent option a chance to handle this.
    var handled = handleKeyBinding(cm, e);
    if (opera) {
      lastStoppedKey = handled ? code : null;
      // Opera has no cut event... we try to at least catch the key combo
      if (!handled && code == 88 && !hasCopyEvent && e_prop(e, mac ? "metaKey" : "ctrlKey"))
        cm.replaceSelection("");
    }
  }

  function onKeyPress(e) {
    var cm = this;
    if (cm.options.onKeyEvent && cm.options.onKeyEvent(cm, addStop(e))) return;
    var keyCode = e_prop(e, "keyCode"), charCode = e_prop(e, "charCode");
    if (opera && keyCode == lastStoppedKey) {lastStoppedKey = null; e_preventDefault(e); return;}
    if (((opera && (!e.which || e.which < 10)) || khtml) && handleKeyBinding(cm, e)) return;
    var ch = String.fromCharCode(charCode == null ? keyCode : charCode);
    if (this.options.electricChars && this.view.mode.electricChars &&
        this.options.smartIndent && !isReadOnly(this) &&
        this.view.mode.electricChars.indexOf(ch) > -1)
      setTimeout(operation(cm, function() {indentLine(cm, cm.view.sel.to.line, "smart");}), 75);
    if (handleCharBinding(cm, e, ch)) return;
    fastPoll(cm);
  }

  function onFocus(cm) {
    if (cm.options.readOnly == "nocursor") return;
    if (!cm.view.focused) {
      signal(cm, "focus", cm);
      cm.view.focused = true;
      if (cm.display.scroller.className.search(/\bCodeMirror-focused\b/) == -1)
        cm.display.scroller.className += " CodeMirror-focused";
      resetInput(cm, true);
    }
    slowPoll(cm);
    restartBlink(cm);
  }
  function onBlur(cm) {
    if (cm.view.focused) {
      signal(cm, "blur", cm);
      cm.view.focused = false;
      cm.display.scroller.className = cm.display.scroller.className.replace(" CodeMirror-focused", "");
    }
    clearInterval(cm.display.blinker);
    setTimeout(function() {if (!cm.view.focused) cm.view.sel.shift = false;}, 150);
  }

  var detectingSelectAll;
  function onContextMenu(cm, e) {
    var display = cm.display, sel = cm.view.sel;
    var pos = posFromMouse(cm, e), scrollPos = display.scroller.scrollTop;
    if (!pos || opera) return; // Opera is difficult.
    if (posEq(sel.from, sel.to) || posLess(pos, sel.from) || !posLess(pos, sel.to))
      operation(cm, setSelection)(cm, pos, pos);

    var oldCSS = display.input.style.cssText;
    display.inputDiv.style.position = "absolute";
    display.input.style.cssText = "position: fixed; width: 30px; height: 30px; top: " + (e.clientY - 5) +
      "px; left: " + (e.clientX - 5) + "px; z-index: 1000; background: white; outline: none;" +
      "border-width: 0; outline: none; overflow: hidden; opacity: .05; filter: alpha(opacity=5);";
    focusInput(cm);
    resetInput(cm, true);
    // Adds "Select all" to context menu in FF
    if (posEq(sel.from, sel.to)) display.input.value = display.prevInput = " ";

    function rehide() {
      display.inputDiv.style.position = "relative";
      display.input.style.cssText = oldCSS;
      if (ie_lt9) display.scrollbarV.scrollTop = display.scroller.scrollTop = scrollPos;
      slowPoll(cm);

      // Try to detect the user choosing select-all 
      if (display.input.selectionStart != null) {
        clearTimeout(detectingSelectAll);
        var extval = display.input.value = " " + (posEq(sel.from, sel.to) ? "" : display.input.value), i = 0;
        display.prevInput = " ";
        display.input.selectionStart = 1; display.input.selectionEnd = extval.length;
        detectingSelectAll = setTimeout(function poll(){
          if (display.prevInput == " " && display.input.selectionStart == 0)
            operation(cm, commands.selectAll)(cm);
          else if (i++ < 10) detectingSelectAll = setTimeout(poll, 500);
          else resetInput(cm);
        }, 200);
      }
    }

    if (gecko) {
      e_stop(e);
      on(window, "mouseup", function mouseup() {
        off(window, "mouseup", mouseup);
        setTimeout(rehide, 20);
      });
    } else {
      setTimeout(rehide, 50);
    }
  }

  // UPDATING

  // Replace the range from from to to by the strings in newText.
  // Afterwards, set the selection to selFrom, selTo.
  function updateDoc(cm, from, to, newText, selUpdate, origin) {
    // Possibly split or suppress the update based on the presence
    // of read-only spans in its range.
    var split = sawReadOnlySpans &&
      removeReadOnlyRanges(cm.view.doc, from, to);
    if (split) {
      for (var i = split.length - 1; i >= 1; --i)
        updateDocInner(cm, split[i].from, split[i].to, [""], origin);
      if (split.length)
        return updateDocInner(cm, split[0].from, split[0].to, newText, selUpdate, origin);
    } else {
      return updateDocInner(cm, from, to, newText, selUpdate, origin);
    }
  }

  function updateDocInner(cm, from, to, newText, selUpdate, origin) {
    if (cm.view.suppressEdits) return;

    var view = cm.view, doc = view.doc, old = [];
    doc.iter(from.line, to.line + 1, function(line) {
      old.push(newHL(line.text, line.markedSpans));
    });
    var startSelFrom = view.sel.from, startSelTo = view.sel.to;
    var lines = updateMarkedSpans(hlSpans(old[0]), hlSpans(lst(old)), from.ch, to.ch, newText);
    var retval = updateDocNoUndo(cm, from, to, lines, selUpdate, origin);
    if (view.history) addChange(cm, from.line, newText.length, old, origin,
                                startSelFrom, startSelTo, view.sel.from, view.sel.to);
    return retval;
  }

  function unredoHelper(cm, type) {
    var doc = cm.view.doc, hist = cm.view.history;
    var set = (type == "undo" ? hist.done : hist.undone).pop();
    if (!set) return;
    var anti = {events: [], fromBefore: set.fromAfter, toBefore: set.toAfter,
                fromAfter: set.fromBefore, toAfter: set.toBefore};
    for (var i = set.events.length - 1; i >= 0; i -= 1) {
      hist.dirtyCounter += type == "undo" ? -1 : 1;
      var change = set.events[i];
      var replaced = [], end = change.start + change.added;
      doc.iter(change.start, end, function(line) { replaced.push(newHL(line.text, line.markedSpans)); });
      anti.events.push({start: change.start, added: change.old.length, old: replaced});
      var selPos = i ? null : {from: set.fromBefore, to: set.toBefore};
      updateDocNoUndo(cm, {line: change.start, ch: 0}, {line: end - 1, ch: getLine(doc, end-1).text.length},
                      change.old, selPos, type);
    }
    (type == "undo" ? hist.undone : hist.done).push(anti);
  }

  function updateDocNoUndo(cm, from, to, lines, selUpdate, origin) {
    var view = cm.view, doc = view.doc, display = cm.display;
    if (view.suppressEdits) return;

    var nlines = to.line - from.line, firstLine = getLine(doc, from.line), lastLine = getLine(doc, to.line);
    var recomputeMaxLength = false, checkWidthStart = from.line;
    if (!cm.options.lineWrapping) {
      checkWidthStart = lineNo(visualLine(doc, firstLine));
      doc.iter(checkWidthStart, to.line + 1, function(line) {
        if (lineLength(doc, line) == view.maxLineLength) {
          recomputeMaxLength = true;
          return true;
        }
      });
    }

    var lastHL = lst(lines), th = textHeight(display);

    // First adjust the line structure
    if (from.ch == 0 && to.ch == 0 && hlText(lastHL) == "") {
      // This is a whole-line replace. Treated specially to make
      // sure line objects move the way they are supposed to.
      var added = [];
      for (var i = 0, e = lines.length - 1; i < e; ++i)
        added.push(makeLine(hlText(lines[i]), hlSpans(lines[i]), th));
      updateLine(cm, lastLine, lastLine.text, hlSpans(lastHL));
      if (nlines) doc.remove(from.line, nlines, cm);
      if (added.length) doc.insert(from.line, added);
    } else if (firstLine == lastLine) {
      if (lines.length == 1) {
        updateLine(cm, firstLine, firstLine.text.slice(0, from.ch) + hlText(lines[0]) +
                   firstLine.text.slice(to.ch), hlSpans(lines[0]));
      } else {
        for (var added = [], i = 1, e = lines.length - 1; i < e; ++i)
          added.push(makeLine(hlText(lines[i]), hlSpans(lines[i]), th));
        added.push(makeLine(hlText(lastHL) + firstLine.text.slice(to.ch), hlSpans(lastHL), th));
        updateLine(cm, firstLine, firstLine.text.slice(0, from.ch) + hlText(lines[0]), hlSpans(lines[0]));
        doc.insert(from.line + 1, added);
      }
    } else if (lines.length == 1) {
      updateLine(cm, firstLine, firstLine.text.slice(0, from.ch) + hlText(lines[0]) +
                 lastLine.text.slice(to.ch), hlSpans(lines[0]));
      doc.remove(from.line + 1, nlines, cm);
    } else {
      var added = [];
      updateLine(cm, firstLine, firstLine.text.slice(0, from.ch) + hlText(lines[0]), hlSpans(lines[0]));
      updateLine(cm, lastLine, hlText(lastHL) + lastLine.text.slice(to.ch), hlSpans(lastHL));
      for (var i = 1, e = lines.length - 1; i < e; ++i)
        added.push(makeLine(hlText(lines[i]), hlSpans(lines[i]), th));
      if (nlines > 1) doc.remove(from.line + 1, nlines - 1, cm);
      doc.insert(from.line + 1, added);
    }

    if (cm.options.lineWrapping) {
      var perLine = Math.max(5, display.scroller.clientWidth / charWidth(display) - 3);
      doc.iter(from.line, from.line + lines.length, function(line) {
        if (line.height == 0) return;
        var guess = (Math.ceil(line.text.length / perLine) || 1) * th;
        if (guess != line.height) updateLineHeight(line, guess);
      });
    } else {
      doc.iter(checkWidthStart, from.line + lines.length, function(line) {
        var len = lineLength(doc, line);
        if (len > view.maxLineLength) {
          view.maxLine = line;
          view.maxLineLength = len;
          view.maxLineChanged = true;
          recomputeMaxLength = false;
        }
      });
      if (recomputeMaxLength) cm.curOp.updateMaxLine = true;
    }

    // Adjust frontier, schedule worker
    view.frontier = Math.min(view.frontier, from.line);
    startWorker(cm, 400);

    var lendiff = lines.length - nlines - 1;
    // Remember that these lines changed, for updating the display
    regChange(cm, from.line, to.line + 1, lendiff);
    if (hasHandler(cm, "change")) {
      // Normalize lines to contain only strings, since that's what
      // the change event handler expects
      for (var i = 0; i < lines.length; ++i)
        if (typeof lines[i] != "string") lines[i] = lines[i].text;
      var changeObj = {from: from, to: to, text: lines, origin: origin};
      if (cm.curOp.textChanged) {
        for (var cur = cm.curOp.textChanged; cur.next; cur = cur.next) {}
        cur.next = changeObj;
      } else cm.curOp.textChanged = changeObj;
    }

    // Update the selection
    var newSelFrom, newSelTo, end = {line: from.line + lines.length - 1,
                                     ch: hlText(lastHL).length  + (lines.length == 1 ? from.ch : 0)};
    if (selUpdate && typeof selUpdate != "string") {
      if (selUpdate.from) { newSelFrom = selUpdate.from; newSelTo = selUpdate.to; }
      else newSelFrom = newSelTo = selUpdate;
    } else if (selUpdate == "end") {
      newSelFrom = newSelTo = end;
    } else if (selUpdate == "start") {
      newSelFrom = newSelTo = from;
    } else if (selUpdate == "around") {
      newSelFrom = from; newSelTo = end;
    } else {
      var adjustPos = function(pos) {
        if (posLess(pos, from)) return pos;
        if (!posLess(to, pos)) return end;
        var line = pos.line + lendiff;
        var ch = pos.ch;
        if (pos.line == to.line)
          ch += hlText(lastHL).length - (to.ch - (to.line == from.line ? from.ch : 0));
        return {line: line, ch: ch};
      };
      newSelFrom = adjustPos(view.sel.from);
      newSelTo = adjustPos(view.sel.to);
    }
    setSelection(cm, newSelFrom, newSelTo, null, true);
    return end;
  }

  function replaceRange(cm, code, from, to, origin) {
    if (!to) to = from;
    if (posLess(to, from)) { var tmp = to; to = from; from = tmp; }
    return updateDoc(cm, from, to, splitLines(code), null, origin);
  }

  // SELECTION

  function posEq(a, b) {return a.line == b.line && a.ch == b.ch;}
  function posLess(a, b) {return a.line < b.line || (a.line == b.line && a.ch < b.ch);}
  function copyPos(x) {return {line: x.line, ch: x.ch};}

  function clipLine(doc, n) {return Math.max(0, Math.min(n, doc.size-1));}
  function clipPos(doc, pos) {
    if (pos.line < 0) return {line: 0, ch: 0};
    if (pos.line >= doc.size) return {line: doc.size-1, ch: getLine(doc, doc.size-1).text.length};
    var ch = pos.ch, linelen = getLine(doc, pos.line).text.length;
    if (ch == null || ch > linelen) return {line: pos.line, ch: linelen};
    else if (ch < 0) return {line: pos.line, ch: 0};
    else return pos;
  }
  function isLine(doc, l) {return l >= 0 && l < doc.size;}

  // If shift is held, this will move the selection anchor. Otherwise,
  // it'll set the whole selection.
  function extendSelection(cm, pos, other, bias) {
    var sel = cm.view.sel;
    if (sel.shift || sel.extend) {
      var anchor = sel.anchor;
      if (other) {
        var posBefore = posLess(pos, anchor);
        if (posBefore != posLess(other, anchor)) {
          anchor = pos;
          pos = other;
        } else if (posBefore != posLess(pos, other)) {
          pos = other;
        }
      }
      setSelection(cm, anchor, pos, bias);
    } else {
      setSelection(cm, pos, other || pos, bias);
    }
    cm.curOp.userSelChange = true;
  }

  // Update the selection. Last two args are only used by
  // updateDoc, since they have to be expressed in the line
  // numbers before the update.
  function setSelection(cm, anchor, head, bias, checkAtomic) {
    cm.view.goalColumn = null;
    var sel = cm.view.sel;
    // Skip over atomic spans.
    if (checkAtomic || !posEq(anchor, sel.anchor))
      anchor = skipAtomic(cm, anchor, bias, checkAtomic != "push");
    if (checkAtomic || !posEq(head, sel.head))
      head = skipAtomic(cm, head, bias, checkAtomic != "push");

    if (posEq(sel.anchor, anchor) && posEq(sel.head, head)) return;

    sel.anchor = anchor; sel.head = head;
    var inv = posLess(head, anchor);
    sel.from = inv ? head : anchor;
    sel.to = inv ? anchor : head;

    cm.curOp.updateInput = true;
    cm.curOp.selectionChanged = true;
  }

  function reCheckSelection(cm) {
    setSelection(cm, cm.view.sel.from, cm.view.sel.to, null, "push");
  }

  function skipAtomic(cm, pos, bias, mayClear) {
    var doc = cm.view.doc, flipped = false, curPos = pos;
    var dir = bias || 1;
    cm.view.cantEdit = false;
    search: for (;;) {
      var line = getLine(doc, curPos.line), toClear;
      if (line.markedSpans) {
        for (var i = 0; i < line.markedSpans.length; ++i) {
          var sp = line.markedSpans[i], m = sp.marker;
          if ((sp.from == null || (m.inclusiveLeft ? sp.from <= curPos.ch : sp.from < curPos.ch)) &&
              (sp.to == null || (m.inclusiveRight ? sp.to >= curPos.ch : sp.to > curPos.ch))) {
            if (mayClear && m.clearOnEnter) {
              (toClear || (toClear = [])).push(m);
              continue;
            } else if (!m.atomic) continue;
            var newPos = m.find()[dir < 0 ? "from" : "to"];
            if (posEq(newPos, curPos)) {
              newPos.ch += dir;
              if (newPos.ch < 0) {
                if (newPos.line) newPos = clipPos(doc, {line: newPos.line - 1});
                else newPos = null;
              } else if (newPos.ch > line.text.length) {
                if (newPos.line < doc.size - 1) newPos = {line: newPos.line + 1, ch: 0};
                else newPos = null;
              }
              if (!newPos) {
                if (flipped) {
                  // Driven in a corner -- no valid cursor position found at all
                  // -- try again *with* clearing, if we didn't already
                  if (!mayClear) return skipAtomic(cm, pos, bias, true);
                  // Otherwise, turn off editing until further notice, and return the start of the doc
                  cm.view.cantEdit = true;
                  return {line: 0, ch: 0};
                }
                flipped = true; newPos = pos; dir = -dir;
              }
            }
            curPos = newPos;
            continue search;
          }
        }
        if (toClear) for (var i = 0; i < toClear.length; ++i) toClear[i].clear();
      }
      return curPos;
    }
  }

  // SCROLLING

  function scrollCursorIntoView(cm) {
    var view = cm.view;
    var coords = scrollPosIntoView(cm, view.sel.head);
    if (!view.focused) return;
    var display = cm.display, box = display.sizer.getBoundingClientRect(), doScroll = null;
    if (coords.top + box.top < 0) doScroll = true;
    else if (coords.bottom + box.top > (window.innerHeight || document.documentElement.clientHeight)) doScroll = false;
    if (doScroll != null && !phantom) {
      var hidden = display.cursor.style.display == "none";
      if (hidden) {
        display.cursor.style.display = "";
        display.cursor.style.left = coords.left + "px";
        display.cursor.style.top = (coords.top - display.viewOffset) + "px";
      }
      display.cursor.scrollIntoView(doScroll);
      if (hidden) display.cursor.style.display = "none";
    }
  }

  function scrollPosIntoView(cm, pos) {
    for (;;) {
      var changed = false, coords = cursorCoords(cm, pos);
      var scrollPos = calculateScrollPos(cm, coords.left, coords.top, coords.left, coords.bottom);
      var startTop = cm.view.scrollTop, startLeft = cm.view.scrollLeft;
      if (scrollPos.scrollTop != null) {
        setScrollTop(cm, scrollPos.scrollTop);
        if (Math.abs(cm.view.scrollTop - startTop) > 1) changed = true;
      }
      if (scrollPos.scrollLeft != null) {
        setScrollLeft(cm, scrollPos.scrollLeft);
        if (Math.abs(cm.view.scrollLeft - startLeft) > 1) changed = true;
      }
      if (!changed) return coords;
    }
  }

  function scrollIntoView(cm, x1, y1, x2, y2) {
    var scrollPos = calculateScrollPos(cm, x1, y1, x2, y2);
    if (scrollPos.scrollTop != null) setScrollTop(cm, scrollPos.scrollTop);
    if (scrollPos.scrollLeft != null) setScrollLeft(cm, scrollPos.scrollLeft);
  }

  function calculateScrollPos(cm, x1, y1, x2, y2) {
    var display = cm.display, pt = paddingTop(display);
    y1 += pt; y2 += pt;
    var screen = display.scroller.clientHeight - scrollerCutOff, screentop = display.scroller.scrollTop, result = {};
    var docBottom = cm.view.doc.height + 2 * pt;
    var atTop = y1 < pt + 10, atBottom = y2 + pt > docBottom - 10;
    if (y1 < screentop) result.scrollTop = atTop ? 0 : Math.max(0, y1);
    else if (y2 > screentop + screen) result.scrollTop = (atBottom ? docBottom : y2) - screen;

    var screenw = display.scroller.clientWidth - scrollerCutOff, screenleft = display.scroller.scrollLeft;
    x1 += display.gutters.offsetWidth; x2 += display.gutters.offsetWidth;
    var gutterw = display.gutters.offsetWidth;
    var atLeft = x1 < gutterw + 10;
    if (x1 < screenleft + gutterw || atLeft) {
      if (atLeft) x1 = 0;
      result.scrollLeft = Math.max(0, x1 - 10 - gutterw);
    } else if (x2 > screenw + screenleft - 3) {
      result.scrollLeft = x2 + 10 - screenw;
    }
    return result;
  }

  // API UTILITIES

  function indentLine(cm, n, how, aggressive) {
    var doc = cm.view.doc;
    if (!how) how = "add";
    if (how == "smart") {
      if (!cm.view.mode.indent) how = "prev";
      else var state = getStateBefore(cm, n);
    }

    var tabSize = cm.options.tabSize;
    var line = getLine(doc, n), curSpace = countColumn(line.text, null, tabSize);
    var curSpaceString = line.text.match(/^\s*/)[0], indentation;
    if (how == "smart") {
      indentation = cm.view.mode.indent(state, line.text.slice(curSpaceString.length), line.text);
      if (indentation == Pass) {
        if (!aggressive) return;
        how = "prev";
      }
    }
    if (how == "prev") {
      if (n) indentation = countColumn(getLine(doc, n-1).text, null, tabSize);
      else indentation = 0;
    }
    else if (how == "add") indentation = curSpace + cm.options.indentUnit;
    else if (how == "subtract") indentation = curSpace - cm.options.indentUnit;
    indentation = Math.max(0, indentation);

    var indentString = "", pos = 0;
    if (cm.options.indentWithTabs)
      for (var i = Math.floor(indentation / tabSize); i; --i) {pos += tabSize; indentString += "\t";}
    if (pos < indentation) indentString += spaceStr(indentation - pos);

    if (indentString != curSpaceString)
      replaceRange(cm, indentString, {line: n, ch: 0}, {line: n, ch: curSpaceString.length}, "input");
    line.stateAfter = null;
  }

  function changeLine(cm, handle, op) {
    var no = handle, line = handle, doc = cm.view.doc;
    if (typeof handle == "number") line = getLine(doc, clipLine(doc, handle));
    else no = lineNo(handle);
    if (no == null) return null;
    if (op(line, no)) regChange(cm, no, no + 1);
    else return null;
    return line;
  }

  function findPosH(cm, dir, unit, visually) {
    var doc = cm.view.doc, end = cm.view.sel.head, line = end.line, ch = end.ch;
    var lineObj = getLine(doc, line);
    function findNextLine() {
      var l = line + dir;
      if (l < 0 || l == doc.size) return false;
      line = l;
      return lineObj = getLine(doc, l);
    }
    function moveOnce(boundToLine) {
      var next = (visually ? moveVisually : moveLogically)(lineObj, ch, dir, true);
      if (next == null) {
        if (!boundToLine && findNextLine()) {
          if (visually) ch = (dir < 0 ? lineRight : lineLeft)(lineObj);
          else ch = dir < 0 ? lineObj.text.length : 0;
        } else return false;
      } else ch = next;
      return true;
    }
    if (unit == "char") moveOnce();
    else if (unit == "column") moveOnce(true);
    else if (unit == "word") {
      var sawWord = false;
      for (;;) {
        if (dir < 0) if (!moveOnce()) break;
        if (isWordChar(lineObj.text.charAt(ch))) sawWord = true;
        else if (sawWord) {if (dir < 0) {dir = 1; moveOnce();} break;}
        if (dir > 0) if (!moveOnce()) break;
      }
    }
    return skipAtomic(cm, {line: line, ch: ch}, dir, true);
  }

  function findWordAt(line, pos) {
    var start = pos.ch, end = pos.ch;
    if (line) {
      if (pos.after === false || end == line.length) --start; else ++end;
      var startChar = line.charAt(start);
      var check = isWordChar(startChar) ? isWordChar :
        /\s/.test(startChar) ? function(ch) {return /\s/.test(ch);} :
      function(ch) {return !/\s/.test(ch) && !isWordChar(ch);};
      while (start > 0 && check(line.charAt(start - 1))) --start;
      while (end < line.length && check(line.charAt(end))) ++end;
    }
    return {from: {line: pos.line, ch: start}, to: {line: pos.line, ch: end}};
  }

  function selectLine(cm, line) {
    extendSelection(cm, {line: line, ch: 0}, clipPos(cm.view.doc, {line: line + 1, ch: 0}));
  }

  // PROTOTYPE

  // The publicly visible API. Note that operation(null, f) means
  // 'wrap f in an operation, performed on its `this` parameter'

  CodeMirror.prototype = {
    getValue: function(lineSep) {
      var text = [], doc = this.view.doc;
      doc.iter(0, doc.size, function(line) { text.push(line.text); });
      return text.join(lineSep || "\n");
    },

    setValue: operation(null, function(code) {
      var doc = this.view.doc, top = {line: 0, ch: 0}, lastLen = getLine(doc, doc.size-1).text.length;
      updateDocInner(this, top, {line: doc.size - 1, ch: lastLen}, splitLines(code), top, top, "setValue");
    }),

    getSelection: function(lineSep) { return this.getRange(this.view.sel.from, this.view.sel.to, lineSep); },

    replaceSelection: operation(null, function(code, collapse, origin) {
      var sel = this.view.sel;
      updateDoc(this, sel.from, sel.to, splitLines(code), collapse || "around", origin);
    }),

    focus: function(){window.focus(); focusInput(this); onFocus(this); fastPoll(this);},

    setOption: function(option, value) {
      var options = this.options, old = options[option];
      if (options[option] == value && option != "mode") return;
      options[option] = value;
      if (optionHandlers.hasOwnProperty(option))
        operation(this, optionHandlers[option])(this, value, old);
    },

    getOption: function(option) {return this.options[option];},

    getMode: function() {return this.view.mode;},

    addKeyMap: function(map) {
      this.view.keyMaps.push(map);
    },

    removeKeyMap: function(map) {
      var maps = this.view.keyMaps;
      for (var i = 0; i < maps.length; ++i)
        if ((typeof map == "string" ? maps[i].name : maps[i]) == map) {
          maps.splice(i, 1);
          return true;
        }
    },

    undo: operation(null, function() {unredoHelper(this, "undo");}),
    redo: operation(null, function() {unredoHelper(this, "redo");}),

    indentLine: operation(null, function(n, dir, aggressive) {
      if (typeof dir != "string") {
        if (dir == null) dir = this.options.smartIndent ? "smart" : "prev";
        else dir = dir ? "add" : "subtract";
      }
      if (isLine(this.view.doc, n)) indentLine(this, n, dir, aggressive);
    }),

    indentSelection: operation(null, function(how) {
      var sel = this.view.sel;
      if (posEq(sel.from, sel.to)) return indentLine(this, sel.from.line, how);
      var e = sel.to.line - (sel.to.ch ? 0 : 1);
      for (var i = sel.from.line; i <= e; ++i) indentLine(this, i, how);
    }),

    historySize: function() {
      var hist = this.view.history;
      return {undo: hist.done.length, redo: hist.undone.length};
    },

    clearHistory: function() {this.view.history = makeHistory();},

    markClean: function() {
      this.view.history.dirtyCounter = 0;
      this.view.history.lastOp = this.view.history.lastOrigin = null;
    },

    isClean: function () {return this.view.history.dirtyCounter == 0;},
      
    getHistory: function() {
      var hist = this.view.history;
      function cp(arr) {
        for (var i = 0, nw = [], nwelt; i < arr.length; ++i) {
          var set = arr[i];
          nw.push({events: nwelt = [], fromBefore: set.fromBefore, toBefore: set.toBefore,
                   fromAfter: set.fromAfter, toAfter: set.toAfter});
          for (var j = 0, elt = set.events; j < elt.length; ++j) {
            var old = [], cur = elt[j];
            nwelt.push({start: cur.start, added: cur.added, old: old});
            for (var k = 0; k < cur.old.length; ++k) old.push(hlText(cur.old[k]));
          }
        }
        return nw;
      }
      return {done: cp(hist.done), undone: cp(hist.undone)};
    },

    setHistory: function(histData) {
      var hist = this.view.history = makeHistory();
      hist.done = histData.done;
      hist.undone = histData.undone;
    },

    // Fetch the parser token for a given character. Useful for hacks
    // that want to inspect the mode state (say, for completion).
    getTokenAt: function(pos) {
      var doc = this.view.doc;
      pos = clipPos(doc, pos);
      var state = getStateBefore(this, pos.line), mode = this.view.mode;
      var line = getLine(doc, pos.line);
      var stream = new StringStream(line.text, this.options.tabSize);
      while (stream.pos < pos.ch && !stream.eol()) {
        stream.start = stream.pos;
        var style = mode.token(stream, state);
      }
      return {start: stream.start,
              end: stream.pos,
              string: stream.current(),
              className: style || null, // Deprecated, use 'type' instead
              type: style || null,
              state: state};
    },

    getStateAfter: function(line) {
      var doc = this.view.doc;
      line = clipLine(doc, line == null ? doc.size - 1: line);
      return getStateBefore(this, line + 1);
    },

    cursorCoords: function(start, mode) {
      var pos, sel = this.view.sel;
      if (start == null) pos = sel.head;
      else if (typeof start == "object") pos = clipPos(this.view.doc, start);
      else pos = start ? sel.from : sel.to;
      return cursorCoords(this, pos, mode || "page");
    },

    charCoords: function(pos, mode) {
      return charCoords(this, clipPos(this.view.doc, pos), mode || "page");
    },

    coordsChar: function(coords) {
      var off = this.display.lineSpace.getBoundingClientRect();
      return coordsChar(this, coords.left - off.left, coords.top - off.top);
    },

    defaultTextHeight: function() { return textHeight(this.display); },

    markText: operation(null, function(from, to, options) {
      return markText(this, clipPos(this.view.doc, from), clipPos(this.view.doc, to),
                      options, "range");
    }),

    setBookmark: operation(null, function(pos, widget) {
      pos = clipPos(this.view.doc, pos);
      return markText(this, pos, pos, widget ? {replacedWith: widget} : {}, "bookmark");
    }),

    findMarksAt: function(pos) {
      var doc = this.view.doc;
      pos = clipPos(doc, pos);
      var markers = [], spans = getLine(doc, pos.line).markedSpans;
      if (spans) for (var i = 0; i < spans.length; ++i) {
        var span = spans[i];
        if ((span.from == null || span.from <= pos.ch) &&
            (span.to == null || span.to >= pos.ch))
          markers.push(span.marker);
      }
      return markers;
    },

    setGutterMarker: operation(null, function(line, gutterID, value) {
      return changeLine(this, line, function(line) {
        var markers = line.gutterMarkers || (line.gutterMarkers = {});
        markers[gutterID] = value;
        if (!value && isEmpty(markers)) line.gutterMarkers = null;
        return true;
      });
    }),

    clearGutter: operation(null, function(gutterID) {
      var i = 0, cm = this, doc = cm.view.doc;
      doc.iter(0, doc.size, function(line) {
        if (line.gutterMarkers && line.gutterMarkers[gutterID]) {
          line.gutterMarkers[gutterID] = null;
          regChange(cm, i, i + 1);
          if (isEmpty(line.gutterMarkers)) line.gutterMarkers = null;
        }
        ++i;
      });
    }),

    addLineClass: operation(null, function(handle, where, cls) {
      return changeLine(this, handle, function(line) {
        var prop = where == "text" ? "textClass" : where == "background" ? "bgClass" : "wrapClass";
        if (!line[prop]) line[prop] = cls;
        else if (new RegExp("\\b" + cls + "\\b").test(line[prop])) return false;
        else line[prop] += " " + cls;
        return true;
      });
    }),

    removeLineClass: operation(null, function(handle, where, cls) {
      return changeLine(this, handle, function(line) {
        var prop = where == "text" ? "textClass" : where == "background" ? "bgClass" : "wrapClass";
        var cur = line[prop];
        if (!cur) return false;
        else if (cls == null) line[prop] = null;
        else {
          var upd = cur.replace(new RegExp("^" + cls + "\\b\\s*|\\s*\\b" + cls + "\\b"), "");
          if (upd == cur) return false;
          line[prop] = upd || null;
        }
        return true;
      });
    }),

    addLineWidget: operation(null, function(handle, node, options) {
      var widget = options || {};
      widget.node = node;
      if (widget.noHScroll) this.display.alignWidgets = true;
      changeLine(this, handle, function(line) {
        (line.widgets || (line.widgets = [])).push(widget);
        widget.line = line;
        return true;
      });
      return widget;
    }),

    removeLineWidget: operation(null, function(widget) {
      var ws = widget.line.widgets, no = lineNo(widget.line);
      if (no == null) return;
      for (var i = 0; i < ws.length; ++i) if (ws[i] == widget) ws.splice(i--, 1);
      regChange(this, no, no + 1);
    }),

    lineInfo: function(line) {
      if (typeof line == "number") {
        if (!isLine(this.view.doc, line)) return null;
        var n = line;
        line = getLine(this.view.doc, line);
        if (!line) return null;
      } else {
        var n = lineNo(line);
        if (n == null) return null;
      }
      return {line: n, handle: line, text: line.text, gutterMarkers: line.gutterMarkers,
              textClass: line.textClass, bgClass: line.bgClass, wrapClass: line.wrapClass,
              widgets: line.widgets};
    },

    getViewport: function() { return {from: this.display.showingFrom, to: this.display.showingTo};},

    addWidget: function(pos, node, scroll, vert, horiz) {
      var display = this.display;
      pos = cursorCoords(this, clipPos(this.view.doc, pos));
      var top = pos.top, left = pos.left;
      node.style.position = "absolute";
      display.sizer.appendChild(node);
      if (vert == "over") top = pos.top;
      else if (vert == "near") {
        var vspace = Math.max(display.wrapper.clientHeight, this.view.doc.height),
        hspace = Math.max(display.sizer.clientWidth, display.lineSpace.clientWidth);
        if (pos.bottom + node.offsetHeight > vspace && pos.top > node.offsetHeight)
          top = pos.top - node.offsetHeight;
        if (left + node.offsetWidth > hspace)
          left = hspace - node.offsetWidth;
      }
      node.style.top = (top + paddingTop(display)) + "px";
      node.style.left = node.style.right = "";
      if (horiz == "right") {
        left = display.sizer.clientWidth - node.offsetWidth;
        node.style.right = "0px";
      } else {
        if (horiz == "left") left = 0;
        else if (horiz == "middle") left = (display.sizer.clientWidth - node.offsetWidth) / 2;
        node.style.left = left + "px";
      }
      if (scroll)
        scrollIntoView(this, left, top, left + node.offsetWidth, top + node.offsetHeight);
    },

    lineCount: function() {return this.view.doc.size;},

    clipPos: function(pos) {return clipPos(this.view.doc, pos);},

    getCursor: function(start) {
      var sel = this.view.sel, pos;
      if (start == null || start == "head") pos = sel.head;
      else if (start == "anchor") pos = sel.anchor;
      else if (start == "end" || start === false) pos = sel.to;
      else pos = sel.from;
      return copyPos(pos);
    },

    somethingSelected: function() {return !posEq(this.view.sel.from, this.view.sel.to);},

    setCursor: operation(null, function(line, ch, extend) {
      var pos = clipPos(this.view.doc, typeof line == "number" ? {line: line, ch: ch || 0} : line);
      if (extend) extendSelection(this, pos);
      else setSelection(this, pos, pos);
    }),

    setSelection: operation(null, function(anchor, head) {
      var doc = this.view.doc;
      setSelection(this, clipPos(doc, anchor), clipPos(doc, head || anchor));
    }),

    extendSelection: operation(null, function(from, to) {
      var doc = this.view.doc;
      extendSelection(this, clipPos(doc, from), to && clipPos(doc, to));
    }),

    setExtending: function(val) {this.view.sel.extend = val;},

    getLine: function(line) {var l = this.getLineHandle(line); return l && l.text;},

    getLineHandle: function(line) {
      var doc = this.view.doc;
      if (isLine(doc, line)) return getLine(doc, line);
    },

    getLineNumber: function(line) {return lineNo(line);},

    setLine: operation(null, function(line, text) {
      if (isLine(this.view.doc, line))
        replaceRange(this, text, {line: line, ch: 0}, {line: line, ch: getLine(this.view.doc, line).text.length});
    }),

    removeLine: operation(null, function(line) {
      if (isLine(this.view.doc, line))
        replaceRange(this, "", {line: line, ch: 0}, clipPos(this.view.doc, {line: line+1, ch: 0}));
    }),

    replaceRange: operation(null, function(code, from, to) {
      var doc = this.view.doc;
      from = clipPos(doc, from);
      to = to ? clipPos(doc, to) : from;
      return replaceRange(this, code, from, to);
    }),

    getRange: function(from, to, lineSep) {
      var doc = this.view.doc;
      from = clipPos(doc, from); to = clipPos(doc, to);
      var l1 = from.line, l2 = to.line;
      if (l1 == l2) return getLine(doc, l1).text.slice(from.ch, to.ch);
      var code = [getLine(doc, l1).text.slice(from.ch)];
      doc.iter(l1 + 1, l2, function(line) { code.push(line.text); });
      code.push(getLine(doc, l2).text.slice(0, to.ch));
      return code.join(lineSep || "\n");
    },

    triggerOnKeyDown: operation(null, onKeyDown),

    execCommand: function(cmd) {return commands[cmd](this);},

    // Stuff used by commands, probably not much use to outside code.
    moveH: operation(null, function(dir, unit) {
      var sel = this.view.sel, pos = dir < 0 ? sel.from : sel.to;
      if (sel.shift || sel.extend || posEq(sel.from, sel.to)) pos = findPosH(this, dir, unit, true);
      extendSelection(this, pos, pos, dir);
    }),

    deleteH: operation(null, function(dir, unit) {
      var sel = this.view.sel;
      if (!posEq(sel.from, sel.to)) replaceRange(this, "", sel.from, sel.to, "delete");
      else replaceRange(this, "", sel.from, findPosH(this, dir, unit, false), "delete");
      this.curOp.userSelChange = true;
    }),

    moveV: operation(null, function(dir, unit) {
      var view = this.view, doc = view.doc, display = this.display;
      var cur = view.sel.head, pos = cursorCoords(this, cur, "div");
      var x = pos.left, y;
      if (view.goalColumn != null) x = view.goalColumn;
      if (unit == "page") {
        var pageSize = Math.min(display.wrapper.clientHeight, window.innerHeight || document.documentElement.clientHeight);
        y = pos.top + dir * pageSize;
      } else if (unit == "line") {
        y = dir > 0 ? pos.bottom + 3 : pos.top - 3;
      }
      do {
        var target = coordsChar(this, x, y);
        y += dir * 5;
      } while (target.outside && (dir < 0 ? y > 0 : y < doc.height));

      if (unit == "page") display.scrollbarV.scrollTop += charCoords(this, target, "div").top - pos.top;
      extendSelection(this, target, target, dir);
      view.goalColumn = x;
    }),

    toggleOverwrite: function() {
      if (this.view.overwrite = !this.view.overwrite)
        this.display.cursor.className += " CodeMirror-overwrite";
      else
        this.display.cursor.className = this.display.cursor.className.replace(" CodeMirror-overwrite", "");
    },

    posFromIndex: function(off) {
      var lineNo = 0, ch, doc = this.view.doc;
      doc.iter(0, doc.size, function(line) {
        var sz = line.text.length + 1;
        if (sz > off) { ch = off; return true; }
        off -= sz;
        ++lineNo;
      });
      return clipPos(doc, {line: lineNo, ch: ch});
    },
    indexFromPos: function (coords) {
      if (coords.line < 0 || coords.ch < 0) return 0;
      var index = coords.ch;
      this.view.doc.iter(0, coords.line, function (line) {
        index += line.text.length + 1;
      });
      return index;
    },

    scrollTo: function(x, y) {
      if (x != null) this.display.scrollbarH.scrollLeft = this.display.scroller.scrollLeft = x;
      if (y != null) this.display.scrollbarV.scrollTop = this.display.scroller.scrollTop = y;
      updateDisplay(this, []);
    },
    getScrollInfo: function() {
      var scroller = this.display.scroller, co = scrollerCutOff;
      return {left: scroller.scrollLeft, top: scroller.scrollTop,
              height: scroller.scrollHeight - co, width: scroller.scrollWidth - co,
              clientHeight: scroller.clientHeight - co, clientWidth: scroller.clientWidth - co};
    },

    scrollIntoView: function(pos) {
      if (typeof pos == "number") pos = {line: pos, ch: 0};
      pos = pos ? clipPos(this.view.doc, pos) : this.view.sel.head;
      scrollPosIntoView(this, pos);
    },

    setSize: function(width, height) {
      function interpret(val) {
        return typeof val == "number" || /^\d+$/.test(String(val)) ? val + "px" : val;
      }
      if (width != null) this.display.wrapper.style.width = interpret(width);
      if (height != null) this.display.wrapper.style.height = interpret(height);
      this.refresh();
    },

    on: function(type, f) {on(this, type, f);},
    off: function(type, f) {off(this, type, f);},

    operation: function(f){return operation(this, f)();},

    refresh: function() {
      clearCaches(this);
      if (this.display.scroller.scrollHeight > this.view.scrollTop)
        this.display.scrollbarV.scrollTop = this.display.scroller.scrollTop = this.view.scrollTop;
      updateDisplay(this, true);
    },

    getInputField: function(){return this.display.input;},
    getWrapperElement: function(){return this.display.wrapper;},
    getScrollerElement: function(){return this.display.scroller;},
    getGutterElement: function(){return this.display.gutters;}
  };

  // OPTION DEFAULTS

  var optionHandlers = CodeMirror.optionHandlers = {};

  // The default configuration options.
  var defaults = CodeMirror.defaults = {};

  function option(name, deflt, handle, notOnInit) {
    CodeMirror.defaults[name] = deflt;
    if (handle) optionHandlers[name] =
      notOnInit ? function(cm, val, old) {if (old != Init) handle(cm, val, old);} : handle;
  }

  var Init = CodeMirror.Init = {toString: function(){return "CodeMirror.Init";}};

  // These two are, on init, called from the constructor because they
  // have to be initialized before the editor can start at all.
  option("value", "", function(cm, val) {cm.setValue(val);}, true);
  option("mode", null, loadMode, true);

  option("indentUnit", 2, loadMode, true);
  option("indentWithTabs", false);
  option("smartIndent", true);
  option("tabSize", 4, function(cm) {
    loadMode(cm);
    clearCaches(cm);
    updateDisplay(cm, true);
  }, true);
  option("electricChars", true);

  option("theme", "default", function(cm) {
    themeChanged(cm);
    guttersChanged(cm);
  }, true);
  option("keyMap", "default", keyMapChanged);
  option("extraKeys", null);

  option("onKeyEvent", null);
  option("onDragEvent", null);

  option("lineWrapping", false, wrappingChanged, true);
  option("gutters", [], function(cm) {
    setGuttersForLineNumbers(cm.options);
    guttersChanged(cm);
  }, true);
  option("lineNumbers", false, function(cm) {
    setGuttersForLineNumbers(cm.options);
    guttersChanged(cm);
  }, true);
  option("firstLineNumber", 1, guttersChanged, true);
  option("lineNumberFormatter", function(integer) {return integer;}, guttersChanged, true);
  option("showCursorWhenSelecting", false, updateSelection, true);
  
  option("readOnly", false, function(cm, val) {
    if (val == "nocursor") {onBlur(cm); cm.display.input.blur();}
    else if (!val) resetInput(cm, true);
  });
  option("dragDrop", true);

  option("cursorBlinkRate", 530);
  option("cursorHeight", 1);
  option("workTime", 100);
  option("workDelay", 100);
  option("flattenSpans", true);
  option("pollInterval", 100);
  option("undoDepth", 40);
  option("viewportMargin", 10, function(cm){cm.refresh();}, true);

  option("tabindex", null, function(cm, val) {
    cm.display.input.tabIndex = val || "";
  });
  option("autofocus", null);

  // MODE DEFINITION AND QUERYING

  // Known modes, by name and by MIME
  var modes = CodeMirror.modes = {}, mimeModes = CodeMirror.mimeModes = {};

  CodeMirror.defineMode = function(name, mode) {
    if (!CodeMirror.defaults.mode && name != "null") CodeMirror.defaults.mode = name;
    if (arguments.length > 2) {
      mode.dependencies = [];
      for (var i = 2; i < arguments.length; ++i) mode.dependencies.push(arguments[i]);
    }
    modes[name] = mode;
  };

  CodeMirror.defineMIME = function(mime, spec) {
    mimeModes[mime] = spec;
  };

  CodeMirror.resolveMode = function(spec) {
    if (typeof spec == "string" && mimeModes.hasOwnProperty(spec))
      spec = mimeModes[spec];
    else if (typeof spec == "string" && /^[\w\-]+\/[\w\-]+\+xml$/.test(spec))
      return CodeMirror.resolveMode("application/xml");
    if (typeof spec == "string") return {name: spec};
    else return spec || {name: "null"};
  };

  CodeMirror.getMode = function(options, spec) {
    var spec = CodeMirror.resolveMode(spec);
    var mfactory = modes[spec.name];
    if (!mfactory) return CodeMirror.getMode(options, "text/plain");
    var modeObj = mfactory(options, spec);
    if (modeExtensions.hasOwnProperty(spec.name)) {
      var exts = modeExtensions[spec.name];
      for (var prop in exts) {
        if (!exts.hasOwnProperty(prop)) continue;
        if (modeObj.hasOwnProperty(prop)) modeObj["_" + prop] = modeObj[prop];
        modeObj[prop] = exts[prop];
      }
    }
    modeObj.name = spec.name;
    return modeObj;
  };

  CodeMirror.defineMode("null", function() {
    return {token: function(stream) {stream.skipToEnd();}};
  });
  CodeMirror.defineMIME("text/plain", "null");

  var modeExtensions = CodeMirror.modeExtensions = {};
  CodeMirror.extendMode = function(mode, properties) {
    var exts = modeExtensions.hasOwnProperty(mode) ? modeExtensions[mode] : (modeExtensions[mode] = {});
    for (var prop in properties) if (properties.hasOwnProperty(prop))
      exts[prop] = properties[prop];
  };

  // EXTENSIONS

  CodeMirror.defineExtension = function(name, func) {
    CodeMirror.prototype[name] = func;
  };

  CodeMirror.defineOption = option;

  var initHooks = [];
  CodeMirror.defineInitHook = function(f) {initHooks.push(f);};

  // MODE STATE HANDLING

  // Utility functions for working with state. Exported because modes
  // sometimes need to do this.
  function copyState(mode, state) {
    if (state === true) return state;
    if (mode.copyState) return mode.copyState(state);
    var nstate = {};
    for (var n in state) {
      var val = state[n];
      if (val instanceof Array) val = val.concat([]);
      nstate[n] = val;
    }
    return nstate;
  }
  CodeMirror.copyState = copyState;

  function startState(mode, a1, a2) {
    return mode.startState ? mode.startState(a1, a2) : true;
  }
  CodeMirror.startState = startState;

  CodeMirror.innerMode = function(mode, state) {
    while (mode.innerMode) {
      var info = mode.innerMode(state);
      state = info.state;
      mode = info.mode;
    }
    return info || {mode: mode, state: state};
  };

  // STANDARD COMMANDS

  var commands = CodeMirror.commands = {
    selectAll: function(cm) {cm.setSelection({line: 0, ch: 0}, {line: cm.lineCount() - 1});},
    killLine: function(cm) {
      var from = cm.getCursor(true), to = cm.getCursor(false), sel = !posEq(from, to);
      if (!sel && cm.getLine(from.line).length == from.ch)
        cm.replaceRange("", from, {line: from.line + 1, ch: 0}, "delete");
      else cm.replaceRange("", from, sel ? to : {line: from.line}, "delete");
    },
    deleteLine: function(cm) {
      var l = cm.getCursor().line;
      cm.replaceRange("", {line: l, ch: 0}, {line: l}, "delete");
    },
    undo: function(cm) {cm.undo();},
    redo: function(cm) {cm.redo();},
    goDocStart: function(cm) {cm.extendSelection({line: 0, ch: 0});},
    goDocEnd: function(cm) {cm.extendSelection({line: cm.lineCount() - 1});},
    goLineStart: function(cm) {
      cm.extendSelection(lineStart(cm, cm.getCursor().line));
    },
    goLineStartSmart: function(cm) {
      var cur = cm.getCursor(), start = lineStart(cm, cur.line);
      var line = cm.getLineHandle(start.line);
      var order = getOrder(line);
      if (!order || order[0].level == 0) {
        var firstNonWS = Math.max(0, line.text.search(/\S/));
        var inWS = cur.line == start.line && cur.ch <= firstNonWS && cur.ch;
        cm.extendSelection({line: start.line, ch: inWS ? 0 : firstNonWS});
      } else cm.extendSelection(start);
    },
    goLineEnd: function(cm) {
      cm.extendSelection(lineEnd(cm, cm.getCursor().line));
    },
    goLineUp: function(cm) {cm.moveV(-1, "line");},
    goLineDown: function(cm) {cm.moveV(1, "line");},
    goPageUp: function(cm) {cm.moveV(-1, "page");},
    goPageDown: function(cm) {cm.moveV(1, "page");},
    goCharLeft: function(cm) {cm.moveH(-1, "char");},
    goCharRight: function(cm) {cm.moveH(1, "char");},
    goColumnLeft: function(cm) {cm.moveH(-1, "column");},
    goColumnRight: function(cm) {cm.moveH(1, "column");},
    goWordLeft: function(cm) {cm.moveH(-1, "word");},
    goWordRight: function(cm) {cm.moveH(1, "word");},
    delCharBefore: function(cm) {cm.deleteH(-1, "char");},
    delCharAfter: function(cm) {cm.deleteH(1, "char");},
    delWordBefore: function(cm) {cm.deleteH(-1, "word");},
    delWordAfter: function(cm) {cm.deleteH(1, "word");},
    indentAuto: function(cm) {cm.indentSelection("smart");},
    indentMore: function(cm) {cm.indentSelection("add");},
    indentLess: function(cm) {cm.indentSelection("subtract");},
    insertTab: function(cm) {cm.replaceSelection("\t", "end", "input");},
    defaultTab: function(cm) {
      if (cm.somethingSelected()) cm.indentSelection("add");
      else cm.replaceSelection("\t", "end", "input");
    },
    transposeChars: function(cm) {
      var cur = cm.getCursor(), line = cm.getLine(cur.line);
      if (cur.ch > 0 && cur.ch < line.length - 1)
        cm.replaceRange(line.charAt(cur.ch) + line.charAt(cur.ch - 1),
                        {line: cur.line, ch: cur.ch - 1}, {line: cur.line, ch: cur.ch + 1});
    },
    newlineAndIndent: function(cm) {
      operation(cm, function() {
        cm.replaceSelection("\n", "end", "input");
        cm.indentLine(cm.getCursor().line, null, true);
      })();
    },
    toggleOverwrite: function(cm) {cm.toggleOverwrite();}
  };

  // STANDARD KEYMAPS

  var keyMap = CodeMirror.keyMap = {};
  keyMap.basic = {
    "Left": "goCharLeft", "Right": "goCharRight", "Up": "goLineUp", "Down": "goLineDown",
    "End": "goLineEnd", "Home": "goLineStartSmart", "PageUp": "goPageUp", "PageDown": "goPageDown",
    "Delete": "delCharAfter", "Backspace": "delCharBefore", "Tab": "defaultTab", "Shift-Tab": "indentAuto",
    "Enter": "newlineAndIndent", "Insert": "toggleOverwrite"
  };
  // Note that the save and find-related commands aren't defined by
  // default. Unknown commands are simply ignored.
  keyMap.pcDefault = {
    "Ctrl-A": "selectAll", "Ctrl-D": "deleteLine", "Ctrl-Z": "undo", "Shift-Ctrl-Z": "redo", "Ctrl-Y": "redo",
    "Ctrl-Home": "goDocStart", "Alt-Up": "goDocStart", "Ctrl-End": "goDocEnd", "Ctrl-Down": "goDocEnd",
    "Ctrl-Left": "goWordLeft", "Ctrl-Right": "goWordRight", "Alt-Left": "goLineStart", "Alt-Right": "goLineEnd",
    "Ctrl-Backspace": "delWordBefore", "Ctrl-Delete": "delWordAfter", "Ctrl-S": "save", "Ctrl-F": "find",
    "Ctrl-G": "findNext", "Shift-Ctrl-G": "findPrev", "Shift-Ctrl-F": "replace", "Shift-Ctrl-R": "replaceAll",
    "Ctrl-[": "indentLess", "Ctrl-]": "indentMore",
    fallthrough: "basic"
  };
  keyMap.macDefault = {
    "Cmd-A": "selectAll", "Cmd-D": "deleteLine", "Cmd-Z": "undo", "Shift-Cmd-Z": "redo", "Cmd-Y": "redo",
    "Cmd-Up": "goDocStart", "Cmd-End": "goDocEnd", "Cmd-Down": "goDocEnd", "Alt-Left": "goWordLeft",
    "Alt-Right": "goWordRight", "Cmd-Left": "goLineStart", "Cmd-Right": "goLineEnd", "Alt-Backspace": "delWordBefore",
    "Ctrl-Alt-Backspace": "delWordAfter", "Alt-Delete": "delWordAfter", "Cmd-S": "save", "Cmd-F": "find",
    "Cmd-G": "findNext", "Shift-Cmd-G": "findPrev", "Cmd-Alt-F": "replace", "Shift-Cmd-Alt-F": "replaceAll",
    "Cmd-[": "indentLess", "Cmd-]": "indentMore",
    fallthrough: ["basic", "emacsy"]
  };
  keyMap["default"] = mac ? keyMap.macDefault : keyMap.pcDefault;
  keyMap.emacsy = {
    "Ctrl-F": "goCharRight", "Ctrl-B": "goCharLeft", "Ctrl-P": "goLineUp", "Ctrl-N": "goLineDown",
    "Alt-F": "goWordRight", "Alt-B": "goWordLeft", "Ctrl-A": "goLineStart", "Ctrl-E": "goLineEnd",
    "Ctrl-V": "goPageDown", "Shift-Ctrl-V": "goPageUp", "Ctrl-D": "delCharAfter", "Ctrl-H": "delCharBefore",
    "Alt-D": "delWordAfter", "Alt-Backspace": "delWordBefore", "Ctrl-K": "killLine", "Ctrl-T": "transposeChars"
  };

  // KEYMAP DISPATCH

  function getKeyMap(val) {
    if (typeof val == "string") return keyMap[val];
    else return val;
  }

  function lookupKey(name, maps, handle, stop) {
    function lookup(map) {
      map = getKeyMap(map);
      var found = map[name];
      if (found === false) {
        if (stop) stop();
        return true;
      }
      if (found != null && handle(found)) return true;
      if (map.nofallthrough) {
        if (stop) stop();
        return true;
      }
      var fallthrough = map.fallthrough;
      if (fallthrough == null) return false;
      if (Object.prototype.toString.call(fallthrough) != "[object Array]")
        return lookup(fallthrough);
      for (var i = 0, e = fallthrough.length; i < e; ++i) {
        if (lookup(fallthrough[i])) return true;
      }
      return false;
    }

    for (var i = 0; i < maps.length; ++i)
      if (lookup(maps[i])) return true;
  }
  function isModifierKey(event) {
    var name = keyNames[e_prop(event, "keyCode")];
    return name == "Ctrl" || name == "Alt" || name == "Shift" || name == "Mod";
  }
  CodeMirror.isModifierKey = isModifierKey;

  // FROMTEXTAREA

  CodeMirror.fromTextArea = function(textarea, options) {
    if (!options) options = {};
    options.value = textarea.value;
    if (!options.tabindex && textarea.tabindex)
      options.tabindex = textarea.tabindex;
    // Set autofocus to true if this textarea is focused, or if it has
    // autofocus and no other element is focused.
    if (options.autofocus == null) {
      var hasFocus = document.body;
      // doc.activeElement occasionally throws on IE
      try { hasFocus = document.activeElement; } catch(e) {}
      options.autofocus = hasFocus == textarea ||
        textarea.getAttribute("autofocus") != null && hasFocus == document.body;
    }

    function save() {textarea.value = cm.getValue();}
    if (textarea.form) {
      // Deplorable hack to make the submit method do the right thing.
      on(textarea.form, "submit", save);
      var form = textarea.form, realSubmit = form.submit;
      try {
        form.submit = function wrappedSubmit() {
          save();
          form.submit = realSubmit;
          form.submit();
          form.submit = wrappedSubmit;
        };
      } catch(e) {}
    }

    textarea.style.display = "none";
    var cm = CodeMirror(function(node) {
      textarea.parentNode.insertBefore(node, textarea.nextSibling);
    }, options);
    cm.save = save;
    cm.getTextArea = function() { return textarea; };
    cm.toTextArea = function() {
      save();
      textarea.parentNode.removeChild(cm.getWrapperElement());
      textarea.style.display = "";
      if (textarea.form) {
        off(textarea.form, "submit", save);
        if (typeof textarea.form.submit == "function")
          textarea.form.submit = realSubmit;
      }
    };
    return cm;
  };

  // STRING STREAM

  // Fed to the mode parsers, provides helper functions to make
  // parsers more succinct.

  // The character stream used by a mode's parser.
  function StringStream(string, tabSize) {
    this.pos = this.start = 0;
    this.string = string;
    this.tabSize = tabSize || 8;
  }

  StringStream.prototype = {
    eol: function() {return this.pos >= this.string.length;},
    sol: function() {return this.pos == 0;},
    peek: function() {return this.string.charAt(this.pos) || undefined;},
    next: function() {
      if (this.pos < this.string.length)
        return this.string.charAt(this.pos++);
    },
    eat: function(match) {
      var ch = this.string.charAt(this.pos);
      if (typeof match == "string") var ok = ch == match;
      else var ok = ch && (match.test ? match.test(ch) : match(ch));
      if (ok) {++this.pos; return ch;}
    },
    eatWhile: function(match) {
      var start = this.pos;
      while (this.eat(match)){}
      return this.pos > start;
    },
    eatSpace: function() {
      var start = this.pos;
      while (/[\s\u00a0]/.test(this.string.charAt(this.pos))) ++this.pos;
      return this.pos > start;
    },
    skipToEnd: function() {this.pos = this.string.length;},
    skipTo: function(ch) {
      var found = this.string.indexOf(ch, this.pos);
      if (found > -1) {this.pos = found; return true;}
    },
    backUp: function(n) {this.pos -= n;},
    column: function() {return countColumn(this.string, this.start, this.tabSize);},
    indentation: function() {return countColumn(this.string, null, this.tabSize);},
    match: function(pattern, consume, caseInsensitive) {
      if (typeof pattern == "string") {
        var cased = function(str) {return caseInsensitive ? str.toLowerCase() : str;};
        if (cased(this.string).indexOf(cased(pattern), this.pos) == this.pos) {
          if (consume !== false) this.pos += pattern.length;
          return true;
        }
      } else {
        var match = this.string.slice(this.pos).match(pattern);
        if (match && match.index > 0) return null;
        if (match && consume !== false) this.pos += match[0].length;
        return match;
      }
    },
    current: function(){return this.string.slice(this.start, this.pos);}
  };
  CodeMirror.StringStream = StringStream;

  // TEXTMARKERS

  function TextMarker(cm, type) {
    this.lines = [];
    this.type = type;
    this.cm = cm;
  }

  TextMarker.prototype.clear = function() {
    if (this.explicitlyCleared) return;
    startOperation(this.cm);
    var min = null, max = null;
    for (var i = 0; i < this.lines.length; ++i) {
      var line = this.lines[i];
      var span = getMarkedSpanFor(line.markedSpans, this);
      if (span.to != null) max = lineNo(line);
      line.markedSpans = removeMarkedSpan(line.markedSpans, span);
      if (span.from != null)
        min = lineNo(line);
      else if (this.collapsed && !lineIsHidden(line))
        updateLineHeight(line, textHeight(this.cm.display));
    }
    if (min != null) regChange(this.cm, min, max + 1);
    this.lines.length = 0;
    this.explicitlyCleared = true;
    if (this.collapsed && this.cm.view.cantEdit) {
      this.cm.view.cantEdit = false;
      reCheckSelection(this.cm);
    }
    endOperation(this.cm);
    signalLater(this.cm, this, "clear");
  };

  TextMarker.prototype.find = function() {
    var from, to;
    for (var i = 0; i < this.lines.length; ++i) {
      var line = this.lines[i];
      var span = getMarkedSpanFor(line.markedSpans, this);
      if (span.from != null || span.to != null) {
        var found = lineNo(line);
        if (span.from != null) from = {line: found, ch: span.from};
        if (span.to != null) to = {line: found, ch: span.to};
      }
    }
    if (this.type == "bookmark") return from;
    return from && {from: from, to: to};
  };

  function markText(cm, from, to, options, type) {
    var doc = cm.view.doc;
    var marker = new TextMarker(cm, type);
    if (type == "range" && !posLess(from, to)) return marker;
    if (options) for (var opt in options) if (options.hasOwnProperty(opt))
      marker[opt] = options[opt];
    if (marker.replacedWith) {
      marker.collapsed = true;
      marker.replacedWith = elt("span", [marker.replacedWith], "CodeMirror-widget");
    }
    if (marker.collapsed) sawCollapsedSpans = true;

    var curLine = from.line, size = 0, collapsedAtStart, collapsedAtEnd;
    doc.iter(curLine, to.line + 1, function(line) {
      var span = {from: null, to: null, marker: marker};
      size += line.text.length;
      if (curLine == from.line) {span.from = from.ch; size -= from.ch;}
      if (curLine == to.line) {span.to = to.ch; size -= line.text.length - to.ch;}
      if (marker.collapsed) {
        if (curLine == to.line) collapsedAtEnd = collapsedSpanAt(line, to.ch);
        if (curLine == from.line) collapsedAtStart = collapsedSpanAt(line, from.ch);
        else updateLineHeight(line, 0);
      }
      addMarkedSpan(line, span);
      if (marker.collapsed && curLine == from.line && lineIsHidden(line))
        updateLineHeight(line, 0);
      ++curLine;
    });

    if (marker.readOnly) {
      sawReadOnlySpans = true;
      if (cm.view.history.done.length || cm.view.history.undone.length)
        cm.clearHistory();
    }
    if (marker.collapsed) {
      if (collapsedAtStart != collapsedAtEnd)
        throw new Error("Inserting collapsed marker overlapping an existing one");
      marker.size = size;
      marker.atomic = true;
    }
    if (marker.className || marker.startStyle || marker.endStyle || marker.collapsed)
      regChange(cm, from.line, to.line + 1);
    if (marker.atomic) reCheckSelection(cm);
    return marker;
  }

  // TEXTMARKER SPANS

  function getMarkedSpanFor(spans, marker) {
    if (spans) for (var i = 0; i < spans.length; ++i) {
      var span = spans[i];
      if (span.marker == marker) return span;
    }
  }
  function removeMarkedSpan(spans, span) {
    for (var r, i = 0; i < spans.length; ++i)
      if (spans[i] != span) (r || (r = [])).push(spans[i]);
    return r;
  }
  function addMarkedSpan(line, span) {
    line.markedSpans = line.markedSpans ? line.markedSpans.concat([span]) : [span];
    span.marker.lines.push(line);
  }

  function markedSpansBefore(old, startCh) {
    if (old) for (var i = 0, nw; i < old.length; ++i) {
      var span = old[i], marker = span.marker;
      var startsBefore = span.from == null || (marker.inclusiveLeft ? span.from <= startCh : span.from < startCh);
      if (startsBefore || marker.type == "bookmark" && span.from == startCh) {
        var endsAfter = span.to == null || (marker.inclusiveRight ? span.to >= startCh : span.to > startCh);
        (nw || (nw = [])).push({from: span.from,
                                to: endsAfter ? null : span.to,
                                marker: marker});
      }
    }
    return nw;
  }

  function markedSpansAfter(old, startCh, endCh) {
    if (old) for (var i = 0, nw; i < old.length; ++i) {
      var span = old[i], marker = span.marker;
      var endsAfter = span.to == null || (marker.inclusiveRight ? span.to >= endCh : span.to > endCh);
      if (endsAfter || marker.type == "bookmark" && span.from == endCh && span.from != startCh) {
        var startsBefore = span.from == null || (marker.inclusiveLeft ? span.from <= endCh : span.from < endCh);
        (nw || (nw = [])).push({from: startsBefore ? null : span.from - endCh,
                                to: span.to == null ? null : span.to - endCh,
                                marker: marker});
      }
    }
    return nw;
  }

  function updateMarkedSpans(oldFirst, oldLast, startCh, endCh, newText) {
    if (!oldFirst && !oldLast) return newText;
    // Get the spans that 'stick out' on both sides
    var first = markedSpansBefore(oldFirst, startCh);
    var last = markedSpansAfter(oldLast, startCh, endCh);

    // Next, merge those two ends
    var sameLine = newText.length == 1, offset = lst(newText).length + (sameLine ? startCh : 0);
    if (first) {
      // Fix up .to properties of first
      for (var i = 0; i < first.length; ++i) {
        var span = first[i];
        if (span.to == null) {
          var found = getMarkedSpanFor(last, span.marker);
          if (!found) span.to = startCh;
          else if (sameLine) span.to = found.to == null ? null : found.to + offset;
        }
      }
    }
    if (last) {
      // Fix up .from in last (or move them into first in case of sameLine)
      for (var i = 0; i < last.length; ++i) {
        var span = last[i];
        if (span.to != null) span.to += offset;
        if (span.from == null) {
          var found = getMarkedSpanFor(first, span.marker);
          if (!found) {
            span.from = offset;
            if (sameLine) (first || (first = [])).push(span);
          }
        } else {
          span.from += offset;
          if (sameLine) (first || (first = [])).push(span);
        }
      }
    }

    var newMarkers = [newHL(newText[0], first)];
    if (!sameLine) {
      // Fill gap with whole-line-spans
      var gap = newText.length - 2, gapMarkers;
      if (gap > 0 && first)
        for (var i = 0; i < first.length; ++i)
          if (first[i].to == null)
            (gapMarkers || (gapMarkers = [])).push({from: null, to: null, marker: first[i].marker});
      for (var i = 0; i < gap; ++i)
        newMarkers.push(newHL(newText[i+1], gapMarkers));
      newMarkers.push(newHL(lst(newText), last));
    }
    return newMarkers;
  }

  function removeReadOnlyRanges(doc, from, to) {
    var markers = null;
    doc.iter(from.line, to.line + 1, function(line) {
      if (line.markedSpans) for (var i = 0; i < line.markedSpans.length; ++i) {
        var mark = line.markedSpans[i].marker;
        if (mark.readOnly && (!markers || indexOf(markers, mark) == -1))
          (markers || (markers = [])).push(mark);
      }
    });
    if (!markers) return null;
    var parts = [{from: from, to: to}];
    for (var i = 0; i < markers.length; ++i) {
      var m = markers[i].find();
      for (var j = 0; j < parts.length; ++j) {
        var p = parts[j];
        if (!posLess(m.from, p.to) || posLess(m.to, p.from)) continue;
        var newParts = [j, 1];
        if (posLess(p.from, m.from)) newParts.push({from: p.from, to: m.from});
        if (posLess(m.to, p.to)) newParts.push({from: m.to, to: p.to});
        parts.splice.apply(parts, newParts);
        j += newParts.length - 1;
      }
    }
    return parts;
  }

  function collapsedSpanAt(line, ch) {
    var sps = sawCollapsedSpans && line.markedSpans, found;
    if (sps) for (var sp, i = 0; i < sps.length; ++i) {
      sp = sps[i];
      if (!sp.marker.collapsed) continue;
      if ((sp.from == null || sp.from < ch) &&
          (sp.to == null || sp.to > ch) &&
          (!found || found.width < sp.marker.width))
        found = sp.marker;
    }
    return found;
  }
  function collapsedSpanAtStart(line) { return collapsedSpanAt(line, -1); }
  function collapsedSpanAtEnd(line) { return collapsedSpanAt(line, line.text.length + 1); }

  function visualLine(doc, line) {
    var merged;
    while (merged = collapsedSpanAtStart(line))
      line = getLine(doc, merged.find().from.line);
    return line;
  }

  function lineIsHidden(line) {
    var sps = sawCollapsedSpans && line.markedSpans;
    if (sps) for (var sp, i = 0; i < sps.length; ++i) {
      sp = sps[i];
      if (!sp.marker.collapsed) continue;
      if (sp.from == null) return true;
      if (sp.from == 0 && sp.marker.inclusiveLeft && lineIsHiddenInner(line, sp))
        return true;
    }
  }
  window.lineIsHidden = lineIsHidden;
  function lineIsHiddenInner(line, span) {
    if (span.to == null || span.marker.inclusiveRight && span.to == line.text.length)
      return true;
    for (var sp, i = 0; i < line.markedSpans.length; ++i) {
      sp = line.markedSpans[i];
      if (sp.marker.collapsed && sp.from == span.to &&
          (sp.marker.inclusiveLeft || span.marker.inclusiveRight) &&
          lineIsHiddenInner(line, sp)) return true;
    }
  }

  // hl stands for history-line, a data structure that can be either a
  // string (line without markers) or a {text, markedSpans} object.
  function hlText(val) { return typeof val == "string" ? val : val.text; }
  function hlSpans(val) {
    if (typeof val == "string") return null;
    var spans = val.markedSpans, out = null;
    for (var i = 0; i < spans.length; ++i) {
      if (spans[i].marker.explicitlyCleared) { if (!out) out = spans.slice(0, i); }
      else if (out) out.push(spans[i]);
    }
    return !out ? spans : out.length ? out : null;
  }
  function newHL(text, spans) { return spans ? {text: text, markedSpans: spans} : text; }

  function detachMarkedSpans(line) {
    var spans = line.markedSpans;
    if (!spans) return;
    for (var i = 0; i < spans.length; ++i) {
      var lines = spans[i].marker.lines;
      var ix = indexOf(lines, line);
      lines.splice(ix, 1);
    }
    line.markedSpans = null;
  }

  function attachMarkedSpans(line, spans) {
    if (!spans) return;
    for (var i = 0; i < spans.length; ++i)
      spans[i].marker.lines.push(line);
    line.markedSpans = spans;
  }

  // LINE DATA STRUCTURE

  // Line objects. These hold state related to a line, including
  // highlighting info (the styles array).
  function makeLine(text, markedSpans, height) {
    var line = {text: text, height: height};
    attachMarkedSpans(line, markedSpans);
    if (lineIsHidden(line)) line.height = 0;
    return line;
  }

  function updateLine(cm, line, text, markedSpans) {
    line.text = text;
    line.stateAfter = line.styles = null;
    if (line.order != null) line.order = null;
    detachMarkedSpans(line);
    attachMarkedSpans(line, markedSpans);
    if (lineIsHidden(line)) line.height = 0;
    else if (!line.height) line.height = textHeight(cm.display);
    signalLater(cm, line, "change");
  }

  function cleanUpLine(line) {
    line.parent = null;
    detachMarkedSpans(line);
  }

  // Run the given mode's parser over a line, update the styles
  // array, which contains alternating fragments of text and CSS
  // classes.
  function highlightLine(cm, line, state) {
    var mode = cm.view.mode, flattenSpans = cm.options.flattenSpans;
    var changed = !line.styles, pos = 0, curText = "", curStyle = null;
    var stream = new StringStream(line.text, cm.options.tabSize), st = line.styles || (line.styles = []);
    if (line.text == "" && mode.blankLine) mode.blankLine(state);
    while (!stream.eol()) {
      var style = mode.token(stream, state), substr = stream.current();
      stream.start = stream.pos;
      if (!flattenSpans || curStyle != style) {
        if (curText) {
          changed = changed || pos >= st.length || curText != st[pos] || curStyle != st[pos+1];
          st[pos++] = curText; st[pos++] = curStyle;
        }
        curText = substr; curStyle = style;
      } else curText = curText + substr;
      // Give up when line is ridiculously long
      if (stream.pos > 5000) break;
    }
    if (curText) {
      changed = changed || pos >= st.length || curText != st[pos] || curStyle != st[pos+1];
      st[pos++] = curText; st[pos++] = curStyle;
    }
    if (stream.pos > 5000) { st[pos++] = line.text.slice(stream.pos); st[pos++] = null; }
    if (pos != st.length) { st.length = pos; changed = true; }
    return changed;
  }

  // Lightweight form of highlight -- proceed over this line and
  // update state, but don't save a style array.
  function processLine(cm, line, state) {
    var mode = cm.view.mode;
    var stream = new StringStream(line.text, cm.options.tabSize);
    if (line.text == "" && mode.blankLine) mode.blankLine(state);
    while (!stream.eol() && stream.pos <= 5000) {
      mode.token(stream, state);
      stream.start = stream.pos;
    }
  }

  var styleToClassCache = {};
  function styleToClass(style) {
    if (!style) return null;
    return styleToClassCache[style] ||
      (styleToClassCache[style] = "cm-" + style.replace(/ +/g, " cm-"));
  }

  function lineContent(cm, realLine, measure) {
    var merged, line = realLine, lineBefore, sawBefore, simple = true;
    while (merged = collapsedSpanAtStart(line)) {
      simple = false;
      line = getLine(cm.view.doc, merged.find().from.line);
      if (!lineBefore) lineBefore = line;
    }

    var builder = {pre: elt("pre"), col: 0, pos: 0, display: !measure,
                   measure: null, addedOne: false, cm: cm};
    if (line.textClass) builder.pre.className = line.textClass;

    do {
      if (!line.styles)
        highlightLine(cm, line, line.stateAfter = getStateBefore(cm, lineNo(line)));
      builder.measure = line == realLine && measure;
      builder.pos = 0;
      builder.addToken = builder.measure ? buildTokenMeasure : buildToken;
      if (measure && sawBefore && line != realLine && !builder.addedOne) {
        measure[0] = builder.pre.appendChild(zeroWidthElement(cm.display.measure));
        builder.addedOne = true;
      }
      var next = insertLineContent(line, builder);
      sawBefore = line == lineBefore;
      if (next) {
        line = getLine(cm.view.doc, next.to.line);
        simple = false;
      }
    } while (next);

    if (measure && !builder.addedOne)
      measure[0] = builder.pre.appendChild(simple ? elt("span", "\u00a0") : zeroWidthElement(cm.display.measure));
    if (!builder.pre.firstChild && !lineIsHidden(realLine))
      builder.pre.appendChild(document.createTextNode("\u00a0"));

    return builder.pre;
  }

  var tokenSpecialChars = /[\t\u0000-\u0019\u200b\u2028\u2029\uFEFF]/g;
  function buildToken(builder, text, style, startStyle, endStyle) {
    if (!text) return;
    if (!tokenSpecialChars.test(text)) {
      builder.col += text.length;
      var content = document.createTextNode(text);
    } else {
      var content = document.createDocumentFragment(), pos = 0;
      while (true) {
        tokenSpecialChars.lastIndex = pos;
        var m = tokenSpecialChars.exec(text);
        var skipped = m ? m.index - pos : text.length - pos;
        if (skipped) {
          content.appendChild(document.createTextNode(text.slice(pos, pos + skipped)));
          builder.col += skipped;
        }
        if (!m) break;
        pos += skipped + 1;
        if (m[0] == "\t") {
          var tabSize = builder.cm.options.tabSize, tabWidth = tabSize - builder.col % tabSize;
          content.appendChild(elt("span", spaceStr(tabWidth), "cm-tab"));
          builder.col += tabWidth;
        } else {
          var token = elt("span", "\u2022", "cm-invalidchar");
          token.title = "\\u" + m[0].charCodeAt(0).toString(16);
          content.appendChild(token);
          builder.col += 1;
        }
      }
    }
    if (style || startStyle || endStyle || builder.measure) {
      var fullStyle = style || "";
      if (startStyle) fullStyle += startStyle;
      if (endStyle) fullStyle += endStyle;
      return builder.pre.appendChild(elt("span", [content], fullStyle));
    }
    builder.pre.appendChild(content);
  }

  function buildTokenMeasure(builder, text, style, startStyle, endStyle) {
    for (var i = 0; i < text.length; ++i) {
      if (i && i < text.length - 1 &&
          builder.cm.options.lineWrapping &&
          spanAffectsWrapping.test(text.slice(i - 1, i + 1)))
        builder.pre.appendChild(elt("wbr"));
      builder.measure[builder.pos++] =
        buildToken(builder, text.charAt(i), style,
                   i == 0 && startStyle, i == text.length - 1 && endStyle);
    }
    if (text.length) builder.addedOne = true;
  }

  function buildCollapsedSpan(builder, size, widget) {
    if (widget) {
      if (!builder.display) widget = widget.cloneNode(true);
      builder.pre.appendChild(widget);
      if (builder.measure && size) {
        builder.measure[builder.pos] = widget;
        builder.addedOne = true;
      }
    }
    builder.pos += size;
  }

  // Outputs a number of spans to make up a line, taking highlighting
  // and marked text into account.
  function insertLineContent(line, builder) {
    var st = line.styles, spans = line.markedSpans;
    if (!spans) {
      for (var i = 0; i < st.length; i+=2)
        builder.addToken(builder, st[i], styleToClass(st[i+1]));
      return;
    }

    var allText = line.text, len = allText.length;
    var pos = 0, i = 0, text = "", style;
    var nextChange = 0, spanStyle, spanEndStyle, spanStartStyle, collapsed;
    for (;;) {
      if (nextChange == pos) { // Update current marker set
        spanStyle = spanEndStyle = spanStartStyle = "";
        collapsed = null; nextChange = Infinity;
        var foundBookmark = null;
        for (var j = 0; j < spans.length; ++j) {
          var sp = spans[j], m = sp.marker;
          if (sp.from <= pos && (sp.to == null || sp.to > pos)) {
            if (sp.to != null && nextChange > sp.to) { nextChange = sp.to; spanEndStyle = ""; }
            if (m.className) spanStyle += " " + m.className;
            if (m.startStyle && sp.from == pos) spanStartStyle += " " + m.startStyle;
            if (m.endStyle && sp.to == nextChange) spanEndStyle += " " + m.endStyle;
            if (m.collapsed && (!collapsed || collapsed.marker.width < m.width))
              collapsed = sp;
          } else if (sp.from > pos && nextChange > sp.from) {
            nextChange = sp.from;
          }
          if (m.type == "bookmark" && sp.from == pos && m.replacedWith)
            foundBookmark = m.replacedWith;
        }
        if (collapsed && (collapsed.from || 0) == pos) {
          buildCollapsedSpan(builder, (collapsed.to == null ? len : collapsed.to) - pos,
                             collapsed.from != null && collapsed.marker.replacedWith);
          if (collapsed.to == null) return collapsed.marker.find();
        }
        if (foundBookmark && !collapsed) buildCollapsedSpan(builder, 0, foundBookmark);
      }
      if (pos >= len) break;

      var upto = Math.min(len, nextChange);
      while (true) {
        if (text) {
          var end = pos + text.length;
          if (!collapsed) {
            var tokenText = end > upto ? text.slice(0, upto - pos) : text;
            builder.addToken(builder, tokenText, style + spanStyle,
                             spanStartStyle, pos + tokenText.length == nextChange ? spanEndStyle : "");
          }
          if (end >= upto) {text = text.slice(upto - pos); pos = upto; break;}
          pos = end;
          spanStartStyle = "";
        }
        text = st[i++]; style = styleToClass(st[i++]);
      }
    }
  }

  // DOCUMENT DATA STRUCTURE

  function LeafChunk(lines) {
    this.lines = lines;
    this.parent = null;
    for (var i = 0, e = lines.length, height = 0; i < e; ++i) {
      lines[i].parent = this;
      height += lines[i].height;
    }
    this.height = height;
  }

  LeafChunk.prototype = {
    chunkSize: function() { return this.lines.length; },
    remove: function(at, n, cm) {
      for (var i = at, e = at + n; i < e; ++i) {
        var line = this.lines[i];
        this.height -= line.height;
        cleanUpLine(line);
        signalLater(cm, line, "delete");
      }
      this.lines.splice(at, n);
    },
    collapse: function(lines) {
      lines.splice.apply(lines, [lines.length, 0].concat(this.lines));
    },
    insertHeight: function(at, lines, height) {
      this.height += height;
      this.lines = this.lines.slice(0, at).concat(lines).concat(this.lines.slice(at));
      for (var i = 0, e = lines.length; i < e; ++i) lines[i].parent = this;
    },
    iterN: function(at, n, op) {
      for (var e = at + n; at < e; ++at)
        if (op(this.lines[at])) return true;
    }
  };

  function BranchChunk(children) {
    this.children = children;
    var size = 0, height = 0;
    for (var i = 0, e = children.length; i < e; ++i) {
      var ch = children[i];
      size += ch.chunkSize(); height += ch.height;
      ch.parent = this;
    }
    this.size = size;
    this.height = height;
    this.parent = null;
  }

  BranchChunk.prototype = {
    chunkSize: function() { return this.size; },
    remove: function(at, n, callbacks) {
      this.size -= n;
      for (var i = 0; i < this.children.length; ++i) {
        var child = this.children[i], sz = child.chunkSize();
        if (at < sz) {
          var rm = Math.min(n, sz - at), oldHeight = child.height;
          child.remove(at, rm, callbacks);
          this.height -= oldHeight - child.height;
          if (sz == rm) { this.children.splice(i--, 1); child.parent = null; }
          if ((n -= rm) == 0) break;
          at = 0;
        } else at -= sz;
      }
      if (this.size - n < 25) {
        var lines = [];
        this.collapse(lines);
        this.children = [new LeafChunk(lines)];
        this.children[0].parent = this;
      }
    },
    collapse: function(lines) {
      for (var i = 0, e = this.children.length; i < e; ++i) this.children[i].collapse(lines);
    },
    insert: function(at, lines) {
      var height = 0;
      for (var i = 0, e = lines.length; i < e; ++i) height += lines[i].height;
      this.insertHeight(at, lines, height);
    },
    insertHeight: function(at, lines, height) {
      this.size += lines.length;
      this.height += height;
      for (var i = 0, e = this.children.length; i < e; ++i) {
        var child = this.children[i], sz = child.chunkSize();
        if (at <= sz) {
          child.insertHeight(at, lines, height);
          if (child.lines && child.lines.length > 50) {
            while (child.lines.length > 50) {
              var spilled = child.lines.splice(child.lines.length - 25, 25);
              var newleaf = new LeafChunk(spilled);
              child.height -= newleaf.height;
              this.children.splice(i + 1, 0, newleaf);
              newleaf.parent = this;
            }
            this.maybeSpill();
          }
          break;
        }
        at -= sz;
      }
    },
    maybeSpill: function() {
      if (this.children.length <= 10) return;
      var me = this;
      do {
        var spilled = me.children.splice(me.children.length - 5, 5);
        var sibling = new BranchChunk(spilled);
        if (!me.parent) { // Become the parent node
          var copy = new BranchChunk(me.children);
          copy.parent = me;
          me.children = [copy, sibling];
          me = copy;
        } else {
          me.size -= sibling.size;
          me.height -= sibling.height;
          var myIndex = indexOf(me.parent.children, me);
          me.parent.children.splice(myIndex + 1, 0, sibling);
        }
        sibling.parent = me.parent;
      } while (me.children.length > 10);
      me.parent.maybeSpill();
    },
    iter: function(from, to, op) { this.iterN(from, to - from, op); },
    iterN: function(at, n, op) {
      for (var i = 0, e = this.children.length; i < e; ++i) {
        var child = this.children[i], sz = child.chunkSize();
        if (at < sz) {
          var used = Math.min(n, sz - at);
          if (child.iterN(at, used, op)) return true;
          if ((n -= used) == 0) break;
          at = 0;
        } else at -= sz;
      }
    }
  };

  // LINE UTILITIES

  function getLine(chunk, n) {
    while (!chunk.lines) {
      for (var i = 0;; ++i) {
        var child = chunk.children[i], sz = child.chunkSize();
        if (n < sz) { chunk = child; break; }
        n -= sz;
      }
    }
    return chunk.lines[n];
  }

  function updateLineHeight(line, height) {
    var diff = height - line.height;
    for (var n = line; n; n = n.parent) n.height += diff;
  }

  function lineNo(line) {
    if (line.parent == null) return null;
    var cur = line.parent, no = indexOf(cur.lines, line);
    for (var chunk = cur.parent; chunk; cur = chunk, chunk = chunk.parent) {
      for (var i = 0;; ++i) {
        if (chunk.children[i] == cur) break;
        no += chunk.children[i].chunkSize();
      }
    }
    return no;
  }

  function lineAtHeight(chunk, h) {
    var n = 0;
    outer: do {
      for (var i = 0, e = chunk.children.length; i < e; ++i) {
        var child = chunk.children[i], ch = child.height;
        if (h < ch) { chunk = child; continue outer; }
        h -= ch;
        n += child.chunkSize();
      }
      return n;
    } while (!chunk.lines);
    for (var i = 0, e = chunk.lines.length; i < e; ++i) {
      var line = chunk.lines[i], lh = line.height;
      if (h < lh) break;
      h -= lh;
    }
    return n + i;
  }

  function heightAtLine(cm, lineObj) {
    lineObj = visualLine(cm.view.doc, lineObj);

    var h = 0, chunk = lineObj.parent;
    for (var i = 0; i < chunk.lines.length; ++i) {
      var line = chunk.lines[i];
      if (line == lineObj) break;
      else h += line.height;
    }
    for (var p = chunk.parent; p; chunk = p, p = chunk.parent) {
      for (var i = 0; i < p.children.length; ++i) {
        var cur = p.children[i];
        if (cur == chunk) break;
        else h += cur.height;
      }
    }
    return h;
  }

  function getOrder(line) {
    var order = line.order;
    if (order == null) order = line.order = bidiOrdering(line.text);
    return order;
  }

  // HISTORY

  function makeHistory() {
    return {
      // Arrays of history events. Doing something adds an event to
      // done and clears undo. Undoing moves events from done to
      // undone, redoing moves them in the other direction.
      done: [], undone: [],
      // Used to track when changes can be merged into a single undo
      // event
      lastTime: 0, lastOp: null, lastOrigin: null,
      // Used by the isClean() method
      dirtyCounter: 0
    };
  }

  function addChange(cm, start, added, old, origin, fromBefore, toBefore, fromAfter, toAfter) {
    var history = cm.view.history;
    history.undone.length = 0;
    var time = +new Date, cur = lst(history.done);
    
    if (cur &&
        (history.lastOp == cm.curOp.id ||
         history.lastOrigin == origin && (origin == "input" || origin == "delete") &&
         history.lastTime > time - 600)) {
      // Merge this change into the last event
      var last = lst(cur.events);
      if (last.start > start + old.length || last.start + last.added < start) {
        // Doesn't intersect with last sub-event, add new sub-event
        cur.events.push({start: start, added: added, old: old});
      } else {
        // Patch up the last sub-event
        var startBefore = Math.max(0, last.start - start),
        endAfter = Math.max(0, (start + old.length) - (last.start + last.added));
        for (var i = startBefore; i > 0; --i) last.old.unshift(old[i - 1]);
        for (var i = endAfter; i > 0; --i) last.old.push(old[old.length - i]);
        if (startBefore) last.start = start;
        last.added += added - (old.length - startBefore - endAfter);
      }
      cur.fromAfter = fromAfter; cur.toAfter = toAfter;
    } else {
      // Can not be merged, start a new event.
      cur = {events: [{start: start, added: added, old: old}],
             fromBefore: fromBefore, toBefore: toBefore, fromAfter: fromAfter, toAfter: toAfter};
      history.done.push(cur);
      while (history.done.length > cm.options.undoDepth)
        history.done.shift();
      if (history.dirtyCounter < 0)
          // The user has made a change after undoing past the last clean state. 
          // We can never get back to a clean state now until markClean() is called.
          history.dirtyCounter = NaN;
      else
        history.dirtyCounter++;
    }
    history.lastTime = time;
    history.lastOp = cm.curOp.id;
    history.lastOrigin = origin;
  }

  // EVENT OPERATORS

  function stopMethod() {e_stop(this);}
  // Ensure an event has a stop method.
  function addStop(event) {
    if (!event.stop) event.stop = stopMethod;
    return event;
  }

  function e_preventDefault(e) {
    if (e.preventDefault) e.preventDefault();
    else e.returnValue = false;
  }
  function e_stopPropagation(e) {
    if (e.stopPropagation) e.stopPropagation();
    else e.cancelBubble = true;
  }
  function e_stop(e) {e_preventDefault(e); e_stopPropagation(e);}
  CodeMirror.e_stop = e_stop;
  CodeMirror.e_preventDefault = e_preventDefault;
  CodeMirror.e_stopPropagation = e_stopPropagation;

  function e_target(e) {return e.target || e.srcElement;}
  function e_button(e) {
    var b = e.which;
    if (b == null) {
      if (e.button & 1) b = 1;
      else if (e.button & 2) b = 3;
      else if (e.button & 4) b = 2;
    }
    if (mac && e.ctrlKey && b == 1) b = 3;
    return b;
  }

  // Allow 3rd-party code to override event properties by adding an override
  // object to an event object.
  function e_prop(e, prop) {
    var overridden = e.override && e.override.hasOwnProperty(prop);
    return overridden ? e.override[prop] : e[prop];
  }

  // EVENT HANDLING

  function on(emitter, type, f) {
    if (emitter.addEventListener)
      emitter.addEventListener(type, f, false);
    else if (emitter.attachEvent)
      emitter.attachEvent("on" + type, f);
    else {
      var map = emitter._handlers || (emitter._handlers = {});
      var arr = map[type] || (map[type] = []);
      arr.push(f);
    }
  }

  function off(emitter, type, f) {
    if (emitter.removeEventListener)
      emitter.removeEventListener(type, f, false);
    else if (emitter.detachEvent)
      emitter.detachEvent("on" + type, f);
    else {
      var arr = emitter._handlers && emitter._handlers[type];
      if (!arr) return;
      for (var i = 0; i < arr.length; ++i)
        if (arr[i] == f) { arr.splice(i, 1); break; }
    }
  }

  function signal(emitter, type /*, values...*/) {
    var arr = emitter._handlers && emitter._handlers[type];
    if (!arr) return;
    var args = Array.prototype.slice.call(arguments, 2);
    for (var i = 0; i < arr.length; ++i) arr[i].apply(null, args);
  }

  function signalLater(cm, emitter, type /*, values...*/) {
    var arr = emitter._handlers && emitter._handlers[type];
    if (!arr) return;
    var args = Array.prototype.slice.call(arguments, 3), flist = cm.curOp && cm.curOp.delayedCallbacks;
    function bnd(f) {return function(){f.apply(null, args);};};
    for (var i = 0; i < arr.length; ++i)
      if (flist) flist.push(bnd(arr[i]));
      else arr[i].apply(null, args);
  }

  function hasHandler(emitter, type) {
    var arr = emitter._handlers && emitter._handlers[type];
    return arr && arr.length > 0;
  }

  CodeMirror.on = on; CodeMirror.off = off; CodeMirror.signal = signal;

  // MISC UTILITIES

  // Number of pixels added to scroller and sizer to hide scrollbar
  var scrollerCutOff = 30;

  // Returned or thrown by various protocols to signal 'I'm not
  // handling this'.
  var Pass = CodeMirror.Pass = {toString: function(){return "CodeMirror.Pass";}};

  function Delayed() {this.id = null;}
  Delayed.prototype = {set: function(ms, f) {clearTimeout(this.id); this.id = setTimeout(f, ms);}};

  // Counts the column offset in a string, taking tabs into account.
  // Used mostly to find indentation.
  function countColumn(string, end, tabSize) {
    if (end == null) {
      end = string.search(/[^\s\u00a0]/);
      if (end == -1) end = string.length;
    }
    for (var i = 0, n = 0; i < end; ++i) {
      if (string.charAt(i) == "\t") n += tabSize - (n % tabSize);
      else ++n;
    }
    return n;
  }
  CodeMirror.countColumn = countColumn;

  var spaceStrs = [""];
  function spaceStr(n) {
    while (spaceStrs.length <= n)
      spaceStrs.push(lst(spaceStrs) + " ");
    return spaceStrs[n];
  }

  function lst(arr) { return arr[arr.length-1]; }

  function selectInput(node) {
    if (ios) { // Mobile Safari apparently has a bug where select() is broken.
      node.selectionStart = 0;
      node.selectionEnd = node.value.length;
    } else node.select();
  }

  function indexOf(collection, elt) {
    if (collection.indexOf) return collection.indexOf(elt);
    for (var i = 0, e = collection.length; i < e; ++i)
      if (collection[i] == elt) return i;
    return -1;
  }

  function emptyArray(size) {
    for (var a = [], i = 0; i < size; ++i) a.push(undefined);
    return a;
  }

  function bind(f) {
    var args = Array.prototype.slice.call(arguments, 1);
    return function(){return f.apply(null, args);};
  }

  var nonASCIISingleCaseWordChar = /[\u3040-\u309f\u30a0-\u30ff\u3400-\u4db5\u4e00-\u9fcc]/;
  function isWordChar(ch) {
    return /\w/.test(ch) || ch > "\x80" &&
      (ch.toUpperCase() != ch.toLowerCase() || nonASCIISingleCaseWordChar.test(ch));
  }

  function isEmpty(obj) {
    var c = 0;
    for (var n in obj) if (obj.hasOwnProperty(n) && obj[n]) ++c;
    return !c;
  }

  var isExtendingChar = /[\u0300-\u036F\u0483-\u0487\u0488-\u0489\u0591-\u05BD\u05BF\u05C1-\u05C2\u05C4-\u05C5\u05C7\u0610-\u061A\u064B-\u065F\u0670\u06D6-\u06DC\u06DF-\u06E4\u06E7-\u06E8\u06EA-\u06ED\uA66F\uA670-\uA672\uA674-\uA67D\uA69F]/;

  // DOM UTILITIES

  function elt(tag, content, className, style) {
    var e = document.createElement(tag);
    if (className) e.className = className;
    if (style) e.style.cssText = style;
    if (typeof content == "string") setTextContent(e, content);
    else if (content) for (var i = 0; i < content.length; ++i) e.appendChild(content[i]);
    return e;
  }

  function removeChildren(e) {
    e.innerHTML = "";
    return e;
  }

  function removeChildrenAndAdd(parent, e) {
    return removeChildren(parent).appendChild(e);
  }

  function setTextContent(e, str) {
    if (ie_lt9) {
      e.innerHTML = "";
      e.appendChild(document.createTextNode(str));
    } else e.textContent = str;
  }

  // FEATURE DETECTION

  // Detect drag-and-drop
  var dragAndDrop = function() {
    // There is *some* kind of drag-and-drop support in IE6-8, but I
    // couldn't get it to work yet.
    if (ie_lt9) return false;
    var div = elt('div');
    return "draggable" in div || "dragDrop" in div;
  }();

  // For a reason I have yet to figure out, some browsers disallow
  // word wrapping between certain characters *only* if a new inline
  // element is started between them. This makes it hard to reliably
  // measure the position of things, since that requires inserting an
  // extra span. This terribly fragile set of regexps matches the
  // character combinations that suffer from this phenomenon on the
  // various browsers.
  var spanAffectsWrapping = /^$/; // Won't match any two-character string
  if (gecko) spanAffectsWrapping = /$'/;
  else if (safari) spanAffectsWrapping = /\-[^ \-?]|\?[^ !'\"\),.\-\/:;\?\]\}]/;
  else if (chrome) spanAffectsWrapping = /\-[^ \-\.?]|\?[^ \-\.?\]\}:;!'\"\),\/]|[\.!\"#&%\)*+,:;=>\]|\}~][\(\{\[<]|\$'/;

  var knownScrollbarWidth;
  function scrollbarWidth(measure) {
    if (knownScrollbarWidth != null) return knownScrollbarWidth;
    var test = elt("div", null, null, "width: 50px; height: 50px; overflow-x: scroll");
    removeChildrenAndAdd(measure, test);
    if (test.offsetWidth)
      knownScrollbarWidth = test.offsetHeight - test.clientHeight;
    return knownScrollbarWidth || 0;
  }

  var zwspSupported;
  function zeroWidthElement(measure) {
    if (zwspSupported == null) {
      var test = elt("span", "\u200b");
      removeChildrenAndAdd(measure, elt("span", [test, document.createTextNode("x")]));
      if (measure.firstChild.offsetHeight != 0)
        zwspSupported = test.offsetWidth <= 1 && test.offsetHeight > 2 && !ie_lt8;
    }
    if (zwspSupported) return elt("span", "\u200b");
    else return elt("span", "\u00a0", null, "display: inline-block; width: 1px; margin-right: -1px");
  }

  // See if "".split is the broken IE version, if so, provide an
  // alternative way to split lines.
  var splitLines = "\n\nb".split(/\n/).length != 3 ? function(string) {
    var pos = 0, result = [], l = string.length;
    while (pos <= l) {
      var nl = string.indexOf("\n", pos);
      if (nl == -1) nl = string.length;
      var line = string.slice(pos, string.charAt(nl - 1) == "\r" ? nl - 1 : nl);
      var rt = line.indexOf("\r");
      if (rt != -1) {
        result.push(line.slice(0, rt));
        pos += rt + 1;
      } else {
        result.push(line);
        pos = nl + 1;
      }
    }
    return result;
  } : function(string){return string.split(/\r\n?|\n/);};
  CodeMirror.splitLines = splitLines;

  var hasSelection = window.getSelection ? function(te) {
    try { return te.selectionStart != te.selectionEnd; }
    catch(e) { return false; }
  } : function(te) {
    try {var range = te.ownerDocument.selection.createRange();}
    catch(e) {}
    if (!range || range.parentElement() != te) return false;
    return range.compareEndPoints("StartToEnd", range) != 0;
  };

  var hasCopyEvent = (function() {
    var e = elt("div");
    if ("oncopy" in e) return true;
    e.setAttribute("oncopy", "return;");
    return typeof e.oncopy == 'function';
  })();

  // KEY NAMING

  var keyNames = {3: "Enter", 8: "Backspace", 9: "Tab", 13: "Enter", 16: "Shift", 17: "Ctrl", 18: "Alt",
                  19: "Pause", 20: "CapsLock", 27: "Esc", 32: "Space", 33: "PageUp", 34: "PageDown", 35: "End",
                  36: "Home", 37: "Left", 38: "Up", 39: "Right", 40: "Down", 44: "PrintScrn", 45: "Insert",
                  46: "Delete", 59: ";", 91: "Mod", 92: "Mod", 93: "Mod", 109: "-", 107: "=", 127: "Delete",
                  186: ";", 187: "=", 188: ",", 189: "-", 190: ".", 191: "/", 192: "`", 219: "[", 220: "\\",
                  221: "]", 222: "'", 63276: "PageUp", 63277: "PageDown", 63275: "End", 63273: "Home",
                  63234: "Left", 63232: "Up", 63235: "Right", 63233: "Down", 63302: "Insert", 63272: "Delete"};
  CodeMirror.keyNames = keyNames;
  (function() {
    // Number keys
    for (var i = 0; i < 10; i++) keyNames[i + 48] = String(i);
    // Alphabetic keys
    for (var i = 65; i <= 90; i++) keyNames[i] = String.fromCharCode(i);
    // Function keys
    for (var i = 1; i <= 12; i++) keyNames[i + 111] = keyNames[i + 63235] = "F" + i;
  })();

  // BIDI HELPERS

  function iterateBidiSections(order, from, to, f) {
    if (!order) return f(from, to, "ltr");
    for (var i = 0; i < order.length; ++i) {
      var part = order[i];
      if (part.from < to && part.to > from)
        f(Math.max(part.from, from), Math.min(part.to, to), part.level == 1 ? "rtl" : "ltr");
    }
  }

  function bidiLeft(part) { return part.level % 2 ? part.to : part.from; }
  function bidiRight(part) { return part.level % 2 ? part.from : part.to; }

  function lineLeft(line) { var order = getOrder(line); return order ? bidiLeft(order[0]) : 0; }
  function lineRight(line) {
    var order = getOrder(line);
    if (!order) return line.text.length;
    return bidiRight(lst(order));
  }

  function lineStart(cm, lineN) {
    var line = getLine(cm.view.doc, lineN);
    var visual = visualLine(cm.view.doc, line);
    if (visual != line) lineN = lineNo(visual);
    var order = getOrder(visual);
    var ch = !order ? 0 : order[0].level % 2 ? lineRight(visual) : lineLeft(visual);
    return {line: lineN, ch: ch};
  }
  function lineEnd(cm, lineNo) {
    var merged, line;
    while (merged = collapsedSpanAtEnd(line = getLine(cm.view.doc, lineNo)))
      lineNo = merged.find().to.line;
    var order = getOrder(line);
    var ch = !order ? line.text.length : order[0].level % 2 ? lineLeft(line) : lineRight(line);
    return {line: lineNo, ch: ch};
  }

  // This is somewhat involved. It is needed in order to move
  // 'visually' through bi-directional text -- i.e., pressing left
  // should make the cursor go left, even when in RTL text. The
  // tricky part is the 'jumps', where RTL and LTR text touch each
  // other. This often requires the cursor offset to move more than
  // one unit, in order to visually move one unit.
  function moveVisually(line, start, dir, byUnit) {
    var bidi = getOrder(line);
    if (!bidi) return moveLogically(line, start, dir, byUnit);
    var moveOneUnit = byUnit ? function(pos, dir) {
      do pos += dir;
      while (pos > 0 && isExtendingChar.test(line.text.charAt(pos)));
      return pos;
    } : function(pos, dir) { return pos + dir; };
    var linedir = bidi[0].level;
    for (var i = 0; i < bidi.length; ++i) {
      var part = bidi[i], sticky = part.level % 2 == linedir;
      if ((part.from < start && part.to > start) ||
          (sticky && (part.from == start || part.to == start))) break;
    }
    var target = moveOneUnit(start, part.level % 2 ? -dir : dir);

    while (target != null) {
      if (part.level % 2 == linedir) {
        if (target < part.from || target > part.to) {
          part = bidi[i += dir];
          target = part && (dir > 0 == part.level % 2 ? moveOneUnit(part.to, -1) : moveOneUnit(part.from, 1));
        } else break;
      } else {
        if (target == bidiLeft(part)) {
          part = bidi[--i];
          target = part && bidiRight(part);
        } else if (target == bidiRight(part)) {
          part = bidi[++i];
          target = part && bidiLeft(part);
        } else break;
      }
    }

    return target < 0 || target > line.text.length ? null : target;
  }

  function moveLogically(line, start, dir, byUnit) {
    var target = start + dir;
    if (byUnit) while (target > 0 && isExtendingChar.test(line.text.charAt(target))) target += dir;
    return target < 0 || target > line.text.length ? null : target;
  }

  // Bidirectional ordering algorithm
  // See http://unicode.org/reports/tr9/tr9-13.html for the algorithm
  // that this (partially) implements.

  // One-char codes used for character types:
  // L (L):   Left-to-Right
  // R (R):   Right-to-Left
  // r (AL):  Right-to-Left Arabic
  // 1 (EN):  European Number
  // + (ES):  European Number Separator
  // % (ET):  European Number Terminator
  // n (AN):  Arabic Number
  // , (CS):  Common Number Separator
  // m (NSM): Non-Spacing Mark
  // b (BN):  Boundary Neutral
  // s (B):   Paragraph Separator
  // t (S):   Segment Separator
  // w (WS):  Whitespace
  // N (ON):  Other Neutrals

  // Returns null if characters are ordered as they appear
  // (left-to-right), or an array of sections ({from, to, level}
  // objects) in the order in which they occur visually.
  var bidiOrdering = (function() {
    // Character types for codepoints 0 to 0xff
    var lowTypes = "bbbbbbbbbtstwsbbbbbbbbbbbbbbssstwNN%%%NNNNNN,N,N1111111111NNNNNNNLLLLLLLLLLLLLLLLLLLLLLLLLLNNNNNNLLLLLLLLLLLLLLLLLLLLLLLLLLNNNNbbbbbbsbbbbbbbbbbbbbbbbbbbbbbbbbb,N%%%%NNNNLNNNNN%%11NLNNN1LNNNNNLLLLLLLLLLLLLLLLLLLLLLLNLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLNLLLLLLLL";
    // Character types for codepoints 0x600 to 0x6ff
    var arabicTypes = "rrrrrrrrrrrr,rNNmmmmmmrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrmmmmmmmmmmmmmmrrrrrrrnnnnnnnnnn%nnrrrmrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrmmmmmmmmmmmmmmmmmmmNmmmmrrrrrrrrrrrrrrrrrr";
    function charType(code) {
      if (code <= 0xff) return lowTypes.charAt(code);
      else if (0x590 <= code && code <= 0x5f4) return "R";
      else if (0x600 <= code && code <= 0x6ff) return arabicTypes.charAt(code - 0x600);
      else if (0x700 <= code && code <= 0x8ac) return "r";
      else return "L";
    }

    var bidiRE = /[\u0590-\u05f4\u0600-\u06ff\u0700-\u08ac]/;
    var isNeutral = /[stwN]/, isStrong = /[LRr]/, countsAsLeft = /[Lb1n]/, countsAsNum = /[1n]/;

    return function charOrdering(str) {
      if (!bidiRE.test(str)) return false;
      var len = str.length, types = [], startType = null;
      for (var i = 0, type; i < len; ++i) {
        types.push(type = charType(str.charCodeAt(i)));
        if (startType == null) {
          if (type == "L") startType = "L";
          else if (type == "R" || type == "r") startType = "R";
        }
      }
      if (startType == null) startType = "L";

      // W1. Examine each non-spacing mark (NSM) in the level run, and
      // change the type of the NSM to the type of the previous
      // character. If the NSM is at the start of the level run, it will
      // get the type of sor.
      for (var i = 0, prev = startType; i < len; ++i) {
        var type = types[i];
        if (type == "m") types[i] = prev;
        else prev = type;
      }

      // W2. Search backwards from each instance of a European number
      // until the first strong type (R, L, AL, or sor) is found. If an
      // AL is found, change the type of the European number to Arabic
      // number.
      // W3. Change all ALs to R.
      for (var i = 0, cur = startType; i < len; ++i) {
        var type = types[i];
        if (type == "1" && cur == "r") types[i] = "n";
        else if (isStrong.test(type)) { cur = type; if (type == "r") types[i] = "R"; }
      }

      // W4. A single European separator between two European numbers
      // changes to a European number. A single common separator between
      // two numbers of the same type changes to that type.
      for (var i = 1, prev = types[0]; i < len - 1; ++i) {
        var type = types[i];
        if (type == "+" && prev == "1" && types[i+1] == "1") types[i] = "1";
        else if (type == "," && prev == types[i+1] &&
                 (prev == "1" || prev == "n")) types[i] = prev;
        prev = type;
      }

      // W5. A sequence of European terminators adjacent to European
      // numbers changes to all European numbers.
      // W6. Otherwise, separators and terminators change to Other
      // Neutral.
      for (var i = 0; i < len; ++i) {
        var type = types[i];
        if (type == ",") types[i] = "N";
        else if (type == "%") {
          for (var end = i + 1; end < len && types[end] == "%"; ++end) {}
          var replace = (i && types[i-1] == "!") || (end < len - 1 && types[end] == "1") ? "1" : "N";
          for (var j = i; j < end; ++j) types[j] = replace;
          i = end - 1;
        }
      }

      // W7. Search backwards from each instance of a European number
      // until the first strong type (R, L, or sor) is found. If an L is
      // found, then change the type of the European number to L.
      for (var i = 0, cur = startType; i < len; ++i) {
        var type = types[i];
        if (cur == "L" && type == "1") types[i] = "L";
        else if (isStrong.test(type)) cur = type;
      }

      // N1. A sequence of neutrals takes the direction of the
      // surrounding strong text if the text on both sides has the same
      // direction. European and Arabic numbers act as if they were R in
      // terms of their influence on neutrals. Start-of-level-run (sor)
      // and end-of-level-run (eor) are used at level run boundaries.
      // N2. Any remaining neutrals take the embedding direction.
      for (var i = 0; i < len; ++i) {
        if (isNeutral.test(types[i])) {
          for (var end = i + 1; end < len && isNeutral.test(types[end]); ++end) {}
          var before = (i ? types[i-1] : startType) == "L";
          var after = (end < len - 1 ? types[end] : startType) == "L";
          var replace = before || after ? "L" : "R";
          for (var j = i; j < end; ++j) types[j] = replace;
          i = end - 1;
        }
      }

      // Here we depart from the documented algorithm, in order to avoid
      // building up an actual levels array. Since there are only three
      // levels (0, 1, 2) in an implementation that doesn't take
      // explicit embedding into account, we can build up the order on
      // the fly, without following the level-based algorithm.
      var order = [], m;
      for (var i = 0; i < len;) {
        if (countsAsLeft.test(types[i])) {
          var start = i;
          for (++i; i < len && countsAsLeft.test(types[i]); ++i) {}
          order.push({from: start, to: i, level: 0});
        } else {
          var pos = i, at = order.length;
          for (++i; i < len && types[i] != "L"; ++i) {}
          for (var j = pos; j < i;) {
            if (countsAsNum.test(types[j])) {
              if (pos < j) order.splice(at, 0, {from: pos, to: j, level: 1});
              var nstart = j;
              for (++j; j < i && countsAsNum.test(types[j]); ++j) {}
              order.splice(at, 0, {from: nstart, to: j, level: 2});
              pos = j;
            } else ++j;
          }
          if (pos < i) order.splice(at, 0, {from: pos, to: i, level: 1});
        }
      }
      if (order[0].level == 1 && (m = str.match(/^\s+/))) {
        order[0].from = m[0].length;
        order.unshift({from: 0, to: m[0].length, level: 0});
      }
      if (lst(order).level == 1 && (m = str.match(/\s+$/))) {
        lst(order).to -= m[0].length;
        order.push({from: len - m[0].length, to: len, level: 0});
      }
      if (order[0].level != lst(order).level)
        order.push({from: len, to: len, level: order[0].level});

      return order;
    };
  })();

  // THE END

  CodeMirror.version = "3.0";

  return CodeMirror;
})();

(function() {
  var matching = {"(": ")>", ")": "(<", "[": "]>", "]": "[<", "{": "}>", "}": "{<"};
  function findMatchingBracket(cm) {
    var cur = cm.getCursor(), line = cm.getLineHandle(cur.line), pos = cur.ch - 1;
    var match = (pos >= 0 && matching[line.text.charAt(pos)]) || matching[line.text.charAt(++pos)];
    if (!match) return null;
    var forward = match.charAt(1) == ">", d = forward ? 1 : -1;
    var style = cm.getTokenAt({line: cur.line, ch: pos + 1}).type;

    var stack = [line.text.charAt(pos)], re = /[(){}[\]]/;
    function scan(line, lineNo, start) {
      if (!line.text) return;
      var pos = forward ? 0 : line.text.length - 1, end = forward ? line.text.length : -1;
      if (start != null) pos = start + d;
      for (; pos != end; pos += d) {
        var ch = line.text.charAt(pos);
        if (re.test(ch) && cm.getTokenAt({line: lineNo, ch: pos + 1}).type == style) {
          var match = matching[ch];
          if (match.charAt(1) == ">" == forward) stack.push(ch);
          else if (stack.pop() != match.charAt(0)) return {pos: pos, match: false};
          else if (!stack.length) return {pos: pos, match: true};
        }
      }
    }
    for (var i = cur.line, found, e = forward ? Math.min(i + 100, cm.lineCount()) : Math.max(-1, i - 100); i != e; i+=d) {
      if (i == cur.line) found = scan(line, i, pos);
      else found = scan(cm.getLineHandle(i), i);
      if (found) break;
    }
    return {from: {line: cur.line, ch: pos}, to: found && {line: i, ch: found.pos}, match: found && found.match};
  }

  function matchBrackets(cm, autoclear) {
    var found = findMatchingBracket(cm);
    if (!found) return;
    var style = found.match ? "CodeMirror-matchingbracket" : "CodeMirror-nonmatchingbracket";
    var one = cm.markText(found.from, {line: found.from.line, ch: found.from.ch + 1},
                          {className: style});
    var two = found.to && cm.markText(found.to, {line: found.to.line, ch: found.to.ch + 1},
                                      {className: style});
    var clear = function() {
      cm.operation(function() { one.clear(); two && two.clear(); });
    };
    if (autoclear) setTimeout(clear, 800);
    else return clear;
  }

  var currentlyHighlighted = null;
  function doMatchBrackets(cm) {
    cm.operation(function() {
      if (currentlyHighlighted) {currentlyHighlighted(); currentlyHighlighted = null;}
      if (!cm.somethingSelected()) currentlyHighlighted = matchBrackets(cm, false);
    });
  }

  CodeMirror.defineOption("matchBrackets", false, function(cm, val) {
    if (val) cm.on("cursorActivity", doMatchBrackets);
    else cm.off("cursorActivity", doMatchBrackets);
  });

  CodeMirror.defineExtension("matchBrackets", function() {matchBrackets(this, true);});
  CodeMirror.defineExtension("findMatchingBracket", function(){return findMatchingBracket(this);});
})();

(function() {
  CodeMirror.simpleHint = function(editor, getHints, givenOptions) {
    // Determine effective options based on given values and defaults.
    var options = {}, defaults = CodeMirror.simpleHint.defaults;
    for (var opt in defaults)
      if (defaults.hasOwnProperty(opt))
        options[opt] = (givenOptions && givenOptions.hasOwnProperty(opt) ? givenOptions : defaults)[opt];
    
    function collectHints(previousToken) {
      // We want a single cursor position.
      if (editor.somethingSelected()) return;

      var tempToken = editor.getTokenAt(editor.getCursor());

      // Don't show completions if token has changed and the option is set.
      if (options.closeOnTokenChange && previousToken != null &&
          (tempToken.start != previousToken.start || tempToken.type != previousToken.type)) {
        return;
      }

      var result = getHints(editor, givenOptions);
      if (!result || !result.list.length) return;
      var completions = result.list;
      function insert(str) {
        editor.replaceRange(str, result.from, result.to);
      }
      // When there is only one completion, use it directly.
      if (options.completeSingle && completions.length == 1) {
        insert(completions[0]);
        return true;
      }

      // Build the select widget
      var complete = document.createElement("div");
      complete.className = "CodeMirror-completions";
      var sel = complete.appendChild(document.createElement("select"));
      // Opera doesn't move the selection when pressing up/down in a
      // multi-select, but it does properly support the size property on
      // single-selects, so no multi-select is necessary.
      if (!window.opera) sel.multiple = true;
      for (var i = 0; i < completions.length; ++i) {
        var opt = sel.appendChild(document.createElement("option"));
        opt.appendChild(document.createTextNode(completions[i]));
      }
      sel.firstChild.selected = true;
      sel.size = Math.min(10, completions.length);
      var pos = editor.cursorCoords(options.alignWithWord ? result.from : null);
      complete.style.left = pos.left + "px";
      complete.style.top = pos.bottom + "px";
      document.body.appendChild(complete);
      // If we're at the edge of the screen, then we want the menu to appear on the left of the cursor.
      var winW = window.innerWidth || Math.max(document.body.offsetWidth, document.documentElement.offsetWidth);
      if(winW - pos.left < sel.clientWidth)
        complete.style.left = (pos.left - sel.clientWidth) + "px";
      // Hack to hide the scrollbar.
      if (completions.length <= 10)
        complete.style.width = (sel.clientWidth - 1) + "px";

      var done = false;
      function close() {
        if (done) return;
        done = true;
        complete.parentNode.removeChild(complete);
      }
      function pick() {
        insert(completions[sel.selectedIndex]);
        close();
        setTimeout(function(){editor.focus();}, 50);
      }
      CodeMirror.on(sel, "blur", close);
      CodeMirror.on(sel, "keydown", function(event) {
        var code = event.keyCode;
        // Enter
        if (code == 13) {CodeMirror.e_stop(event); pick();}
        // Escape
        else if (code == 27) {CodeMirror.e_stop(event); close(); editor.focus();}
        else if (code != 38 && code != 40 && code != 33 && code != 34 && !CodeMirror.isModifierKey(event)) {
          close(); editor.focus();
          // Pass the event to the CodeMirror instance so that it can handle things like backspace properly.
          editor.triggerOnKeyDown(event);
          // Don't show completions if the code is backspace and the option is set.
          if (!options.closeOnBackspace || code != 8) {
            setTimeout(function(){collectHints(tempToken);}, 50);
          }
        }
      });
      CodeMirror.on(sel, "dblclick", pick);

      sel.focus();
      // Opera sometimes ignores focusing a freshly created node
      if (window.opera) setTimeout(function(){if (!done) sel.focus();}, 100);
      return true;
    }
    return collectHints();
  };
  CodeMirror.simpleHint.defaults = {
    closeOnBackspace: true,
    closeOnTokenChange: false,
    completeSingle: true,
    alignWithWord: true
  };
})();

CodeMirror.defineMode("python", function(conf, parserConf) {
    var ERRORCLASS = 'error';
    
    function wordRegexp(words) {
        return new RegExp("^((" + words.join(")|(") + "))\\b");
    }
    
    var singleOperators = new RegExp("^[\\+\\-\\*/%&|\\^~<>!]");
    var singleDelimiters = new RegExp('^[\\(\\)\\[\\]\\{\\}@,:`=;\\.]');
    var doubleOperators = new RegExp("^((==)|(!=)|(<=)|(>=)|(<>)|(<<)|(>>)|(//)|(\\*\\*))");
    var doubleDelimiters = new RegExp("^((\\+=)|(\\-=)|(\\*=)|(%=)|(/=)|(&=)|(\\|=)|(\\^=))");
    var tripleDelimiters = new RegExp("^((//=)|(>>=)|(<<=)|(\\*\\*=))");
    var identifiers = new RegExp("^[_A-Za-z][_A-Za-z0-9]*");

    var wordOperators = wordRegexp(['and', 'or', 'not', 'is', 'in']);
    var commonkeywords = ['as', 'assert', 'break', 'class', 'continue',
                          'def', 'del', 'elif', 'else', 'except', 'finally',
                          'for', 'from', 'global', 'if', 'import',
                          'lambda', 'pass', 'raise', 'return',
                          'try', 'while', 'with', 'yield'];
    var commonBuiltins = ['abs', 'all', 'any', 'bin', 'bool', 'bytearray', 'callable', 'chr',
                          'classmethod', 'compile', 'complex', 'delattr', 'dict', 'dir', 'divmod',
                          'enumerate', 'eval', 'filter', 'float', 'format', 'frozenset',
                          'getattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id',
                          'input', 'int', 'isinstance', 'issubclass', 'iter', 'len',
                          'list', 'locals', 'map', 'max', 'memoryview', 'min', 'next',
                          'object', 'oct', 'open', 'ord', 'pow', 'property', 'range',
                          'repr', 'reversed', 'round', 'set', 'setattr', 'slice',
                          'sorted', 'staticmethod', 'str', 'sum', 'super', 'tuple',
                          'type', 'vars', 'zip', '__import__', 'NotImplemented',
                          'Ellipsis', '__debug__'];
    var py2 = {'builtins': ['apply', 'basestring', 'buffer', 'cmp', 'coerce', 'execfile',
                            'file', 'intern', 'long', 'raw_input', 'reduce', 'reload',
                            'unichr', 'unicode', 'xrange', 'False', 'True', 'None'],
               'keywords': ['exec', 'print']};
    var py3 = {'builtins': ['ascii', 'bytes', 'exec', 'print'],
               'keywords': ['nonlocal', 'False', 'True', 'None']};

    if (!!parserConf.version && parseInt(parserConf.version, 10) === 3) {
        commonkeywords = commonkeywords.concat(py3.keywords);
        commonBuiltins = commonBuiltins.concat(py3.builtins);
        var stringPrefixes = new RegExp("^(([rb]|(br))?('{3}|\"{3}|['\"]))", "i");
    } else {
        commonkeywords = commonkeywords.concat(py2.keywords);
        commonBuiltins = commonBuiltins.concat(py2.builtins);
        var stringPrefixes = new RegExp("^(([rub]|(ur)|(br))?('{3}|\"{3}|['\"]))", "i");
    }
    var keywords = wordRegexp(commonkeywords);
    var builtins = wordRegexp(commonBuiltins);

    var indentInfo = null;

    // tokenizers
    function tokenBase(stream, state) {
        // Handle scope changes
        if (stream.sol()) {
            var scopeOffset = state.scopes[0].offset;
            if (stream.eatSpace()) {
                var lineOffset = stream.indentation();
                if (lineOffset > scopeOffset) {
                    indentInfo = 'indent';
                } else if (lineOffset < scopeOffset) {
                    indentInfo = 'dedent';
                }
                return null;
            } else {
                if (scopeOffset > 0) {
                    dedent(stream, state);
                }
            }
        }
        if (stream.eatSpace()) {
            return null;
        }
        
        var ch = stream.peek();
        
        // Handle Comments
        if (ch === '#') {
            stream.skipToEnd();
            return 'comment';
        }
        
        // Handle Number Literals
        if (stream.match(/^[0-9\.]/, false)) {
            var floatLiteral = false;
            // Floats
            if (stream.match(/^\d*\.\d+(e[\+\-]?\d+)?/i)) { floatLiteral = true; }
            if (stream.match(/^\d+\.\d*/)) { floatLiteral = true; }
            if (stream.match(/^\.\d+/)) { floatLiteral = true; }
            if (floatLiteral) {
                // Float literals may be "imaginary"
                stream.eat(/J/i);
                return 'number';
            }
            // Integers
            var intLiteral = false;
            // Hex
            if (stream.match(/^0x[0-9a-f]+/i)) { intLiteral = true; }
            // Binary
            if (stream.match(/^0b[01]+/i)) { intLiteral = true; }
            // Octal
            if (stream.match(/^0o[0-7]+/i)) { intLiteral = true; }
            // Decimal
            if (stream.match(/^[1-9]\d*(e[\+\-]?\d+)?/)) {
                // Decimal literals may be "imaginary"
                stream.eat(/J/i);
                // TODO - Can you have imaginary longs?
                intLiteral = true;
            }
            // Zero by itself with no other piece of number.
            if (stream.match(/^0(?![\dx])/i)) { intLiteral = true; }
            if (intLiteral) {
                // Integer literals may be "long"
                stream.eat(/L/i);
                return 'number';
            }
        }
        
        // Handle Strings
        if (stream.match(stringPrefixes)) {
            state.tokenize = tokenStringFactory(stream.current());
            return state.tokenize(stream, state);
        }
        
        // Handle operators and Delimiters
        if (stream.match(tripleDelimiters) || stream.match(doubleDelimiters)) {
            return null;
        }
        if (stream.match(doubleOperators)
            || stream.match(singleOperators)
            || stream.match(wordOperators)) {
            return 'operator';
        }
        if (stream.match(singleDelimiters)) {
            return null;
        }
        
        if (stream.match(keywords)) {
            return 'keyword';
        }
        
        if (stream.match(builtins)) {
            return 'builtin';
        }
        
        if (stream.match(identifiers)) {
            return 'variable';
        }
        
        // Handle non-detected items
        stream.next();
        return ERRORCLASS;
    }
    
    function tokenStringFactory(delimiter) {
        while ('rub'.indexOf(delimiter.charAt(0).toLowerCase()) >= 0) {
            delimiter = delimiter.substr(1);
        }
        var singleline = delimiter.length == 1;
        var OUTCLASS = 'string';
        
        function tokenString(stream, state) {
            while (!stream.eol()) {
                stream.eatWhile(/[^'"\\]/);
                if (stream.eat('\\')) {
                    stream.next();
                    if (singleline && stream.eol()) {
                        return OUTCLASS;
                    }
                } else if (stream.match(delimiter)) {
                    state.tokenize = tokenBase;
                    return OUTCLASS;
                } else {
                    stream.eat(/['"]/);
                }
            }
            if (singleline) {
                if (parserConf.singleLineStringErrors) {
                    return ERRORCLASS;
                } else {
                    state.tokenize = tokenBase;
                }
            }
            return OUTCLASS;
        }
        tokenString.isString = true;
        return tokenString;
    }
    
    function indent(stream, state, type) {
        type = type || 'py';
        var indentUnit = 0;
        if (type === 'py') {
            if (state.scopes[0].type !== 'py') {
                state.scopes[0].offset = stream.indentation();
                return;
            }
            for (var i = 0; i < state.scopes.length; ++i) {
                if (state.scopes[i].type === 'py') {
                    indentUnit = state.scopes[i].offset + conf.indentUnit;
                    break;
                }
            }
        } else {
            indentUnit = stream.column() + stream.current().length;
        }
        state.scopes.unshift({
            offset: indentUnit,
            type: type
        });
    }
    
    function dedent(stream, state, type) {
        type = type || 'py';
        if (state.scopes.length == 1) return;
        if (state.scopes[0].type === 'py') {
            var _indent = stream.indentation();
            var _indent_index = -1;
            for (var i = 0; i < state.scopes.length; ++i) {
                if (_indent === state.scopes[i].offset) {
                    _indent_index = i;
                    break;
                }
            }
            if (_indent_index === -1) {
                return true;
            }
            while (state.scopes[0].offset !== _indent) {
                state.scopes.shift();
            }
            return false;
        } else {
            if (type === 'py') {
                state.scopes[0].offset = stream.indentation();
                return false;
            } else {
                if (state.scopes[0].type != type) {
                    return true;
                }
                state.scopes.shift();
                return false;
            }
        }
    }

    function tokenLexer(stream, state) {
        indentInfo = null;
        var style = state.tokenize(stream, state);
        var current = stream.current();

        // Handle '.' connected identifiers
        if (current === '.') {
            style = stream.match(identifiers, false) ? null : ERRORCLASS;
            if (style === null && state.lastToken === 'meta') {
                // Apply 'meta' style to '.' connected identifiers when
                // appropriate.
                style = 'meta';
            }
            return style;
        }
        
        // Handle decorators
        if (current === '@') {
            return stream.match(identifiers, false) ? 'meta' : ERRORCLASS;
        }

        if ((style === 'variable' || style === 'builtin')
            && state.lastToken === 'meta') {
            style = 'meta';
        }
        
        // Handle scope changes.
        if (current === 'pass' || current === 'return') {
            state.dedent += 1;
        }
        if (current === 'lambda') state.lambda = true;
        if ((current === ':' && !state.lambda && state.scopes[0].type == 'py')
            || indentInfo === 'indent') {
            indent(stream, state);
        }
        var delimiter_index = '[({'.indexOf(current);
        if (delimiter_index !== -1) {
            indent(stream, state, '])}'.slice(delimiter_index, delimiter_index+1));
        }
        if (indentInfo === 'dedent') {
            if (dedent(stream, state)) {
                return ERRORCLASS;
            }
        }
        delimiter_index = '])}'.indexOf(current);
        if (delimiter_index !== -1) {
            if (dedent(stream, state, current)) {
                return ERRORCLASS;
            }
        }
        if (state.dedent > 0 && stream.eol() && state.scopes[0].type == 'py') {
            if (state.scopes.length > 1) state.scopes.shift();
            state.dedent -= 1;
        }
        
        return style;
    }

    var external = {
        startState: function(basecolumn) {
            return {
              tokenize: tokenBase,
              scopes: [{offset:basecolumn || 0, type:'py'}],
              lastToken: null,
              lambda: false,
              dedent: 0
          };
        },
        
        token: function(stream, state) {
            var style = tokenLexer(stream, state);
            
            state.lastToken = style;
            
            if (stream.eol() && stream.lambda) {
                state.lambda = false;
            }
            
            return style;
        },
        
        indent: function(state) {
            if (state.tokenize != tokenBase) {
                return state.tokenize.isString ? CodeMirror.Pass : 0;
            }
            
            return state.scopes[0].offset;
        }
        
    };
    return external;
});

CodeMirror.defineMIME("text/x-python", "python");

/*
 * Author: Constantin Jucovschi (c.jucovschi@jacobs-university.de)
 * Licence: MIT
 */

CodeMirror.defineMode("stex", function() 
{    
    function pushCommand(state, command) {
	state.cmdState.push(command);
    }

    function peekCommand(state) { 
	if (state.cmdState.length>0)
	    return state.cmdState[state.cmdState.length-1];
	else
	    return null;
    }

    function popCommand(state) {
	if (state.cmdState.length>0) {
	    var plug = state.cmdState.pop();
	    plug.closeBracket();
	}	    
    }

    function applyMostPowerful(state) {
      var context = state.cmdState;
      for (var i = context.length - 1; i >= 0; i--) {
	  var plug = context[i];
	  if (plug.name=="DEFAULT")
	      continue;
	  return plug.styleIdentifier();
      }
      return null;
    }

    function addPluginPattern(pluginName, cmdStyle, brackets, styles) {
	return function () {
	    this.name=pluginName;
	    this.bracketNo = 0;
	    this.style=cmdStyle;
	    this.styles = styles;
	    this.brackets = brackets;

	    this.styleIdentifier = function() {
		if (this.bracketNo<=this.styles.length)
		    return this.styles[this.bracketNo-1];
		else
		    return null;
	    };
	    this.openBracket = function() {
		this.bracketNo++;
		return "bracket";
	    };
	    this.closeBracket = function() {};
	};
    }

    var plugins = new Array();
   
    plugins["importmodule"] = addPluginPattern("importmodule", "tag", "{[", ["string", "builtin"]);
    plugins["documentclass"] = addPluginPattern("documentclass", "tag", "{[", ["", "atom"]);
    plugins["usepackage"] = addPluginPattern("documentclass", "tag", "[", ["atom"]);
    plugins["begin"] = addPluginPattern("documentclass", "tag", "[", ["atom"]);
    plugins["end"] = addPluginPattern("documentclass", "tag", "[", ["atom"]);

    plugins["DEFAULT"] = function () {
	this.name="DEFAULT";
	this.style="tag";

	this.styleIdentifier = this.openBracket = this.closeBracket = function() {};
    };

    function setState(state, f) {
	state.f = f;
    }

    function normal(source, state) {
	if (source.match(/^\\[a-zA-Z@]+/)) {
	    var cmdName = source.current();
	    cmdName = cmdName.substr(1, cmdName.length-1);
            var plug;
            if (plugins.hasOwnProperty(cmdName)) {
	      plug = plugins[cmdName];
            } else {
              plug = plugins["DEFAULT"];
            }
	    plug = new plug();
	    pushCommand(state, plug);
	    setState(state, beginParams);
	    return plug.style;
	}

        // escape characters 
        if (source.match(/^\\[$&%#{}_]/)) {
          return "tag";
        }

        // white space control characters
        if (source.match(/^\\[,;!\/]/)) {
          return "tag";
        }

	var ch = source.next();
	if (ch == "%") {
            // special case: % at end of its own line; stay in same state
            if (!source.eol()) {
              setState(state, inCComment);
            }
	    return "comment";
	} 
	else if (ch=='}' || ch==']') {
	    plug = peekCommand(state);
	    if (plug) {
		plug.closeBracket(ch);
		setState(state, beginParams);
	    } else
		return "error";
	    return "bracket";
	} else if (ch=='{' || ch=='[') {
	    plug = plugins["DEFAULT"];	    
	    plug = new plug();
	    pushCommand(state, plug);
	    return "bracket";	    
	}
	else if (/\d/.test(ch)) {
	    source.eatWhile(/[\w.%]/);
	    return "atom";
	}
	else {
	    source.eatWhile(/[\w-_]/);
	    return applyMostPowerful(state);
	}
    }

    function inCComment(source, state) {
	source.skipToEnd();
	setState(state, normal);
	return "comment";
    }

    function beginParams(source, state) {
	var ch = source.peek();
	if (ch == '{' || ch == '[') {
	   var lastPlug = peekCommand(state);
	   lastPlug.openBracket(ch);
	   source.eat(ch);
	   setState(state, normal);
	   return "bracket";
	}
	if (/[ \t\r]/.test(ch)) {
	    source.eat(ch);
	    return null;
	}
	setState(state, normal);
	lastPlug = peekCommand(state);
	if (lastPlug) {
	    popCommand(state);
	}
        return normal(source, state);
    }

    return {
     startState: function() { return { f:normal, cmdState:[] }; },
	 copyState: function(s) { return { f: s.f, cmdState: s.cmdState.slice(0, s.cmdState.length) }; },
	 
	 token: function(stream, state) {
	 var t = state.f(stream, state);
	 return t;
     }
 };
});

CodeMirror.defineMIME("text/x-stex", "stex");
CodeMirror.defineMIME("text/x-latex", "stex");

CodeMirror.defineMode("r", function(config) {
  function wordObj(str) {
    var words = str.split(" "), res = {};
    for (var i = 0; i < words.length; ++i) res[words[i]] = true;
    return res;
  }
  var atoms = wordObj("NULL NA Inf NaN NA_integer_ NA_real_ NA_complex_ NA_character_");
  var builtins = wordObj("list quote bquote eval return call parse deparse");
  var keywords = wordObj("if else repeat while function for in next break");
  var blockkeywords = wordObj("if else repeat while function for");
  var opChars = /[+\-*\/^<>=!&|~$:]/;
  var curPunc;

  function tokenBase(stream, state) {
    curPunc = null;
    var ch = stream.next();
    if (ch == "#") {
      stream.skipToEnd();
      return "comment";
    } else if (ch == "0" && stream.eat("x")) {
      stream.eatWhile(/[\da-f]/i);
      return "number";
    } else if (ch == "." && stream.eat(/\d/)) {
      stream.match(/\d*(?:e[+\-]?\d+)?/);
      return "number";
    } else if (/\d/.test(ch)) {
      stream.match(/\d*(?:\.\d+)?(?:e[+\-]\d+)?L?/);
      return "number";
    } else if (ch == "'" || ch == '"') {
      state.tokenize = tokenString(ch);
      return "string";
    } else if (ch == "." && stream.match(/.[.\d]+/)) {
      return "keyword";
    } else if (/[\w\.]/.test(ch) && ch != "_") {
      stream.eatWhile(/[\w\.]/);
      var word = stream.current();
      if (atoms.propertyIsEnumerable(word)) return "atom";
      if (keywords.propertyIsEnumerable(word)) {
        if (blockkeywords.propertyIsEnumerable(word)) curPunc = "block";
        return "keyword";
      }
      if (builtins.propertyIsEnumerable(word)) return "builtin";
      return "variable";
    } else if (ch == "%") {
      if (stream.skipTo("%")) stream.next();
      return "variable-2";
    } else if (ch == "<" && stream.eat("-")) {
      return "arrow";
    } else if (ch == "=" && state.ctx.argList) {
      return "arg-is";
    } else if (opChars.test(ch)) {
      if (ch == "$") return "dollar";
      stream.eatWhile(opChars);
      return "operator";
    } else if (/[\(\){}\[\];]/.test(ch)) {
      curPunc = ch;
      if (ch == ";") return "semi";
      return null;
    } else {
      return null;
    }
  }

  function tokenString(quote) {
    return function(stream, state) {
      if (stream.eat("\\")) {
        var ch = stream.next();
        if (ch == "x") stream.match(/^[a-f0-9]{2}/i);
        else if ((ch == "u" || ch == "U") && stream.eat("{") && stream.skipTo("}")) stream.next();
        else if (ch == "u") stream.match(/^[a-f0-9]{4}/i);
        else if (ch == "U") stream.match(/^[a-f0-9]{8}/i);
        else if (/[0-7]/.test(ch)) stream.match(/^[0-7]{1,2}/);
        return "string-2";
      } else {
        var next;
        while ((next = stream.next()) != null) {
          if (next == quote) { state.tokenize = tokenBase; break; }
          if (next == "\\") { stream.backUp(1); break; }
        }
        return "string";
      }
    };
  }

  function push(state, type, stream) {
    state.ctx = {type: type,
                 indent: state.indent,
                 align: null,
                 column: stream.column(),
                 prev: state.ctx};
  }
  function pop(state) {
    state.indent = state.ctx.indent;
    state.ctx = state.ctx.prev;
  }

  return {
    startState: function() {
      return {tokenize: tokenBase,
              ctx: {type: "top",
                    indent: -config.indentUnit,
                    align: false},
              indent: 0,
              afterIdent: false};
    },

    token: function(stream, state) {
      if (stream.sol()) {
        if (state.ctx.align == null) state.ctx.align = false;
        state.indent = stream.indentation();
      }
      if (stream.eatSpace()) return null;
      var style = state.tokenize(stream, state);
      if (style != "comment" && state.ctx.align == null) state.ctx.align = true;

      var ctype = state.ctx.type;
      if ((curPunc == ";" || curPunc == "{" || curPunc == "}") && ctype == "block") pop(state);
      if (curPunc == "{") push(state, "}", stream);
      else if (curPunc == "(") {
        push(state, ")", stream);
        if (state.afterIdent) state.ctx.argList = true;
      }
      else if (curPunc == "[") push(state, "]", stream);
      else if (curPunc == "block") push(state, "block", stream);
      else if (curPunc == ctype) pop(state);
      state.afterIdent = style == "variable" || style == "keyword";
      return style;
    },

    indent: function(state, textAfter) {
      if (state.tokenize != tokenBase) return 0;
      var firstChar = textAfter && textAfter.charAt(0), ctx = state.ctx,
          closing = firstChar == ctx.type;
      if (ctx.type == "block") return ctx.indent + (firstChar == "{" ? 0 : config.indentUnit);
      else if (ctx.align) return ctx.column + (closing ? 0 : 1);
      else return ctx.indent + (closing ? 0 : config.indentUnit);
    }
  };
});

CodeMirror.defineMIME("text/x-rsrc", "r");

CodeMirror.defineMode('shell', function() {

  var words = {};
  function define(style, string) {
    var split = string.split(' ');
    for(var i = 0; i < split.length; i++) {
      words[split[i]] = style;
    }
  };

  // Atoms
  define('atom', 'true false');

  // Keywords
  define('keyword', 'if then do else elif while until for in esac fi fin ' +
    'fil done exit set unset export function');

  // Commands
  define('builtin', 'ab awk bash beep cat cc cd chown chmod chroot clear cp ' +
    'curl cut diff echo find gawk gcc get git grep kill killall ln ls make ' +
    'mkdir openssl mv nc node npm ping ps restart rm rmdir sed service sh ' +
    'shopt shred source sort sleep ssh start stop su sudo tee telnet top ' +
    'touch vi vim wall wc wget who write yes zsh');

  function tokenBase(stream, state) {

    var sol = stream.sol();
    var ch = stream.next();

    if (ch === '\'' || ch === '"' || ch === '`') {
      state.tokens.unshift(tokenString(ch));
      return tokenize(stream, state);
    }
    if (ch === '#') {
      if (sol && stream.eat('!')) {
        stream.skipToEnd();
        return 'meta'; // 'comment'?
      }
      stream.skipToEnd();
      return 'comment';
    }
    if (ch === '$') {
      state.tokens.unshift(tokenDollar);
      return tokenize(stream, state);
    }
    if (ch === '+' || ch === '=') {
      return 'operator';
    }
    if (ch === '-') {
      stream.eat('-');
      stream.eatWhile(/\w/);
      return 'attribute';
    }
    if (/\d/.test(ch)) {
      stream.eatWhile(/\d/);
      if(!/\w/.test(stream.peek())) {
        return 'number';
      }
    }
    stream.eatWhile(/\w/);
    var cur = stream.current();
    if (stream.peek() === '=' && /\w+/.test(cur)) return 'def';
    return words.hasOwnProperty(cur) ? words[cur] : null;
  }

  function tokenString(quote) {
    return function(stream, state) {
      var next, end = false, escaped = false;
      while ((next = stream.next()) != null) {
        if (next === quote && !escaped) {
          end = true;
          break;
        }
        if (next === '$' && !escaped && quote !== '\'') {
          escaped = true;
          stream.backUp(1);
          state.tokens.unshift(tokenDollar);
          break;
        }
        escaped = !escaped && next === '\\';
      }
      if (end || !escaped) {
        state.tokens.shift();
      }
      return (quote === '`' || quote === ')' ? 'quote' : 'string');
    };
  };

  var tokenDollar = function(stream, state) {
    if (state.tokens.length > 1) stream.eat('$');
    var ch = stream.next(), hungry = /\w/;
    if (ch === '{') hungry = /[^}]/;
    if (ch === '(') {
      state.tokens[0] = tokenString(')');
      return tokenize(stream, state);
    }
    if (!/\d/.test(ch)) {
      stream.eatWhile(hungry);
      stream.eat('}');
    }
    state.tokens.shift();
    return 'def';
  };

  function tokenize(stream, state) {
    return (state.tokens[0] || tokenBase) (stream, state);
  };

  return {
    startState: function() {return {tokens:[]};},
    token: function(stream, state) {
      if (stream.eatSpace()) return null;
      return tokenize(stream, state);
    }
  };
});
  
CodeMirror.defineMIME('text/x-sh', 'shell');

CodeMirror.defineMode("xml", function(config, parserConfig) {
  var indentUnit = config.indentUnit;
  var Kludges = parserConfig.htmlMode ? {
    autoSelfClosers: {'area': true, 'base': true, 'br': true, 'col': true, 'command': true,
                      'embed': true, 'frame': true, 'hr': true, 'img': true, 'input': true,
                      'keygen': true, 'link': true, 'meta': true, 'param': true, 'source': true,
                      'track': true, 'wbr': true},
    implicitlyClosed: {'dd': true, 'li': true, 'optgroup': true, 'option': true, 'p': true,
                       'rp': true, 'rt': true, 'tbody': true, 'td': true, 'tfoot': true,
                       'th': true, 'tr': true},
    contextGrabbers: {
      'dd': {'dd': true, 'dt': true},
      'dt': {'dd': true, 'dt': true},
      'li': {'li': true},
      'option': {'option': true, 'optgroup': true},
      'optgroup': {'optgroup': true},
      'p': {'address': true, 'article': true, 'aside': true, 'blockquote': true, 'dir': true,
            'div': true, 'dl': true, 'fieldset': true, 'footer': true, 'form': true,
            'h1': true, 'h2': true, 'h3': true, 'h4': true, 'h5': true, 'h6': true,
            'header': true, 'hgroup': true, 'hr': true, 'menu': true, 'nav': true, 'ol': true,
            'p': true, 'pre': true, 'section': true, 'table': true, 'ul': true},
      'rp': {'rp': true, 'rt': true},
      'rt': {'rp': true, 'rt': true},
      'tbody': {'tbody': true, 'tfoot': true},
      'td': {'td': true, 'th': true},
      'tfoot': {'tbody': true},
      'th': {'td': true, 'th': true},
      'thead': {'tbody': true, 'tfoot': true},
      'tr': {'tr': true}
    },
    doNotIndent: {"pre": true},
    allowUnquoted: true,
    allowMissing: true
  } : {
    autoSelfClosers: {},
    implicitlyClosed: {},
    contextGrabbers: {},
    doNotIndent: {},
    allowUnquoted: false,
    allowMissing: false
  };
  var alignCDATA = parserConfig.alignCDATA;

  // Return variables for tokenizers
  var tagName, type;

  function inText(stream, state) {
    function chain(parser) {
      state.tokenize = parser;
      return parser(stream, state);
    }

    var ch = stream.next();
    if (ch == "<") {
      if (stream.eat("!")) {
        if (stream.eat("[")) {
          if (stream.match("CDATA[")) return chain(inBlock("atom", "]]>"));
          else return null;
        }
        else if (stream.match("--")) return chain(inBlock("comment", "-->"));
        else if (stream.match("DOCTYPE", true, true)) {
          stream.eatWhile(/[\w\._\-]/);
          return chain(doctype(1));
        }
        else return null;
      }
      else if (stream.eat("?")) {
        stream.eatWhile(/[\w\._\-]/);
        state.tokenize = inBlock("meta", "?>");
        return "meta";
      }
      else {
        var isClose = stream.eat("/");
        tagName = "";
        var c;
        while ((c = stream.eat(/[^\s\u00a0=<>\"\'\/?]/))) tagName += c;
        if (!tagName) return "error";
        type = isClose ? "closeTag" : "openTag";
        state.tokenize = inTag;
        return "tag";
      }
    }
    else if (ch == "&") {
      var ok;
      if (stream.eat("#")) {
        if (stream.eat("x")) {
          ok = stream.eatWhile(/[a-fA-F\d]/) && stream.eat(";");          
        } else {
          ok = stream.eatWhile(/[\d]/) && stream.eat(";");
        }
      } else {
        ok = stream.eatWhile(/[\w\.\-:]/) && stream.eat(";");
      }
      return ok ? "atom" : "error";
    }
    else {
      stream.eatWhile(/[^&<]/);
      return null;
    }
  }

  function inTag(stream, state) {
    var ch = stream.next();
    if (ch == ">" || (ch == "/" && stream.eat(">"))) {
      state.tokenize = inText;
      type = ch == ">" ? "endTag" : "selfcloseTag";
      return "tag";
    }
    else if (ch == "=") {
      type = "equals";
      return null;
    }
    else if (/[\'\"]/.test(ch)) {
      state.tokenize = inAttribute(ch);
      return state.tokenize(stream, state);
    }
    else {
      stream.eatWhile(/[^\s\u00a0=<>\"\']/);
      return "word";
    }
  }

  function inAttribute(quote) {
    return function(stream, state) {
      while (!stream.eol()) {
        if (stream.next() == quote) {
          state.tokenize = inTag;
          break;
        }
      }
      return "string";
    };
  }

  function inBlock(style, terminator) {
    return function(stream, state) {
      while (!stream.eol()) {
        if (stream.match(terminator)) {
          state.tokenize = inText;
          break;
        }
        stream.next();
      }
      return style;
    };
  }
  function doctype(depth) {
    return function(stream, state) {
      var ch;
      while ((ch = stream.next()) != null) {
        if (ch == "<") {
          state.tokenize = doctype(depth + 1);
          return state.tokenize(stream, state);
        } else if (ch == ">") {
          if (depth == 1) {
            state.tokenize = inText;
            break;
          } else {
            state.tokenize = doctype(depth - 1);
            return state.tokenize(stream, state);
          }
        }
      }
      return "meta";
    };
  }

  var curState, setStyle;
  function pass() {
    for (var i = arguments.length - 1; i >= 0; i--) curState.cc.push(arguments[i]);
  }
  function cont() {
    pass.apply(null, arguments);
    return true;
  }

  function pushContext(tagName, startOfLine) {
    var noIndent = Kludges.doNotIndent.hasOwnProperty(tagName) || (curState.context && curState.context.noIndent);
    curState.context = {
      prev: curState.context,
      tagName: tagName,
      indent: curState.indented,
      startOfLine: startOfLine,
      noIndent: noIndent
    };
  }
  function popContext() {
    if (curState.context) curState.context = curState.context.prev;
  }

  function element(type) {
    if (type == "openTag") {
      curState.tagName = tagName;
      return cont(attributes, endtag(curState.startOfLine));
    } else if (type == "closeTag") {
      var err = false;
      if (curState.context) {
        if (curState.context.tagName != tagName) {
          if (Kludges.implicitlyClosed.hasOwnProperty(curState.context.tagName.toLowerCase())) {
            popContext();
          }
          err = !curState.context || curState.context.tagName != tagName;
        }
      } else {
        err = true;
      }
      if (err) setStyle = "error";
      return cont(endclosetag(err));
    }
    return cont();
  }
  function endtag(startOfLine) {
    return function(type) {
      var tagName = curState.tagName;
      curState.tagName = null;
      if (type == "selfcloseTag" ||
          (type == "endTag" && Kludges.autoSelfClosers.hasOwnProperty(tagName.toLowerCase()))) {
        maybePopContext(tagName.toLowerCase());
        return cont();
      }
      if (type == "endTag") {
        maybePopContext(tagName.toLowerCase());
        pushContext(tagName, startOfLine);
        return cont();
      }
      return cont();
    };
  }
  function endclosetag(err) {
    return function(type) {
      if (err) setStyle = "error";
      if (type == "endTag") { popContext(); return cont(); }
      setStyle = "error";
      return cont(arguments.callee);
    };
  }
  function maybePopContext(nextTagName) {
    var parentTagName;
    while (true) {
      if (!curState.context) {
        return;
      }
      parentTagName = curState.context.tagName.toLowerCase();
      if (!Kludges.contextGrabbers.hasOwnProperty(parentTagName) ||
          !Kludges.contextGrabbers[parentTagName].hasOwnProperty(nextTagName)) {
        return;
      }
      popContext();
    }
  }

  function attributes(type) {
    if (type == "word") {setStyle = "attribute"; return cont(attribute, attributes);}
    if (type == "endTag" || type == "selfcloseTag") return pass();
    setStyle = "error";
    return cont(attributes);
  }
  function attribute(type) {
    if (type == "equals") return cont(attvalue, attributes);
    if (!Kludges.allowMissing) setStyle = "error";
    else if (type == "word") setStyle = "attribute";
    return (type == "endTag" || type == "selfcloseTag") ? pass() : cont();
  }
  function attvalue(type) {
    if (type == "string") return cont(attvaluemaybe);
    if (type == "word" && Kludges.allowUnquoted) {setStyle = "string"; return cont();}
    setStyle = "error";
    return (type == "endTag" || type == "selfCloseTag") ? pass() : cont();
  }
  function attvaluemaybe(type) {
    if (type == "string") return cont(attvaluemaybe);
    else return pass();
  }

  return {
    startState: function() {
      return {tokenize: inText, cc: [], indented: 0, startOfLine: true, tagName: null, context: null};
    },

    token: function(stream, state) {
      if (stream.sol()) {
        state.startOfLine = true;
        state.indented = stream.indentation();
      }
      if (stream.eatSpace()) return null;

      setStyle = type = tagName = null;
      var style = state.tokenize(stream, state);
      state.type = type;
      if ((style || type) && style != "comment") {
        curState = state;
        while (true) {
          var comb = state.cc.pop() || element;
          if (comb(type || style)) break;
        }
      }
      state.startOfLine = false;
      return setStyle || style;
    },

    indent: function(state, textAfter, fullLine) {
      var context = state.context;
      if ((state.tokenize != inTag && state.tokenize != inText) ||
          context && context.noIndent)
        return fullLine ? fullLine.match(/^(\s*)/)[0].length : 0;
      if (alignCDATA && /<!\[CDATA\[/.test(textAfter)) return 0;
      if (context && /^<\//.test(textAfter))
        context = context.prev;
      while (context && !context.startOfLine)
        context = context.prev;
      if (context) return context.indent + indentUnit;
      else return 0;
    },

    electricChars: "/",

    configuration: parserConfig.htmlMode ? "html" : "xml"
  };
});

CodeMirror.defineMIME("text/xml", "xml");
CodeMirror.defineMIME("application/xml", "xml");
if (!CodeMirror.mimeModes.hasOwnProperty("text/html"))
  CodeMirror.defineMIME("text/html", {name: "xml", htmlMode: true});

// TODO actually recognize syntax of TypeScript constructs

CodeMirror.defineMode("javascript", function(config, parserConfig) {
  var indentUnit = config.indentUnit;
  var jsonMode = parserConfig.json;
  var isTS = parserConfig.typescript;

  // Tokenizer

  var keywords = function(){
    function kw(type) {return {type: type, style: "keyword"};}
    var A = kw("keyword a"), B = kw("keyword b"), C = kw("keyword c");
    var operator = kw("operator"), atom = {type: "atom", style: "atom"};
    
    var jsKeywords = {
      "if": A, "while": A, "with": A, "else": B, "do": B, "try": B, "finally": B,
      "return": C, "break": C, "continue": C, "new": C, "delete": C, "throw": C,
      "var": kw("var"), "const": kw("var"), "let": kw("var"),
      "function": kw("function"), "catch": kw("catch"),
      "for": kw("for"), "switch": kw("switch"), "case": kw("case"), "default": kw("default"),
      "in": operator, "typeof": operator, "instanceof": operator,
      "true": atom, "false": atom, "null": atom, "undefined": atom, "NaN": atom, "Infinity": atom
    };

    // Extend the 'normal' keywords with the TypeScript language extensions
    if (isTS) {
      var type = {type: "variable", style: "variable-3"};
      var tsKeywords = {
        // object-like things
        "interface": kw("interface"),
        "class": kw("class"),
        "extends": kw("extends"),
        "constructor": kw("constructor"),

        // scope modifiers
        "public": kw("public"),
        "private": kw("private"),
        "protected": kw("protected"),
        "static": kw("static"),

        "super": kw("super"),

        // types
        "string": type, "number": type, "bool": type, "any": type
      };

      for (var attr in tsKeywords) {
        jsKeywords[attr] = tsKeywords[attr];
      }
    }

    return jsKeywords;
  }();

  var isOperatorChar = /[+\-*&%=<>!?|]/;

  function chain(stream, state, f) {
    state.tokenize = f;
    return f(stream, state);
  }

  function nextUntilUnescaped(stream, end) {
    var escaped = false, next;
    while ((next = stream.next()) != null) {
      if (next == end && !escaped)
        return false;
      escaped = !escaped && next == "\\";
    }
    return escaped;
  }

  // Used as scratch variables to communicate multiple values without
  // consing up tons of objects.
  var type, content;
  function ret(tp, style, cont) {
    type = tp; content = cont;
    return style;
  }

  function jsTokenBase(stream, state) {
    var ch = stream.next();
    if (ch == '"' || ch == "'")
      return chain(stream, state, jsTokenString(ch));
    else if (/[\[\]{}\(\),;\:\.]/.test(ch))
      return ret(ch);
    else if (ch == "0" && stream.eat(/x/i)) {
      stream.eatWhile(/[\da-f]/i);
      return ret("number", "number");
    }      
    else if (/\d/.test(ch) || ch == "-" && stream.eat(/\d/)) {
      stream.match(/^\d*(?:\.\d*)?(?:[eE][+\-]?\d+)?/);
      return ret("number", "number");
    }
    else if (ch == "/") {
      if (stream.eat("*")) {
        return chain(stream, state, jsTokenComment);
      }
      else if (stream.eat("/")) {
        stream.skipToEnd();
        return ret("comment", "comment");
      }
      else if (state.lastType == "operator" || state.lastType == "keyword c" ||
               /^[\[{}\(,;:]$/.test(state.lastType)) {
        nextUntilUnescaped(stream, "/");
        stream.eatWhile(/[gimy]/); // 'y' is "sticky" option in Mozilla
        return ret("regexp", "string-2");
      }
      else {
        stream.eatWhile(isOperatorChar);
        return ret("operator", null, stream.current());
      }
    }
    else if (ch == "#") {
        stream.skipToEnd();
        return ret("error", "error");
    }
    else if (isOperatorChar.test(ch)) {
      stream.eatWhile(isOperatorChar);
      return ret("operator", null, stream.current());
    }
    else {
      stream.eatWhile(/[\w\$_]/);
      var word = stream.current(), known = keywords.propertyIsEnumerable(word) && keywords[word];
      return (known && state.lastType != ".") ? ret(known.type, known.style, word) :
                     ret("variable", "variable", word);
    }
  }

  function jsTokenString(quote) {
    return function(stream, state) {
      if (!nextUntilUnescaped(stream, quote))
        state.tokenize = jsTokenBase;
      return ret("string", "string");
    };
  }

  function jsTokenComment(stream, state) {
    var maybeEnd = false, ch;
    while (ch = stream.next()) {
      if (ch == "/" && maybeEnd) {
        state.tokenize = jsTokenBase;
        break;
      }
      maybeEnd = (ch == "*");
    }
    return ret("comment", "comment");
  }

  // Parser

  var atomicTypes = {"atom": true, "number": true, "variable": true, "string": true, "regexp": true};

  function JSLexical(indented, column, type, align, prev, info) {
    this.indented = indented;
    this.column = column;
    this.type = type;
    this.prev = prev;
    this.info = info;
    if (align != null) this.align = align;
  }

  function inScope(state, varname) {
    for (var v = state.localVars; v; v = v.next)
      if (v.name == varname) return true;
  }

  function parseJS(state, style, type, content, stream) {
    var cc = state.cc;
    // Communicate our context to the combinators.
    // (Less wasteful than consing up a hundred closures on every call.)
    cx.state = state; cx.stream = stream; cx.marked = null, cx.cc = cc;
  
    if (!state.lexical.hasOwnProperty("align"))
      state.lexical.align = true;

    while(true) {
      var combinator = cc.length ? cc.pop() : jsonMode ? expression : statement;
      if (combinator(type, content)) {
        while(cc.length && cc[cc.length - 1].lex)
          cc.pop()();
        if (cx.marked) return cx.marked;
        if (type == "variable" && inScope(state, content)) return "variable-2";
        return style;
      }
    }
  }

  // Combinator utils

  var cx = {state: null, column: null, marked: null, cc: null};
  function pass() {
    for (var i = arguments.length - 1; i >= 0; i--) cx.cc.push(arguments[i]);
  }
  function cont() {
    pass.apply(null, arguments);
    return true;
  }
  function register(varname) {
    var state = cx.state;
    if (state.context) {
      cx.marked = "def";
      for (var v = state.localVars; v; v = v.next)
        if (v.name == varname) return;
      state.localVars = {name: varname, next: state.localVars};
    }
  }

  // Combinators

  var defaultVars = {name: "this", next: {name: "arguments"}};
  function pushcontext() {
    cx.state.context = {prev: cx.state.context, vars: cx.state.localVars};
    cx.state.localVars = defaultVars;
  }
  function popcontext() {
    cx.state.localVars = cx.state.context.vars;
    cx.state.context = cx.state.context.prev;
  }
  function pushlex(type, info) {
    var result = function() {
      var state = cx.state;
      state.lexical = new JSLexical(state.indented, cx.stream.column(), type, null, state.lexical, info);
    };
    result.lex = true;
    return result;
  }
  function poplex() {
    var state = cx.state;
    if (state.lexical.prev) {
      if (state.lexical.type == ")")
        state.indented = state.lexical.indented;
      state.lexical = state.lexical.prev;
    }
  }
  poplex.lex = true;

  function expect(wanted) {
    return function expecting(type) {
      if (type == wanted) return cont();
      else if (wanted == ";") return pass();
      else return cont(arguments.callee);
    };
  }

  function statement(type) {
    if (type == "var") return cont(pushlex("vardef"), vardef1, expect(";"), poplex);
    if (type == "keyword a") return cont(pushlex("form"), expression, statement, poplex);
    if (type == "keyword b") return cont(pushlex("form"), statement, poplex);
    if (type == "{") return cont(pushlex("}"), block, poplex);
    if (type == ";") return cont();
    if (type == "function") return cont(functiondef);
    if (type == "for") return cont(pushlex("form"), expect("("), pushlex(")"), forspec1, expect(")"),
                                      poplex, statement, poplex);
    if (type == "variable") return cont(pushlex("stat"), maybelabel);
    if (type == "switch") return cont(pushlex("form"), expression, pushlex("}", "switch"), expect("{"),
                                         block, poplex, poplex);
    if (type == "case") return cont(expression, expect(":"));
    if (type == "default") return cont(expect(":"));
    if (type == "catch") return cont(pushlex("form"), pushcontext, expect("("), funarg, expect(")"),
                                        statement, poplex, popcontext);
    return pass(pushlex("stat"), expression, expect(";"), poplex);
  }
  function expression(type) {
    if (atomicTypes.hasOwnProperty(type)) return cont(maybeoperator);
    if (type == "function") return cont(functiondef);
    if (type == "keyword c") return cont(maybeexpression);
    if (type == "(") return cont(pushlex(")"), maybeexpression, expect(")"), poplex, maybeoperator);
    if (type == "operator") return cont(expression);
    if (type == "[") return cont(pushlex("]"), commasep(expression, "]"), poplex, maybeoperator);
    if (type == "{") return cont(pushlex("}"), commasep(objprop, "}"), poplex, maybeoperator);
    return cont();
  }
  function maybeexpression(type) {
    if (type.match(/[;\}\)\],]/)) return pass();
    return pass(expression);
  }
    
  function maybeoperator(type, value) {
    if (type == "operator" && /\+\+|--/.test(value)) return cont(maybeoperator);
    if (type == "operator" && value == "?") return cont(expression, expect(":"), expression);
    if (type == ";") return;
    if (type == "(") return cont(pushlex(")"), commasep(expression, ")"), poplex, maybeoperator);
    if (type == ".") return cont(property, maybeoperator);
    if (type == "[") return cont(pushlex("]"), expression, expect("]"), poplex, maybeoperator);
  }
  function maybelabel(type) {
    if (type == ":") return cont(poplex, statement);
    return pass(maybeoperator, expect(";"), poplex);
  }
  function property(type) {
    if (type == "variable") {cx.marked = "property"; return cont();}
  }
  function objprop(type) {
    if (type == "variable") cx.marked = "property";
    if (atomicTypes.hasOwnProperty(type)) return cont(expect(":"), expression);
  }
  function commasep(what, end) {
    function proceed(type) {
      if (type == ",") return cont(what, proceed);
      if (type == end) return cont();
      return cont(expect(end));
    }
    return function commaSeparated(type) {
      if (type == end) return cont();
      else return pass(what, proceed);
    };
  }
  function block(type) {
    if (type == "}") return cont();
    return pass(statement, block);
  }
  function maybetype(type) {
    if (type == ":") return cont(typedef);
    return pass();
  }
  function typedef(type) {
    if (type == "variable"){cx.marked = "variable-3"; return cont();}
    return pass();
  }
  function vardef1(type, value) {
    if (type == "variable") {
      register(value);
      return isTS ? cont(maybetype, vardef2) : cont(vardef2);
    }
    return pass();
  }
  function vardef2(type, value) {
    if (value == "=") return cont(expression, vardef2);
    if (type == ",") return cont(vardef1);
  }
  function forspec1(type) {
    if (type == "var") return cont(vardef1, expect(";"), forspec2);
    if (type == ";") return cont(forspec2);
    if (type == "variable") return cont(formaybein);
    return cont(forspec2);
  }
  function formaybein(_type, value) {
    if (value == "in") return cont(expression);
    return cont(maybeoperator, forspec2);
  }
  function forspec2(type, value) {
    if (type == ";") return cont(forspec3);
    if (value == "in") return cont(expression);
    return cont(expression, expect(";"), forspec3);
  }
  function forspec3(type) {
    if (type != ")") cont(expression);
  }
  function functiondef(type, value) {
    if (type == "variable") {register(value); return cont(functiondef);}
    if (type == "(") return cont(pushlex(")"), pushcontext, commasep(funarg, ")"), poplex, statement, popcontext);
  }
  function funarg(type, value) {
    if (type == "variable") {register(value); return isTS ? cont(maybetype) : cont();}
  }

  // Interface

  return {
    startState: function(basecolumn) {
      return {
        tokenize: jsTokenBase,
        lastType: null,
        cc: [],
        lexical: new JSLexical((basecolumn || 0) - indentUnit, 0, "block", false),
        localVars: parserConfig.localVars,
        context: parserConfig.localVars && {vars: parserConfig.localVars},
        indented: 0
      };
    },

    token: function(stream, state) {
      if (stream.sol()) {
        if (!state.lexical.hasOwnProperty("align"))
          state.lexical.align = false;
        state.indented = stream.indentation();
      }
      if (stream.eatSpace()) return null;
      var style = state.tokenize(stream, state);
      if (type == "comment") return style;
      state.lastType = type;
      return parseJS(state, style, type, content, stream);
    },

    indent: function(state, textAfter) {
      if (state.tokenize == jsTokenComment) return CodeMirror.Pass;
      if (state.tokenize != jsTokenBase) return 0;
      var firstChar = textAfter && textAfter.charAt(0), lexical = state.lexical;
      if (lexical.type == "stat" && firstChar == "}") lexical = lexical.prev;
      var type = lexical.type, closing = firstChar == type;
      if (type == "vardef") return lexical.indented + (state.lastType == "operator" || state.lastType == "," ? 4 : 0);
      else if (type == "form" && firstChar == "{") return lexical.indented;
      else if (type == "form") return lexical.indented + indentUnit;
      else if (type == "stat")
        return lexical.indented + (state.lastType == "operator" || state.lastType == "," ? indentUnit : 0);
      else if (lexical.info == "switch" && !closing)
        return lexical.indented + (/^(?:case|default)\b/.test(textAfter) ? indentUnit : 2 * indentUnit);
      else if (lexical.align) return lexical.column + (closing ? 0 : 1);
      else return lexical.indented + (closing ? 0 : indentUnit);
    },

    electricChars: ":{}",

    jsonMode: jsonMode
  };
});

CodeMirror.defineMIME("text/javascript", "javascript");
CodeMirror.defineMIME("application/json", {name: "javascript", json: true});
CodeMirror.defineMIME("text/typescript", { name: "javascript", typescript: true });
CodeMirror.defineMIME("application/typescript", { name: "javascript", typescript: true });

CodeMirror.defineMode("css", function(config) {
  var indentUnit = config.indentUnit, type;
  
  var atMediaTypes = keySet([
    "all", "aural", "braille", "handheld", "print", "projection", "screen",
    "tty", "tv", "embossed"
  ]);
  
  var atMediaFeatures = keySet([
    "width", "min-width", "max-width", "height", "min-height", "max-height",
    "device-width", "min-device-width", "max-device-width", "device-height",
    "min-device-height", "max-device-height", "aspect-ratio",
    "min-aspect-ratio", "max-aspect-ratio", "device-aspect-ratio",
    "min-device-aspect-ratio", "max-device-aspect-ratio", "color", "min-color",
    "max-color", "color-index", "min-color-index", "max-color-index",
    "monochrome", "min-monochrome", "max-monochrome", "resolution",
    "min-resolution", "max-resolution", "scan", "grid"
  ]);

  var propertyKeywords = keySet([
    "align-content", "align-items", "align-self", "alignment-adjust",
    "alignment-baseline", "anchor-point", "animation", "animation-delay",
    "animation-direction", "animation-duration", "animation-iteration-count",
    "animation-name", "animation-play-state", "animation-timing-function",
    "appearance", "azimuth", "backface-visibility", "background",
    "background-attachment", "background-clip", "background-color",
    "background-image", "background-origin", "background-position",
    "background-repeat", "background-size", "baseline-shift", "binding",
    "bleed", "bookmark-label", "bookmark-level", "bookmark-state",
    "bookmark-target", "border", "border-bottom", "border-bottom-color",
    "border-bottom-left-radius", "border-bottom-right-radius",
    "border-bottom-style", "border-bottom-width", "border-collapse",
    "border-color", "border-image", "border-image-outset",
    "border-image-repeat", "border-image-slice", "border-image-source",
    "border-image-width", "border-left", "border-left-color",
    "border-left-style", "border-left-width", "border-radius", "border-right",
    "border-right-color", "border-right-style", "border-right-width",
    "border-spacing", "border-style", "border-top", "border-top-color",
    "border-top-left-radius", "border-top-right-radius", "border-top-style",
    "border-top-width", "border-width", "bottom", "box-decoration-break",
    "box-shadow", "box-sizing", "break-after", "break-before", "break-inside",
    "caption-side", "clear", "clip", "color", "color-profile", "column-count",
    "column-fill", "column-gap", "column-rule", "column-rule-color",
    "column-rule-style", "column-rule-width", "column-span", "column-width",
    "columns", "content", "counter-increment", "counter-reset", "crop", "cue",
    "cue-after", "cue-before", "cursor", "direction", "display",
    "dominant-baseline", "drop-initial-after-adjust",
    "drop-initial-after-align", "drop-initial-before-adjust",
    "drop-initial-before-align", "drop-initial-size", "drop-initial-value",
    "elevation", "empty-cells", "fit", "fit-position", "flex", "flex-basis",
    "flex-direction", "flex-flow", "flex-grow", "flex-shrink", "flex-wrap",
    "float", "float-offset", "font", "font-feature-settings", "font-family",
    "font-kerning", "font-language-override", "font-size", "font-size-adjust",
    "font-stretch", "font-style", "font-synthesis", "font-variant",
    "font-variant-alternates", "font-variant-caps", "font-variant-east-asian",
    "font-variant-ligatures", "font-variant-numeric", "font-variant-position",
    "font-weight", "grid-cell", "grid-column", "grid-column-align",
    "grid-column-sizing", "grid-column-span", "grid-columns", "grid-flow",
    "grid-row", "grid-row-align", "grid-row-sizing", "grid-row-span",
    "grid-rows", "grid-template", "hanging-punctuation", "height", "hyphens",
    "icon", "image-orientation", "image-rendering", "image-resolution",
    "inline-box-align", "justify-content", "left", "letter-spacing",
    "line-break", "line-height", "line-stacking", "line-stacking-ruby",
    "line-stacking-shift", "line-stacking-strategy", "list-style",
    "list-style-image", "list-style-position", "list-style-type", "margin",
    "margin-bottom", "margin-left", "margin-right", "margin-top",
    "marker-offset", "marks", "marquee-direction", "marquee-loop",
    "marquee-play-count", "marquee-speed", "marquee-style", "max-height",
    "max-width", "min-height", "min-width", "move-to", "nav-down", "nav-index",
    "nav-left", "nav-right", "nav-up", "opacity", "order", "orphans", "outline",
    "outline-color", "outline-offset", "outline-style", "outline-width",
    "overflow", "overflow-style", "overflow-wrap", "overflow-x", "overflow-y",
    "padding", "padding-bottom", "padding-left", "padding-right", "padding-top",
    "page", "page-break-after", "page-break-before", "page-break-inside",
    "page-policy", "pause", "pause-after", "pause-before", "perspective",
    "perspective-origin", "pitch", "pitch-range", "play-during", "position",
    "presentation-level", "punctuation-trim", "quotes", "rendering-intent",
    "resize", "rest", "rest-after", "rest-before", "richness", "right",
    "rotation", "rotation-point", "ruby-align", "ruby-overhang",
    "ruby-position", "ruby-span", "size", "speak", "speak-as", "speak-header",
    "speak-numeral", "speak-punctuation", "speech-rate", "stress", "string-set",
    "tab-size", "table-layout", "target", "target-name", "target-new",
    "target-position", "text-align", "text-align-last", "text-decoration",
    "text-decoration-color", "text-decoration-line", "text-decoration-skip",
    "text-decoration-style", "text-emphasis", "text-emphasis-color",
    "text-emphasis-position", "text-emphasis-style", "text-height",
    "text-indent", "text-justify", "text-outline", "text-shadow",
    "text-space-collapse", "text-transform", "text-underline-position",
    "text-wrap", "top", "transform", "transform-origin", "transform-style",
    "transition", "transition-delay", "transition-duration",
    "transition-property", "transition-timing-function", "unicode-bidi",
    "vertical-align", "visibility", "voice-balance", "voice-duration",
    "voice-family", "voice-pitch", "voice-range", "voice-rate", "voice-stress",
    "voice-volume", "volume", "white-space", "widows", "width", "word-break",
    "word-spacing", "word-wrap", "z-index"
  ]);

  var colorKeywords = keySet([
    "black", "silver", "gray", "white", "maroon", "red", "purple", "fuchsia",
    "green", "lime", "olive", "yellow", "navy", "blue", "teal", "aqua"
  ]);
  
  var valueKeywords = keySet([
    "above", "absolute", "activeborder", "activecaption", "afar",
    "after-white-space", "ahead", "alias", "all", "all-scroll", "alternate",
    "always", "amharic", "amharic-abegede", "antialiased", "appworkspace",
    "arabic-indic", "armenian", "asterisks", "auto", "avoid", "background",
    "backwards", "baseline", "below", "bidi-override", "binary", "bengali",
    "blink", "block", "block-axis", "bold", "bolder", "border", "border-box",
    "both", "bottom", "break-all", "break-word", "button", "button-bevel",
    "buttonface", "buttonhighlight", "buttonshadow", "buttontext", "cambodian",
    "capitalize", "caps-lock-indicator", "caption", "captiontext", "caret",
    "cell", "center", "checkbox", "circle", "cjk-earthly-branch",
    "cjk-heavenly-stem", "cjk-ideographic", "clear", "clip", "close-quote",
    "col-resize", "collapse", "compact", "condensed", "contain", "content",
    "content-box", "context-menu", "continuous", "copy", "cover", "crop",
    "cross", "crosshair", "currentcolor", "cursive", "dashed", "decimal",
    "decimal-leading-zero", "default", "default-button", "destination-atop",
    "destination-in", "destination-out", "destination-over", "devanagari",
    "disc", "discard", "document", "dot-dash", "dot-dot-dash", "dotted",
    "double", "down", "e-resize", "ease", "ease-in", "ease-in-out", "ease-out",
    "element", "ellipsis", "embed", "end", "ethiopic", "ethiopic-abegede",
    "ethiopic-abegede-am-et", "ethiopic-abegede-gez", "ethiopic-abegede-ti-er",
    "ethiopic-abegede-ti-et", "ethiopic-halehame-aa-er",
    "ethiopic-halehame-aa-et", "ethiopic-halehame-am-et",
    "ethiopic-halehame-gez", "ethiopic-halehame-om-et",
    "ethiopic-halehame-sid-et", "ethiopic-halehame-so-et",
    "ethiopic-halehame-ti-er", "ethiopic-halehame-ti-et",
    "ethiopic-halehame-tig", "ew-resize", "expanded", "extra-condensed",
    "extra-expanded", "fantasy", "fast", "fill", "fixed", "flat", "footnotes",
    "forwards", "from", "geometricPrecision", "georgian", "graytext", "groove",
    "gujarati", "gurmukhi", "hand", "hangul", "hangul-consonant", "hebrew",
    "help", "hidden", "hide", "higher", "highlight", "highlighttext",
    "hiragana", "hiragana-iroha", "horizontal", "hsl", "hsla", "icon", "ignore",
    "inactiveborder", "inactivecaption", "inactivecaptiontext", "infinite",
    "infobackground", "infotext", "inherit", "initial", "inline", "inline-axis",
    "inline-block", "inline-table", "inset", "inside", "intrinsic", "invert",
    "italic", "justify", "kannada", "katakana", "katakana-iroha", "khmer",
    "landscape", "lao", "large", "larger", "left", "level", "lighter",
    "line-through", "linear", "lines", "list-item", "listbox", "listitem",
    "local", "logical", "loud", "lower", "lower-alpha", "lower-armenian",
    "lower-greek", "lower-hexadecimal", "lower-latin", "lower-norwegian",
    "lower-roman", "lowercase", "ltr", "malayalam", "match",
    "media-controls-background", "media-current-time-display",
    "media-fullscreen-button", "media-mute-button", "media-play-button",
    "media-return-to-realtime-button", "media-rewind-button",
    "media-seek-back-button", "media-seek-forward-button", "media-slider",
    "media-sliderthumb", "media-time-remaining-display", "media-volume-slider",
    "media-volume-slider-container", "media-volume-sliderthumb", "medium",
    "menu", "menulist", "menulist-button", "menulist-text",
    "menulist-textfield", "menutext", "message-box", "middle", "min-intrinsic",
    "mix", "mongolian", "monospace", "move", "multiple", "myanmar", "n-resize",
    "narrower", "navy", "ne-resize", "nesw-resize", "no-close-quote", "no-drop",
    "no-open-quote", "no-repeat", "none", "normal", "not-allowed", "nowrap",
    "ns-resize", "nw-resize", "nwse-resize", "oblique", "octal", "open-quote",
    "optimizeLegibility", "optimizeSpeed", "oriya", "oromo", "outset",
    "outside", "overlay", "overline", "padding", "padding-box", "painted",
    "paused", "persian", "plus-darker", "plus-lighter", "pointer", "portrait",
    "pre", "pre-line", "pre-wrap", "preserve-3d", "progress", "push-button",
    "radio", "read-only", "read-write", "read-write-plaintext-only", "relative",
    "repeat", "repeat-x", "repeat-y", "reset", "reverse", "rgb", "rgba",
    "ridge", "right", "round", "row-resize", "rtl", "run-in", "running",
    "s-resize", "sans-serif", "scroll", "scrollbar", "se-resize", "searchfield",
    "searchfield-cancel-button", "searchfield-decoration",
    "searchfield-results-button", "searchfield-results-decoration",
    "semi-condensed", "semi-expanded", "separate", "serif", "show", "sidama",
    "single", "skip-white-space", "slide", "slider-horizontal",
    "slider-vertical", "sliderthumb-horizontal", "sliderthumb-vertical", "slow",
    "small", "small-caps", "small-caption", "smaller", "solid", "somali",
    "source-atop", "source-in", "source-out", "source-over", "space", "square",
    "square-button", "start", "static", "status-bar", "stretch", "stroke",
    "sub", "subpixel-antialiased", "super", "sw-resize", "table",
    "table-caption", "table-cell", "table-column", "table-column-group",
    "table-footer-group", "table-header-group", "table-row", "table-row-group",
    "telugu", "text", "text-bottom", "text-top", "textarea", "textfield", "thai",
    "thick", "thin", "threeddarkshadow", "threedface", "threedhighlight",
    "threedlightshadow", "threedshadow", "tibetan", "tigre", "tigrinya-er",
    "tigrinya-er-abegede", "tigrinya-et", "tigrinya-et-abegede", "to", "top",
    "transparent", "ultra-condensed", "ultra-expanded", "underline", "up",
    "upper-alpha", "upper-armenian", "upper-greek", "upper-hexadecimal",
    "upper-latin", "upper-norwegian", "upper-roman", "uppercase", "urdu", "url",
    "vertical", "vertical-text", "visible", "visibleFill", "visiblePainted",
    "visibleStroke", "visual", "w-resize", "wait", "wave", "white", "wider",
    "window", "windowframe", "windowtext", "x-large", "x-small", "xor",
    "xx-large", "xx-small", "yellow"
  ]);

  function keySet(array) { var keys = {}; for (var i = 0; i < array.length; ++i) keys[array[i]] = true; return keys; }
  function ret(style, tp) {type = tp; return style;}

  function tokenBase(stream, state) {
    var ch = stream.next();
    if (ch == "@") {stream.eatWhile(/[\w\\\-]/); return ret("def", stream.current());}
    else if (ch == "/" && stream.eat("*")) {
      state.tokenize = tokenCComment;
      return tokenCComment(stream, state);
    }
    else if (ch == "<" && stream.eat("!")) {
      state.tokenize = tokenSGMLComment;
      return tokenSGMLComment(stream, state);
    }
    else if (ch == "=") ret(null, "compare");
    else if ((ch == "~" || ch == "|") && stream.eat("=")) return ret(null, "compare");
    else if (ch == "\"" || ch == "'") {
      state.tokenize = tokenString(ch);
      return state.tokenize(stream, state);
    }
    else if (ch == "#") {
      stream.eatWhile(/[\w\\\-]/);
      return ret("atom", "hash");
    }
    else if (ch == "!") {
      stream.match(/^\s*\w*/);
      return ret("keyword", "important");
    }
    else if (/\d/.test(ch)) {
      stream.eatWhile(/[\w.%]/);
      return ret("number", "unit");
    }
    else if (ch === "-") {
      if (/\d/.test(stream.peek())) {
        stream.eatWhile(/[\w.%]/);
        return ret("number", "unit");
      } else if (stream.match(/^[^-]+-/)) {
        return ret("meta", type);
      }
    }
    else if (/[,+>*\/]/.test(ch)) {
      return ret(null, "select-op");
    }
    else if (ch == "." && stream.match(/^-?[_a-z][_a-z0-9-]*/i)) {
      return ret("qualifier", type);
    }
    else if (ch == ":") {
      return ret("operator", ch);
    }
    else if (/[;{}\[\]\(\)]/.test(ch)) {
      return ret(null, ch);
    }
    else if (ch == "u" && stream.match("rl(")) {
      stream.backUp(1);
      state.tokenize = tokenParenthesized;
      return ret("property", "variable");
    }
    else {
      stream.eatWhile(/[\w\\\-]/);
      return ret("property", "variable");
    }
  }

  function tokenCComment(stream, state) {
    var maybeEnd = false, ch;
    while ((ch = stream.next()) != null) {
      if (maybeEnd && ch == "/") {
        state.tokenize = tokenBase;
        break;
      }
      maybeEnd = (ch == "*");
    }
    return ret("comment", "comment");
  }

  function tokenSGMLComment(stream, state) {
    var dashes = 0, ch;
    while ((ch = stream.next()) != null) {
      if (dashes >= 2 && ch == ">") {
        state.tokenize = tokenBase;
        break;
      }
      dashes = (ch == "-") ? dashes + 1 : 0;
    }
    return ret("comment", "comment");
  }

  function tokenString(quote, nonInclusive) {
    return function(stream, state) {
      var escaped = false, ch;
      while ((ch = stream.next()) != null) {
        if (ch == quote && !escaped)
          break;
        escaped = !escaped && ch == "\\";
      }
      if (!escaped) {
        if (nonInclusive) stream.backUp(1);
        state.tokenize = tokenBase;
      }
      return ret("string", "string");
    };
  }

  function tokenParenthesized(stream, state) {
    stream.next(); // Must be '('
    if (!stream.match(/\s*[\"\']/, false))
      state.tokenize = tokenString(")", true);
    else
      state.tokenize = tokenBase;
    return ret(null, "(");
  }

  return {
    startState: function(base) {
      return {tokenize: tokenBase,
              baseIndent: base || 0,
              stack: []};
    },

    token: function(stream, state) {
      
      // Use these terms when applicable (see http://www.xanthir.com/blog/b4E50)
      // 
      // rule** or **ruleset:
      // A selector + braces combo, or an at-rule.
      // 
      // declaration block:
      // A sequence of declarations.
      // 
      // declaration:
      // A property + colon + value combo.
      // 
      // property value:
      // The entire value of a property.
      // 
      // component value:
      // A single piece of a property value. Like the 5px in
      // text-shadow: 0 0 5px blue;. Can also refer to things that are
      // multiple terms, like the 1-4 terms that make up the background-size
      // portion of the background shorthand.
      // 
      // term:
      // The basic unit of author-facing CSS, like a single number (5),
      // dimension (5px), string ("foo"), or function. Officially defined
      //  by the CSS 2.1 grammar (look for the 'term' production)
      // 
      // 
      // simple selector:
      // A single atomic selector, like a type selector, an attr selector, a
      // class selector, etc.
      // 
      // compound selector:
      // One or more simple selectors without a combinator. div.example is
      // compound, div > .example is not.
      // 
      // complex selector:
      // One or more compound selectors chained with combinators.
      // 
      // combinator:
      // The parts of selectors that express relationships. There are four
      // currently - the space (descendant combinator), the greater-than
      // bracket (child combinator), the plus sign (next sibling combinator),
      // and the tilda (following sibling combinator).
      // 
      // sequence of selectors:
      // One or more of the named type of selector chained with commas.

      if (state.tokenize == tokenBase && stream.eatSpace()) return null;
      var style = state.tokenize(stream, state);

      // Changing style returned based on context
      var context = state.stack[state.stack.length-1];
      if (style == "property") {
        if (context == "propertyValue"){
          if (valueKeywords[stream.current()]) {
            style = "string-2";
          } else if (colorKeywords[stream.current()]) {
            style = "keyword";
          } else {
            style = "variable-2";
          }
        } else if (context == "rule") {
          if (!propertyKeywords[stream.current()]) {
            style += " error";
          }
        } else if (!context || context == "@media{") {
          style = "tag";
        } else if (context == "@media") {
          if (atMediaTypes[stream.current()]) {
            style = "attribute"; // Known attribute
          } else if (/^(only|not)$/i.test(stream.current())) {
            style = "keyword";
          } else if (stream.current().toLowerCase() == "and") {
            style = "error"; // "and" is only allowed in @mediaType
          } else if (atMediaFeatures[stream.current()]) {
            style = "error"; // Known property, should be in @mediaType(
          } else {
            // Unknown, expecting keyword or attribute, assuming attribute
            style = "attribute error";
          }
        } else if (context == "@mediaType") {
          if (atMediaTypes[stream.current()]) {
            style = "attribute";
          } else if (stream.current().toLowerCase() == "and") {
            style = "operator";
          } else if (/^(only|not)$/i.test(stream.current())) {
            style = "error"; // Only allowed in @media
          } else if (atMediaFeatures[stream.current()]) {
            style = "error"; // Known property, should be in parentheses
          } else {
            // Unknown attribute or property, but expecting property (preceded
            // by "and"). Should be in parentheses
            style = "error";
          }
        } else if (context == "@mediaType(") {
          if (propertyKeywords[stream.current()]) {
            // do nothing, remains "property"
          } else if (atMediaTypes[stream.current()]) {
            style = "error"; // Known property, should be in parentheses
          } else if (stream.current().toLowerCase() == "and") {
            style = "operator";
          } else if (/^(only|not)$/i.test(stream.current())) {
            style = "error"; // Only allowed in @media
          } else {
            style += " error";
          }
        } else {
          style = "error";
        }
      } else if (style == "atom") {
        if(!context || context == "@media{") {
          style = "builtin";
        } else if (context == "propertyValue") {
          if (!/^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/.test(stream.current())) {
            style += " error";
          }
        } else {
          style = "error";
        }
      } else if (context == "@media" && type == "{") {
        style = "error";
      }

      // Push/pop context stack
      if (type == "{") {
        if (context == "@media" || context == "@mediaType") {
          state.stack.pop();
          state.stack[state.stack.length-1] = "@media{";
        }
        else state.stack.push("rule");
      }
      else if (type == "}") {
        state.stack.pop();
        if (context == "propertyValue") state.stack.pop();
      }
      else if (type == "@media") state.stack.push("@media");
      else if (context == "@media" && /\b(keyword|attribute)\b/.test(style))
        state.stack.push("@mediaType");
      else if (context == "@mediaType" && stream.current() == ",") state.stack.pop();
      else if (context == "@mediaType" && type == "(") state.stack.push("@mediaType(");
      else if (context == "@mediaType(" && type == ")") state.stack.pop();
      else if (context == "rule" && type == ":") state.stack.push("propertyValue");
      else if (context == "propertyValue" && type == ";") state.stack.pop();
      return style;
    },

    indent: function(state, textAfter) {
      var n = state.stack.length;
      if (/^\}/.test(textAfter))
        n -= state.stack[state.stack.length-1] == "propertyValue" ? 2 : 1;
      return state.baseIndent + n * indentUnit;
    },

    electricChars: "}"
  };
});

CodeMirror.defineMIME("text/css", "css");

CodeMirror.defineMode("htmlmixed", function(config) {
  var htmlMode = CodeMirror.getMode(config, {name: "xml", htmlMode: true});
  var jsMode = CodeMirror.getMode(config, "javascript");
  var cssMode = CodeMirror.getMode(config, "css");

  function html(stream, state) {
    var style = htmlMode.token(stream, state.htmlState);
    if (/(?:^|\s)tag(?:\s|$)/.test(style) && stream.current() == ">" && state.htmlState.context) {
      if (/^script$/i.test(state.htmlState.context.tagName)) {
        state.token = javascript;
        state.localState = jsMode.startState(htmlMode.indent(state.htmlState, ""));
      }
      else if (/^style$/i.test(state.htmlState.context.tagName)) {
        state.token = css;
        state.localState = cssMode.startState(htmlMode.indent(state.htmlState, ""));
      }
    }
    return style;
  }
  function maybeBackup(stream, pat, style) {
    var cur = stream.current();
    var close = cur.search(pat), m;
    if (close > -1) stream.backUp(cur.length - close);
    else if (m = cur.match(/<\/?$/)) {
      stream.backUp(cur.length);
      if (!stream.match(pat, false)) stream.match(cur[0]);
    }
    return style;
  }
  function javascript(stream, state) {
    if (stream.match(/^<\/\s*script\s*>/i, false)) {
      state.token = html;
      state.localState = null;
      return html(stream, state);
    }
    return maybeBackup(stream, /<\/\s*script\s*>/,
                       jsMode.token(stream, state.localState));
  }
  function css(stream, state) {
    if (stream.match(/^<\/\s*style\s*>/i, false)) {
      state.token = html;
      state.localState = null;
      return html(stream, state);
    }
    return maybeBackup(stream, /<\/\s*style\s*>/,
                       cssMode.token(stream, state.localState));
  }

  return {
    startState: function() {
      var state = htmlMode.startState();
      return {token: html, localState: null, mode: "html", htmlState: state};
    },

    copyState: function(state) {
      if (state.localState)
        var local = CodeMirror.copyState(state.token == css ? cssMode : jsMode, state.localState);
      return {token: state.token, localState: local, mode: state.mode,
              htmlState: CodeMirror.copyState(htmlMode, state.htmlState)};
    },

    token: function(stream, state) {
      return state.token(stream, state);
    },

    indent: function(state, textAfter) {
      if (state.token == html || /^\s*<\//.test(textAfter))
        return htmlMode.indent(state.htmlState, textAfter);
      else if (state.token == javascript)
        return jsMode.indent(state.localState, textAfter);
      else
        return cssMode.indent(state.localState, textAfter);
    },

    electricChars: "/{}:",

    innerMode: function(state) {
      var mode = state.token == html ? htmlMode : state.token == javascript ? jsMode : cssMode;
      return {state: state.localState || state.htmlState, mode: mode};
    }
  };
}, "xml", "javascript", "css");

CodeMirror.defineMIME("text/html", "htmlmixed");

/*
 *
 * This manages the 3d viewer applet. 
 * This is done centrally because we only want one instance of 
 * the actual applet to ever get run. 
 * 
 * AUTHOR: 
 *     -- Robert Bradshaw
 * 
 */


var applet_tag = 'Loading Java 3d Libraries... <applet id="sage3d" code="org.jdesktop.applet.util.JNLPAppletLauncher"  \
width="500" height="22"                                                       \
codebase="/java/sage3d"                                                       \
archive="lib/sage3d.jar,                                                      \
         sun-libs/applet-launcher.jar,sun-libs/j3dcore.jar,sun-libs/j3dutils.jar,sun-libs/vecmath.jar,sun-libs/jogl.jar,sun-libs/gluegen-rt.jar">      \
<param name="codebase_lookup" value="false">                                  \
<param name="subapplet.displayname" value="Java 3D Viewer">                   \
<param name="jnlpNumExtensions" value="1">                                    \
<param name="progressbar" value="true">                                       \
<param name="noddraw.check" value="true">                                     \
                                                                              \
<param name="subapplet.classname" value="org.sagemath.sage3d.ObjectViewerApplet">       \
<param name="jnlpExtension1" value="'+document.location.protocol+'//'+document.location.host+'/java/sun-libs/java3d-latest.jnlp">              \
</applet>'

var test_applet_tag = '<applet id="sage3d" code="org.sagemath.TestApplet"     \
width="100" height="20"                                                       \
codebase="/java/sage3d"                                                       \
archive="lib/sage3d.jar"                                                      \
</applet>xxxx'


var sage3d_div;
var launcher_applet;
var sage3d_applet;

function sage3d_init() {
  sage3d_div = document.createElement("div");
  sage3d_div.innerHTML = applet_tag;
  //document.body.appendChild(div);
  document.body.insertBefore(sage3d_div, document.body.firstChild)
  launcher_applet = document.getElementById("sage3d");
}

function sage3d_show(url, cell, name) {
  //if (!confirm("sage3d_show")) return;
  if (launcher_applet == undefined) {
    sage3d_init();
  }
  sage3d_applet = launcher_applet.getSubApplet();
  if (sage3d_applet == undefined) {
    setTimeout(function() {sage3d_show(url, cell, name); }, 500);
  }
  else if (sage3d_div.style.visibility != "hidden") {
    sage3d_div.style.visibility = "hidden";
    sage3d_div.style.position = "absolute";
    setTimeout(function() {sage3d_show(url, cell, name); }, 500);
  }
  else {
    sage3d_applet.showView(url, cell, name);
  }
}


sagenb.jmol = {};

sagenb.jmol.jmol_inline = function(container) {
	var _this = this;

	if(!container.hasClass("jmol_instance")) {
		throw "Not a Jmol container";
		return;
	}

	_this.container = container;
	_this.url = _this.container.data("url");
	_this.static_img_url = _this.container.data("img");
	_this.dimensions = [500, 500];
	_this.popup_win = null;

	_this.state_script = null;

	_this.init = function() {
		// get random id
		_this.suffix = Math.floor(Math.random() * 1000000);
		while($("#jmol_instance" + _this.suffix).length > 0) {
			_this.suffix = Math.floor(Math.random() * 1000000);
		}
		_this.container.attr("id", "jmol_instance" + _this.suffix);

		_this.sleep();
	};

	_this.is_alive = function() {
		return _this.container.hasClass("alive");
	};
	_this.appletify = function() {
		_this.container.children().detach();
		_this.update_state_script();

		jmolSetDocument(false);
		var sleep_btn = $("<button />")
			.text(gettext("Show Static Image"))
			.addClass("btn")
			.click(_this.sleep);
		var popup_btn = $("<button />")
			.text(gettext("Popout"))
			.addClass("btn")
			.click(_this.popup);
		_this.container.append(sleep_btn,
							   popup_btn,
							   $("<br>"),
							   jmolApplet(_this.dimensions, _this.state_script, _this.suffix));

		_this.container.addClass("alive");
	};
	_this.sleep = function() {
		if (_this.is_alive()) {
			_this.update_state_script();
			// _this.static_img_url = "data:image/jpeg;base64, " + jmolGetPropertyAsString("image", "", _this.suffix);
		}
		_this.container.children().detach();
		_this.container.removeClass("alive");
		var appletify_btn = $("<button />")
			.text(gettext("Open Interactive View"))
			.addClass("btn")
			.click(_this.appletify);
		var popup_btn = $("<button />")
			.text(gettext("Popout"))
			.addClass("btn")
			.click(_this.popup);
		var static_img = $("<img />").attr("src", _this.static_img_url);
		_this.container.append(appletify_btn, popup_btn, $("<br>"), static_img);
	};
	_this.resize = function(dimensions) {
		_this.dimensions = dimensions;
		jmolResizeApplet(dimensions, _this.suffix);
	};
	_this.update_state_script = function() {
		var default_dir = _this.url.split("?")[0];

		/*if(_this.is_alive()) {
			var stateStr = "#a comment to guarrantee one line\n";
			stateStr += jmolGetPropertyAsString("stateInfo", "", _this.suffix);
			var re_modelinline = /data "model list"(.|\n|\r)*end "model list"/;
			if(stateStr.match(re_modelinline)) {
				//If we didn't get a good response we'll ignore and get later
				var modelStr = (stateStr.match(re_modelinline))[0];
				modelStr = modelStr.replace(/\r\n/g, '|').replace(/\r/g, '|').replace(/\n/g, '|').replace(/\|\|/g, '|');
				modelStr = 'fix between here ' + modelStr + ' and here';
				stateStr = stateStr.replace(re_modelinline, modelStr);
			}

			_this.state_script = 'set defaultdirectory="' + default_dir + '";\n' + stateStr;
		}*/

		if(!_this.state_script) {
			_this.state_script = 'set defaultdirectory "' + default_dir + '";' + 
								 'script "' + _this.url + '";' + 
								 'isosurface fullylit;' + 
								 'pmesh o* fullylit;' + 
								 'set antialiasdisplay on;' + 
								 'set repaintWaitMs 1500;' + 
								 'x=defaultdirectory;' + 
								 'data "directory @x";' + 
								 'set MessageCallback "jmolMessageHandler";' + 
								 'show defaultdirectory;';
		}
	};
	_this.popup = function() {
		_this.sleep();
		_this.update_state_script();
		_this.popup_win = window.open("jmol_popup.html", "jmol_viewer" + _this.suffix, "width=600,height=600,resizable=1,statusbar=0");
		_this.popup_win.onload = function() {
			_this.popup_win.the_popup = new sagenb.jmol.jmol_popup(_this.popup_win);
			_this.popup_win.the_popup.state_script = _this.state_script;
			_this.popup_win.the_popup.url = _this.url;
			_this.popup_win.the_popup.suffix = _this.suffix + "popup";
			_this.popup_win.the_popup.init();
			_this.popup_win.focus();
		};
	};
}

sagenb.jmol.jmol_popup = function(win) {
	var _this = this;

	_this.win = win;
	_this.container = $(".jmol_instance", _this.win.document);
	
	_this.init = function() {
		jmolSetDocument(false);
		_this.container.html(jmolApplet("100%", _this.state_script, _this.suffix));

		function on_resize() {
			_this.container.height(_this.container.width());
		}
		$(_this.win).resize(on_resize);
		on_resize();
	};

	_this.spin = function(s) {
		if(s) {
			jmolScriptWait("spin on", "");
		}
		else {
			jmolScriptWait("spin off", "");
		}
	};
	_this.antialias = function(s) {
		if(s) {
			jmolScriptWait("set antialiasdisplay on", "");
		}
		else {
			jmolScriptWait("set antialiasdisplay off", "");
		}
	};
}
/*
 * Javascript functionality for the worksheet page
 * 
 * AUTHOR - Samuel Ainsworth (samuel_ainsworth@brown.edu)
 */

// simulated namespace
sagenb.worksheetapp = {};

sagenb.worksheetapp.worksheet = function() {
	/* this allows us to access this cell object from 
	 * inner functions
	 */
	var _this = this;
	
	/* Array of all of the cells. This is a sparse array because 
	 * cells get deleted etc. Because it is sparse, you have to 
	 * use a conditional when you loop over each element. See
	 * hide_all_output, show_all_output, etc.
	 */
	_this.cells = {};
	
	// Worksheet information from worksheet.py
	_this.state_number = -1;
	
	// Current worksheet info, set in notebook.py.
	_this.filename = "";
	_this.name = "";
	_this.owner = "";
	_this.id = -1;
	_this.is_published = false;
	_this.system = "";
	_this.pretty_print = false;
	
	// sharing
	_this.collaborators = [];
	_this.auto_publish = false;
	_this.published_id_number = -1;
	_this.published_url = null;
	_this.published_time = null;

	// data
	_this.attached_data_files = [];
	
	// Ping the server periodically for worksheet updates.
	_this.server_ping_time = 10000;
	
	// Focus / blur.
	_this.current_cell_id = -1;
	
	// Evaluate all
	_this.is_evaluating_all = false;
	
	
	// other variables go here
	
	///////////// COMMANDS ////////////
	_this.worksheet_command = function(cmd) {
		/*
		Create a string formatted as a URL to send back to the server and
		execute the given cmd on the current worksheet.

		INPUT:
			cmd -- string
		OUTPUT:
			a string
		*/
		if (cmd === 'eval' 
		|| cmd === 'new_cell_before' 
		|| cmd === 'new_cell_after'
		|| cmd === 'new_text_cell_before'
		|| cmd === 'new_text_cell_after') {
			_this.state_number = parseInt(_this.state_number, 10) + 1;
		}
		// worksheet_filename differs from actual url for public interacts
		// users see /home/pub but worksheet_filename is /home/_sage_
		return ('/home/' + _this.filename + '/' + cmd);
	};
	
	//// MISC ////
	_this.forEachCell = function(f) {
		/* Execute the given function on all cells in 
		 * this worksheet. This is useful since some values 
		 * in _this.cells are null.
		 */
		$.each(_this.cells, function(i, cell) {
			if(cell) f(cell);
		});
	}
	
	///////////////// PINGS //////////////////
	_this.ping_server = function() {
		/* for some reason pinging doesn't work well.
		 * the callback goes but jQuery throws a 404 error.
		 * this error may not be a bug, not sure...
		 */
		sagenb.async_request(_this.worksheet_command('alive'), sagenb.generic_callback(function(status, response) {
			/*  Each time the server is up and responds, the server includes
				the worksheet state_number is the response.  If this number is out
				of sync with our view of the worksheet state, then we force a
				refresh of the list of cells.  This is very useful in case the
				user uses the back button and the browser cache displays an
				invalid worksheet list (which can cause massive confusion), or the
				user open the same worksheet in multiple browsers, or multiple
				users open the same shared worksheet.
			*/
			if (_this.state_number >= 0 && parseInt(response, 10) > _this.state_number) {
				// Force a refresh of just the cells in the body.
				_this.worksheet_update();
				_this.cell_list_update();
			}
		}));
	};
	
	function close_window() {
		// this is a hack which gets close working
		window.open('', '_self', '');
		close();
		window.close();
		self.close();
	}
	
	//////////// FILE MENU TYPE STUFF //////////
	_this.new_worksheet = function() {
		if(_this.published_mode) return;
		window.open("/new_worksheet");
	};
	_this.save = function() {
		if(_this.published_mode) return;
		sagenb.async_request(_this.worksheet_command("save_snapshot"), sagenb.generic_callback());
	};
	_this.close = function() {
		if(_this.name === gettext("Untitled") && !_this.published_mode) {
			$(".alert_rename").show();
		} else {
			// maybe other stuff here??
			close_window();
		}
	};
	_this.print = function() {
		/* here we may want to convert MathJax expressions into
		 * something more readily printable eg images. I think 
		 * there may be some issues with printing using whatever 
		 * we have as default. I haven't seen this issue yet
		 * but it may exist.
		 */
		window.print();
	};
	
	//////// EXPORT/IMPORT ///////
	_this.export_worksheet = function() {
		window.open(_this.worksheet_command("download/" + _this.name + ".sws"));
	};
	_this.import_worksheet = function() {
	
	};
	
	////////// INSERT CELL //////////////
	_this.add_new_cell_button_after = function(obj) {
		/* Add a new cell button after the given
		 * DOM/jQuery object
		 */
		var button = $("<div class=\"new_cell_button\">" + 
							"<div class=\"line\"></div>" + 
						"</div>");
		
		button.insertAfter(obj);
		button.click(function(event) {
			// get the cell above this button in the dom
			// here 'this' references the button that was clicked
			if($(this).prev(".cell_wrapper").find(".cell").length > 0) {
				// this is not the first button
				var after_cell_id = toint($(this).prev(".cell_wrapper").find(".cell").attr("id").substring(5));
				
				if(event.shiftKey) {
					_this.new_text_cell_after(after_cell_id);
				} else {
					_this.new_cell_after(after_cell_id);
				}
			}
			else {
				// this is the first button
				var before_cell_id = toint($(this).next(".cell_wrapper").find(".cell").attr("id").substring(5));
				
				if(event.shiftKey) {
					_this.new_text_cell_before(before_cell_id);
				} else {
					_this.new_cell_before(before_cell_id);
				}
			}
		});
	};
	
	////////////// EVALUATION ///////////////
	_this.evaluate_all = function() {
		if(_this.published_mode) return;
		_this.is_evaluating_all = true;
		
		_this.forEachCell(function(cell) {
			cell.set_output_loading();
		});
		
		var firstcell_id = parseInt($(".cell").attr("id").substring(5));
		_this.cells[firstcell_id].evaluate();
	};
	_this.interrupt = function() {
		if(_this.published_mode) return;
		sagenb.async_request(_this.worksheet_command('interrupt'), sagenb.generic_callback());
	};
	_this.interrupt_with_confirm = function() {
		if(confirm(gettext("Are you sure you would like to interrupt the running computation?"))) {
			_this.interrupt();
		}
	};
	_this.restart_sage = function() {
		if(_this.published_mode) return;
		_this.forEachCell(function(cell) {
			if(cell.is_evaluating) cell.render_output("");
		});
		sagenb.async_request(_this.worksheet_command('restart_sage'), sagenb.generic_callback());
	};
	
	//// OUTPUT STUFF ////
	_this.hide_all_output = function() {
		if(_this.published_mode) return;
		sagenb.async_request(_this.worksheet_command('hide_all'), sagenb.generic_callback(function(status, response) {
			_this.forEachCell(function(cell) {
				cell.set_output_hidden();
			});
		}));
	};
	_this.show_all_output = function() {
		if(_this.published_mode) return;
		sagenb.async_request(_this.worksheet_command('show_all'), sagenb.generic_callback(function(status, response) {
			_this.forEachCell(function(cell) {
				cell.set_output_visible();
			});
		}));
	};
	_this.delete_all_output = function() {
		if(_this.published_mode) return;
		sagenb.async_request(_this.worksheet_command('delete_all_output'), sagenb.generic_callback(function(status, response) {
			_this.forEachCell(function(cell) {
				cell.output = "";
				cell.render_output();
			});
		}));
	};
	
	_this.change_system = function(newsystem) {
		if(_this.published_mode) return;
		sagenb.async_request(_this.worksheet_command("system/" + newsystem), sagenb.generic_callback(function(status, response) {
			_this.system = newsystem;
			
			_this.forEachCell(function(cell) {
				cell.update_codemirror_mode();
			});
		}));
	};
	_this.set_pretty_print = function(s) {
		if(_this.published_mode) return;
		sagenb.async_request(_this.worksheet_command("pretty_print/" + s), sagenb.generic_callback());
	};
	
	//// NEW CELL /////
	_this.new_cell_before = function(id) {
		if(_this.published_mode) return;
		sagenb.async_request(_this.worksheet_command("new_cell_before"), function(status, response) {
			if(response === "locked") {
				$(".alert_locked").show();
				return;
			}
			
			var X = decode_response(response);
			var new_cell = new sagenb.worksheetapp.cell(X.new_id);
			var a = $("#cell_" + X.id).parent().prev();
			var wrapper = $("<div></div>").addClass("cell_wrapper").insertAfter(a);
			new_cell.worksheet = _this;
			new_cell.update(wrapper);
			
			// add the next new cell button
			_this.add_new_cell_button_after(wrapper);
			
			// wait for the render to finish
			setTimeout(new_cell.focus, 50);
			
			_this.cells[new_cell.id] = new_cell;
		},
		{
			id: id
		});
	};
	_this.new_cell_after = function(id) {
		if(_this.published_mode) return;
		sagenb.async_request(_this.worksheet_command("new_cell_after"), function(status, response) {
			if(response === "locked") {
				$(".alert_locked").show();
				return;
			}
			
			var X = decode_response(response);
			var new_cell = new sagenb.worksheetapp.cell(X.new_id);
			var a = $("#cell_" + X.id).parent().next();
			var wrapper = $("<div></div>").addClass("cell_wrapper").insertAfter(a);
			new_cell.worksheet = _this;
			new_cell.update(wrapper);
			
			// add the next new cell button
			_this.add_new_cell_button_after(wrapper);
			
			// wait for the render to finish
			setTimeout(new_cell.focus, 50);
			
			_this.cells[new_cell.id] = new_cell;
		},
		{
			id: id
		});
	};
	
	_this.new_text_cell_before = function(id) {
		if(_this.published_mode) return;
		sagenb.async_request(_this.worksheet_command("new_text_cell_before"), function(status, response) {
			if(response === "locked") {
				$(".alert_locked").show();
				return;
			}
			
			var X = decode_response(response);
			var new_cell = new sagenb.worksheetapp.cell(X.new_id);
			var a = $("#cell_" + X.id).parent().prev();
			var wrapper = $("<div></div>").addClass("cell_wrapper").insertAfter(a);
			new_cell.worksheet = _this;
			new_cell.update(wrapper);
			
			// add the next new cell button
			_this.add_new_cell_button_after(wrapper);
			
			// wait for the render to finish
			setTimeout(new_cell.focus, 50);
			
			_this.cells[new_cell.id] = new_cell;
		},
		{
			id: id
		});
	};
	_this.new_text_cell_after = function(id) {
		if(_this.published_mode) return;
		sagenb.async_request(_this.worksheet_command("new_text_cell_after"), function(status, response) {
			if(response === "locked") {
				$(".alert_locked").show();
				return;
			}
			
			var X = decode_response(response);
			var new_cell = new sagenb.worksheetapp.cell(X.new_id);
			var a = $("#cell_" + X.id).parent().next();
			var wrapper = $("<div></div>").addClass("cell_wrapper").insertAfter(a);
			new_cell.worksheet = _this;
			new_cell.update(wrapper);
			
			// add the next new cell button
			_this.add_new_cell_button_after(wrapper);
			
			// wait for the render to finish
			setTimeout(new_cell.focus, 50);
			
			_this.cells[new_cell.id] = new_cell;
		},
		{
			id: id
		});
	};
	
	
	/////////////// WORKSHEET UPDATE //////////////////////
	_this.worksheet_update = function() {
		sagenb.async_request(_this.worksheet_command("worksheet_properties"), sagenb.generic_callback(function(status, response) {
			var X = decode_response(response);
			
			_this.id = X.id_number;
			_this.name = X.name;
			_this.owner = X.owner;
			_this.system = X.system;
			_this.pretty_print = X.pretty_print;
			_this.collaborators = X.collaborators;
			
			if(X.published) {
				_this.published_url = X.published_url;
				_this.published_time = X.published_time;
				_this.auto_publish = X.auto_publish;
				_this.published_id_number = X.published_id_number;
			}
			else {
				_this.published_url = null;
				_this.published_time = null;
				_this.auto_publish = null;
				_this.published_id_number = null;
			}

			_this.running = X.running;
			
			_this.attached_data_files = X.attached_data_files;

			// update the title
			document.title = _this.name + " - Sage";
			$(".worksheet_name h1").text(_this.name);
			
			// update the typesetting checkbox
			$("#typesetting_checkbox").prop("checked", _this.pretty_print);
			
			// set the system select
			$("#system_select").val(_this.system);
			
			// sharing
			if(_this.published_id_number !== null && _this.published_id_number >= 0) {
				$("#publish_checkbox").prop("checked", true);
				$("#auto_republish_checkbox").removeAttr("disabled");
				
				$("#auto_republish_checkbox").prop("checked", _this.auto_publish);
				
				$("#worksheet_url a").text(_this.published_url);
				$("#worksheet_url").show();
			} else {
				$("#publish_checkbox").prop("checked", false);
				$("#auto_republish_checkbox").prop("checked", false);
				$("#auto_republish_checkbox").attr("disabled", true);
				
				$("#worksheet_url").hide();
			}

			$("#collaborators").val(_this.collaborators.join(", "));
			
			// data
			$("#data_list ul *").detach();
			for(var i in _this.attached_data_files) {
				var datafile = _this.attached_data_files[i];

				$("#data_list ul").append('<li>' + 
												'<a href="#" class="filename">' + datafile + '</a>' + 
												'<div class="btn-group">' + 
													'<a href="#" class="btn btn-mini copy_path_btn" rel="tooltip" title="Get Path"><i class="icon-edit"></i></a>' + 
													'<a href="edit_datafile/' + datafile + '" class="btn btn-mini download_btn" rel="tooltip" title="Edit"><i class="icon-pencil"></i></a>' + 
													'<a href="data/' + datafile + '" class="btn btn-mini download_btn" rel="tooltip" title="Download" target="_blank"><i class="icon-download"></i></a>' + 
													'<a href="#" class="btn btn-mini delete_btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a>' + 
												'</div>' + 
											'</li>');

				var elem = $("#data_list li").last();

				// cannot access datastore variable in these functions because it will change by the time they are called
				elem.find(".copy_path_btn").click(function(e) {
					window.prompt(gettext("Copy to clipboard: ") + sagenb.ctrlkey + "-C, Enter", "DATA+'" + $(this).parent().prev().text() + "'");
				});
				elem.find(".delete_btn").click(function(e) {
					var fn = $(this).parent().prev().text();
					$(this).parent().parent().fadeOut("slow", function() {
						sagenb.async_request(_this.worksheet_command("delete_datafile"), sagenb.generic_callback(function(status, response) {
							_this.worksheet_update();
						}),
						{
							name: fn
						});
					});
				});
			}
			$("#data_list ul .btn-group a").tooltip();

			if($("#data_list ul li").length === 0) {
				$("#data_list ul").append('<li class="no_data_files"><a href="#" class="filename">No data files</a></li>');
			}
		}));
	};
	_this.cell_list_update = function() {
		// load in cells
		sagenb.async_request(_this.worksheet_command("cell_list"), sagenb.generic_callback(function(status, response) {
			var X = decode_response(response);
			
			// set the state_number
			_this.state_number = X.state_number;
			
			// remove all previous cells
			$(".cell_wrapper").detach();
			$(".new_cell_button").detach();
			
			// add the first new cell button
			_this.add_new_cell_button_after($(".the_page .worksheet_name"));

			// load in cells
			for(i in X.cell_list) {
				// create wrapper
				var wrapper = $("<div></div>").addClass("cell_wrapper").appendTo(".the_page");
				
				var cell_obj = X.cell_list[i];
				
				// create the new cell
				var newcell = new sagenb.worksheetapp.cell(toint(cell_obj.id));
				
				// connect it to this worksheet
				newcell.worksheet = _this;
				
				// update all of the cell properties and render it into wrapper
				newcell.update(wrapper, true);
				
				// add the next new cell button
				_this.add_new_cell_button_after(wrapper);
				
				// put the cell in the array
				_this.cells[cell_obj.id] = newcell;
			}
		}));
	};
	
	
	
	_this.on_load_done = function() {
		/* This is the stuff that gets done
		 * after the entire worksheet and all 
		 * of the cells are loaded into the 
		 * DOM.
		 */
		
		// check for # in url commands
		if(window.location.hash) {
			// there is some #hashanchor at the end of the url
			// #hashtext -> hashtext
			var hash = window.location.hash.substring(1);
			
			// do stuff
			// something like #single_cell#cell8
			var splithash = hash.split("#");
			
			$.each(splithash, function(i, e) {
				if(e.substring(0, 5) === "cell_") {
					$('html, body').animate({
						// -40 for navbar and -20 extra
						scrollTop: $("#" + e).offset().top - 60
					}, "slow");
					
					$("#" + e).addClass("current_cell");
					
					// break each loop
					return false;
				}
			});
			
			if($.inArray("single_cell", splithash) >= 0) {
				// #single_cell is in hash
				$("#single_cell_mode_radio").click();
			}
		}
		
		sagenb.done_loading();
	};
	
	
	//////////////// INITIALIZATION ////////////////////
	_this.init = function() {
		// show the spinner
		sagenb.start_loading();
		
		// do the actual load
		_this.worksheet_update();
		
		_this.cell_list_update();
		
		// published mode
		if(_this.published_mode) {
			$("body").addClass("published_mode");
		}

		/////////// setup up the title stuff ////////////
		if(!_this.published_mode) {
			$(".worksheet_name").click(function(e) {
				if(!$(".worksheet_name").hasClass("edit")) {
					$(".worksheet_name input").val(_this.name);
					$(".worksheet_name").addClass("edit");
					$(".worksheet_name input").focus();
				}
			});
		}
		
		// this is the event handler for the input
		var worksheet_name_input_handler = function(e) {
			$(".worksheet_name").removeClass("edit");
			
			if(_this.name !== $(".worksheet_name input").val()) {
				// send to the server
				sagenb.async_request(_this.worksheet_command("rename"), sagenb.generic_callback(function(status, response) {
					// update the title when we get good response
					_this.worksheet_update();
				}), {
					name: $(".worksheet_name input").val()
				});
			}
		};
		
		$(".worksheet_name input").blur(worksheet_name_input_handler).keypress(function(e) {
			if(e.which === 13) {
				// they hit enter
				worksheet_name_input_handler(e);
			}
		});
		
		////////// TYPESETTING CHECKBOX //////////
		$("#typesetting_checkbox").change(function(e) {
			_this.set_pretty_print($("#typesetting_checkbox").prop("checked"));
			
			// update
			_this.worksheet_update();
		});
		
		////////// SINGLE/MULTI CELL ///////////
		function update_single_cell_controls() {
			var current_index = $(".cell").index($(".current_cell")) + 1;
			var num_of_cells = $(".cell").length;
			
			window.location.hash = "#single_cell#" + $(".current_cell").attr("id");
			
			$(".progress_text").text(current_index + "/" + num_of_cells);
			$(".progress .bar").css("width", current_index / num_of_cells * 100 + "%");
			
			if(current_index / num_of_cells < 0.55) {
				$(".progress_text").css("color", "#222");
			}
			else {
				$(".progress_text").css("color", "#eee");
			}
			
			if(current_index === 1) {
				$("#first_cell, #previous_cell").attr("disabled", "disabled");
			}
			else {
				$("#first_cell, #previous_cell").removeAttr("disabled");
			}
			
			if(current_index === num_of_cells) {
				$("#last_cell, #next_cell").attr("disabled", "disabled");
			}
			else {
				$("#last_cell, #next_cell").removeAttr("disabled");
			}
		}
		
		$("#first_cell").click(function(e) {
			$(".cell").removeClass("current_cell");
			$(".cell").first().addClass("current_cell");
			update_single_cell_controls();
		});
		$("#last_cell").click(function(e) {
			$(".cell").removeClass("current_cell");
			$(".cell").last().addClass("current_cell");
			update_single_cell_controls();
		});
		
		$("#previous_cell").click(function(e) {
			$(".current_cell").removeClass("current_cell").parent().prev().prev().find(".cell").addClass("current_cell");
			update_single_cell_controls();
		});
		$("#next_cell").click(function(e) {
			$(".current_cell").removeClass("current_cell").parent().next().next().find(".cell").addClass("current_cell");
			update_single_cell_controls();
		});
		
		$("[name=cell_mode_radio]").change(function(e) {
			if($("[name=cell_mode_radio]:checked").val() === "single") {
				// single cell mode
				var $current_cell = $(".current_cell");
				if($current_cell.length === 0) {
					$current_cell = $(".cell").first().addClass("current_cell");
				}
				$("body").addClass("single_cell_mode");
				update_single_cell_controls();
			}
			else {
				// multi cell mode
				window.location.hash = "";
				$("body").removeClass("single_cell_mode");
			}
			
			// fix codemirror bug
			_this.forEachCell(function(cell) {
				if(cell.codemirror) {
					cell.codemirror.refresh();
				}
			});
		});
		
		////////// LINE NUMBERS CHECKBOX //////////
		$("#line_numbers_checkbox").change(function(e) {
			_this.forEachCell(function(cell) {
				if(cell.is_evaluate_cell) {
					cell.codemirror.setOption("lineNumbers", $("#line_numbers_checkbox").prop("checked"));
				}
			});
		});
		
		/////// RENAME ALERT //////
		$(".alert_rename .rename").click(function(e) {
			$(".worksheet_name").click();
			$(".alert_rename").hide();
		});
		$(".alert_rename .cancel").click(close_window);
		
		///////// LOCKED ALERT //////////
		$(".alert_locked button").click(function(e) {
			$(".alert_locked").hide();
		});
		
		/////// CHANGE SYSTEM DIALOG //////////
		$("#system_modal .btn-primary").click(function(e) {
			_this.change_system($("#system_select").val());
		});
		
		
		//////// SHARING DIALOG ///////////
		$("#sharing_modal .btn-primary").click(function(e) {
			sagenb.async_request(_this.worksheet_command("invite_collab"), sagenb.generic_callback(), {
				collaborators: $("#collaborators").val()
			});
		});
		$("#publish_checkbox").change(function(e) {
			var command;
			if($("#publish_checkbox").prop("checked")) {
				command = _this.worksheet_command("publish?publish_on");
			} else {
				command = _this.worksheet_command("publish?publish_off");
			}
			
			sagenb.async_request(command, sagenb.generic_callback(function(status, response) {
				_this.worksheet_update();
			}));
		});
		$("#auto_republish_checkbox").change(function(e) {
			var command;
			if($("#auto_republish_checkbox").prop("checked")) {
				command = _this.worksheet_command("publish?auto_on");
			} else {
				command = _this.worksheet_command("publish?auto_off");
			}
			
			// for some reason, auto is a toggle command
			sagenb.async_request(command, sagenb.generic_callback(function(status, response) {
				_this.worksheet_update();
			}));
		});


		///////// DATA DIALOG //////////
		$("#data_modal ul.nav a").click(function(e) {
			setTimeout(function() {
				if($("#data_modal .tab-pane.active").attr("id") !== "manage_tab") {
					$("#data_modal #upload_button").show();
					$("#data_modal #done_button").hide();
				} else {
					$("#data_modal #upload_button").removeClass("disabled").hide();
					$("#data_modal #done_button").show();
					$("#data_modal input").val("");
					_this.worksheet_update();
				}
			}, 50);
		});

		$("#data_modal #upload_button").click(function(e) {
			if($(this).hasClass("disabled")) return;

			var current_tab = $("#data_modal .tab-pane.active").attr("id");
			$(this).addClass("disabled");

			if(current_tab === "upload_data_file_tab") {
				$("body").append('<iframe name="upload_frame" id="upload_frame" />');
				var upload_frame = $("iframe#upload_frame");
				upload_frame.hide();
				$("#upload_data_file_tab form").submit();
				upload_frame.load(function() {
					if($.trim(upload_frame.text()) !== "") {
						alert(upload_frame.text());
					}
					else {
						$("#manage_tab_button").click();
					}
					upload_frame.detach();
				});
			}
			else if(current_tab === "file_from_url_tab") {
				sagenb.async_request(_this.worksheet_command("datafile_from_url"), sagenb.generic_callback(function(status, response) {
					$("#manage_tab_button").click();
				}), {
					url: $("#data_modal #file_from_url_tab input#url").val(),
					name: $("#data_modal #file_from_url_tab input#name").val()
				});
			}
			else if(current_tab === "new_file_tab") {
				sagenb.async_request(_this.worksheet_command("new_datafile"), sagenb.generic_callback(function(status, response) {
					$("#manage_tab_button").click();
				}), {
					"new": $("#data_modal #new_file_tab input#new").val()
				});
			}
		});
		
		// start the ping interval
		_this.ping_interval_id = window.setInterval(_this.ping_server, _this.server_ping_time);
		
		var load_done_interval = setInterval(function() {
			/* because the cells array is sparse we need this.
			 * it may be easier/faster to use $.grep either way...
			 */
			var numcells = 0;
			
			_this.forEachCell(function(cell) {
				numcells++;
			});
			
			if(numcells > 0 && numcells === $(".cell").length) {
				_this.on_load_done();
				clearInterval(load_done_interval);
			}
		},
			1000
		);
		
		// Setup hotkeys
		/* Notes on hotkeys: these don't work on all browsers consistently
		but they are included in the best case scenario that they are all 
		accepted. */
		$(document).bind("keydown", sagenb.ctrlkey + "+N", function(evt) { _this.new_worksheet(); return false; });
		$(document).bind("keydown", sagenb.ctrlkey + "+S", function(evt) { _this.save(); return false; });
		$(document).bind("keydown", sagenb.ctrlkey + "+W", function(evt) { _this.close(); return false; });
		$(document).bind("keydown", sagenb.ctrlkey + "+P", function(evt) { _this.print(); return false; });
		$(document).bind("keydown", "esc", function(evt) { _this.interrupt_with_confirm(); return false; });
				
		/////// FILE MENU ////////
		$("#new_worksheet").click(_this.new_worksheet);
		$("#save_worksheet").click(_this.save);
		$("#close_worksheet").click(_this.close);
		$("#export_to_file").click(_this.export_worksheet);
		// $("#import_from_file").click(_this.import_worksheet);
		$("#print").click(_this.print);
				
		////////// EVALUATION ///////////
		$("#evaluate_all_cells").click(_this.evaluate_all);
		$("#interrupt").click(_this.interrupt);
		$("#restart_worksheet").click(_this.restart_sage);
		// change system doesn't require event handler here
		$("#hide_all_output").click(_this.hide_all_output);
		$("#show_all_output").click(_this.show_all_output);
		$("#delete_all_output").click(_this.delete_all_output);
	};
};
// the cell object
sagenb.worksheetapp.cell = function(id) {
	/* this allows us to access this cell object from 
	 * inner functions
	 */
	var _this = this;
	
	_this.id = id;
	_this.input = "";
	_this.output = "";
	_this.system = "";
	_this.percent_directives = null;
	
	_this.introspect_state = null;
	_this.is_evaluate_cell = true;
	_this.is_evaluating = false;
	
	_this.codemirror = null;

	_this.worksheet = null;
	
	// this is the id of the interval for checking for new output
	_this.output_check_interval_id;
	
	// the amount of time in millisecs between update checks
	_this.output_check_interval = 250;

	
	// HELPERS
	function get_next_eval_cell() {
		var $nextcell = $("#cell_" + _this.id).parent().next().next();
		while($nextcell.length > 0 && $nextcell.find(".evaluate_cell").length === 0) {
			$nextcell = $nextcell.next().next();
		}
		if($nextcell.length > 0) {
			// we're not the last cell
			var nextcell_id = parseInt($nextcell.find(".evaluate_cell").attr("id").substring(5));
			return _this.worksheet.cells[nextcell_id];
		}
	}

	function get_prev_eval_cell() {
		var $prevcell = $("#cell_" + _this.id).parent().prev().prev();
		while($prevcell.length > 0 && $prevcell.find(".evaluate_cell").length === 0) {
			$prevcell = $prevcell.prev().prev();
		}
		if($prevcell.length > 0) {
			// we're not the last cell
			var prevcell_id = parseInt($prevcell.find(".evaluate_cell").attr("id").substring(5));
			return _this.worksheet.cells[prevcell_id];
		}
	}

	function fixOldjsMath(elem) {
		// mathjax each span with \( \)
		$(elem).find("span.math").each(function(i, element) {
			$(element).html("\\(" + $(element).html() + "\\)");
		});
		
		// mathjax each div with \[ \]
		$(elem).find("div.math").each(function(i, element) {
			$(element).html("\\[" + $(element).html() + "\\]");
		});
	}

	///////////// UPDATING /////////////
	_this.update = function(render_container, auto_evaluate) {
		/* Update cell properties. Updates the codemirror mode (if necessary)
		 * and %hide stuff. Only performs rendering if a render_container is 
		 * given. If auto_evaluate is true and this is an #auto cell, it will
		 * be evaluated.
		 */
		sagenb.async_request(_this.worksheet.worksheet_command("cell_properties"), sagenb.generic_callback(function(status, response) {
			var X = decode_response(response);
			
			// set up all of the parameters
			_this.input = X.input;
			_this.output = X.output;
			_this.system = X.system;
			_this.percent_directives = X.percent_directives;
			
			// it doesn't seem right to have a different property here
			// it seems like X.output is sufficient
			if($.trim(X.output_html) !== "") {
				_this.output = X.output_html;
			}
			if($.trim(X.output_wrapped) !== "") {
				_this.output = X.output_wrapped;
			}
					
			_this.is_evaluate_cell = (X.type === "evaluate") ? true : false;
			
			// change the codemirror mode
			_this.update_codemirror_mode();
			
			if(render_container) {
				_this.render(render_container);
			}
			
			// if it's a %hide cell, hide it
			if(_this.is_hide()) {
				$("#cell_" + _this.id + " .input_cell").addClass("input_hidden");
			}
			
			// if it's an auto cell, evaluate
			if(auto_evaluate && _this.is_auto()) {
				_this.evaluate();
			}
		}),
		{
			id: _this.id
		});
	};
	_this.get_codemirror_mode = function() {
		/* This is a utility function to get the correct
		 * CodeMirror mode which this cell should be 
		 * rendered in.
		 */
		if(_this.system !== "" && _this.system !== null) {
			// specific cell system
			return system_to_codemirror_mode(_this.system);
		} else {
			// fall through to worksheet system
			return system_to_codemirror_mode(_this.worksheet.system);
		}
	}
	_this.update_codemirror_mode = function() {
		if(_this.codemirror) {
			if(_this.get_codemirror_mode() !== _this.codemirror.getOption("mode")) {
				// change the codemirror mode
				_this.codemirror.setOption("mode", _this.get_codemirror_mode());
			}
		}
	}
	
	//////// RENDER //////////
	_this.render = function(container) {
		if(_this.is_evaluate_cell) {
			// it's an evaluate cell
		
			// render into the container
			$(container).html("<div class=\"cell evaluate_cell\" id=\"cell_" + _this.id + "\">" +
									"<div class=\"input_cell\"></div>" +
									"<div class=\"evaluate_button_container\">" + 
										"<button class=\"btn evaluate_button\" type=\"button\">" + gettext("Evaluate") + "</button>" +
									"</div>" +
								"</div> <!-- /cell -->");
			
			// Bind the evaluate button
			$(container).find(".evaluate_button").click(_this.evaluate);

			//set up extraKeys object
			/* because of some codemirror or chrome bug, we have to
			 * use = new Object(); instead of = {}; When we use = {};
			 * all of the key events are automatically passed to codemirror.
			 */
			var extrakeys = new Object();
			
			// set up autocomplete. we may want to use tab
			//extrakeys[sagenb.ctrlkey + "-Space"] = "autocomplete";
			extrakeys[sagenb.ctrlkey + "-Space"] = function(cm) {
				_this.introspect();
			};
			
			extrakeys["Tab"] = function(cm) {
				if(!_this.introspect() && cm.getCursor(true).line != cm.getCursor().line) {
					CodeMirror.commands.indentMore(cm);
				}
			};
			
			extrakeys["Shift-Tab"] = "indentLess";
			
			// backspace handler
			extrakeys["Backspace"] = function(cm) {
				// check if it is empty
				_this.cancel_introspect();

				var key, count = 0;
				for(key in _this.worksheet.cells) count++;
				if(cm.getValue() === "" && count > 0 && !($("body").hasClass("single_cell_mode"))) {
					// it's empty and not the only one -> delete it
					_this.delete();
				} else {
					// not empty -> pass to the default behaviour
					throw CodeMirror.Pass;
				}
			};

			extrakeys["Up"] = function(cm) {
				var c = cm.getCursor();
				if(c.line === 0) {
					var prevcell = get_prev_eval_cell();
					if(prevcell) {
						_this.cancel_introspect();

						_this.blur();
						prevcell.focus();
					}
				} else {
					throw CodeMirror.Pass;
				}
			};
			
			extrakeys["Down"] = function(cm) {
				var c = cm.getCursor();
				if(c.line === cm.getValue().split("\n").length - 1) {
					var nextcell = get_next_eval_cell();
					if(nextcell) {
						_this.cancel_introspect();

						_this.blur();
						nextcell.focus();
					}
				} else {
					throw CodeMirror.Pass;
				}
			};

			extrakeys["Shift-Enter"] = function(cm) {
				_this.evaluate();
			};
			
			extrakeys[sagenb.ctrlkey + "-N"] = function(cm) {
				_this.worksheet.new_worksheet();
			};
			extrakeys[sagenb.ctrlkey + "-S"] = function(cm) {
				_this.worksheet.save();
			};
			extrakeys[sagenb.ctrlkey + "-W"] = function(cm) {
				_this.worksheet.close();
			};
			extrakeys[sagenb.ctrlkey + "-P"] = function(cm) {
				_this.worksheet.print();
			};
			
			extrakeys["F1"] = function() {
				_this.worksheet.open_help();
			};
			
			// create the codemirror
			_this.codemirror = CodeMirror($(container).find(".input_cell")[0], {
				value: _this.input,
				
				/* some of these may need to be settings */
				indentWithTabs: false,
				tabSize: 4,
				indentUnit: 4,
				lineNumbers: $("#line_numbers_checkbox").prop("checked"),
				matchBrackets: true,
				
				readOnly: (_this.worksheet.published_mode ? true : false),

				mode: _this.get_codemirror_mode(),
				
				/* autofocus messes up when true */
				autofocus: false,

				extraKeys: extrakeys
			});

			_this.codemirror.on("change", function(cm, chg) {
				if(chg.text[0] === "(") {
					_this.introspect();
				}
				else if(chg.text[0] === ")") {
					_this.cancel_introspect();
				}
			});
			
			_this.codemirror.on("focus", function() {
				_this.worksheet.current_cell_id = _this.id;
				
				$(".cell").removeClass("current_cell");
				$("#cell_" + _this.id).addClass("current_cell");
				
				// unhide if hidden
				$("#cell_" + _this.id + " .input_cell").removeClass("input_hidden");
			});

			_this.codemirror.on("blur", function() {
				if(!($("body").hasClass("single_cell_mode"))) {
					$("#cell_" + _this.id).removeClass("current_cell");
				}
				
				if(_this.input !== _this.codemirror.getValue()) {
					// the input has changed since the user focused
					// so we send it back to the server
					_this.send_input();
				}

				// update cell properties without rendering
				_this.update();
			});

			// render the output
			_this.render_output();
		}
		else {
			// it's a text cell
			if(_this.worksheet.published_mode) {
				$(container).html("<div class=\"cell text_cell\" id=\"cell_" + _this.id + "\">" + 
										"<div class=\"view_text\">" + _this.input + "</div>" + 
									"</div> <!-- /cell -->");

				var $_this = $("#cell_" + _this.id);
			
				// MathJax the text
				MathJax.Hub.Queue(["Typeset", MathJax.Hub, $_this.find(".view_text")[0]]);
			}
			else {
				$(container).html("<div class=\"cell text_cell\" id=\"cell_" + _this.id + "\">" + 
										"<div class=\"view_text\">" + _this.input + "</div>" + 
										"<div class=\"edit_text\">" + 
											"<textarea name=\"text_cell_textarea_" + _this.id + "\" id=\"text_cell_textarea_" + _this.id + "\">" + _this.input + "</textarea>" + 
											"<div class=\"buttons\">" + 
												"<button class=\"btn btn-danger delete_button pull-left\">Delete</button>" + 
												"<button class=\"btn cancel_button\">Cancel</button>" + 
												"<button class=\"btn btn-primary save_button\">Save</button>" + 
											"</div>" + 
										"</div>" + 
									"</div> <!-- /cell -->");
				
				
				// init tinyMCE
				// we may want to customize the editor some to include other buttons/features
				tinyMCE.init({
					mode: "exact",
					elements: ("text_cell_textarea_" + _this.id),
					
					plugins: "advlist,inlinepopups,lists,media,paste,searchreplace,table,autolink,",

					theme : "advanced",
					theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_buttons1 : "formatselect,fontselect,fontsizeselect,bold,italic,underline,strikethrough,forecolor,backcolor,|,bullist,numlist,|,undo,redo,search,pastetext,pasteword",
					theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,justifyfull,outdent,indent,|,charmap,|,table,tablecontrols,|,code,|,link,image,media,unlink",
					theme_advanced_buttons3 : "",
					theme_advanced_resizing : true,
					theme_advanced_show_current_color: true,
					theme_advanced_default_background_color : "#FFCC99",

					width: "100%",
					height: "300"
				});
				
				var $_this = $("#cell_" + _this.id);
				
				// MathJax the text
				MathJax.Hub.Queue(["Typeset", MathJax.Hub, $_this.find(".view_text")[0]]);
				
				$_this.dblclick(function(e) {
					if(!_this.is_evaluate_cell) {
						// lose any selection that was made
						if (window.getSelection) {
							window.getSelection().removeAllRanges();
						} else if (document.selection) {
							document.selection.empty();
						}
						
						// add the edit class
						$("#cell_" + _this.id).addClass("edit")
											  .addClass("current_cell");
					}
				});
				
				$_this.find(".delete_button").click(_this.delete);
				
				$_this.find(".cancel_button").click(function(e) {
					// get tinymce instance
					var ed = tinyMCE.get("text_cell_textarea_" + _this.id);
					
					// revert the text
					ed.setContent(_this.input);
					
					// remove the edit class
					$("#cell_" + _this.id).removeClass("edit")
										  .removeClass("current_cell");
				});
				
				$_this.find(".save_button").click(function(e) {
					// get tinymce instance
					var ed = tinyMCE.get("text_cell_textarea_" + _this.id);
					
					// send input
					_this.send_input();
					
					// update the cell
					$_this.find(".view_text").html(_this.input);
					
					// MathJax the text
					MathJax.Hub.Queue(["Typeset", MathJax.Hub, $_this.find(".view_text")[0]]);
					
					// remove the edit class
					$("#cell_" + _this.id).removeClass("edit")
										  .removeClass("current_cell");
				});
			}
		}
	};
	_this.render_output = function(stuff_to_render) {
		/* Renders stuff_to_render as the cells output, 
		 * if given. If not, then it renders _this.output.
		 */
		
		// don't do anything for text cells
		if(!_this.is_evaluate_cell) return;
		
		stuff_to_render = stuff_to_render ? stuff_to_render : _this.output;
		stuff_to_render = $.trim(stuff_to_render);
		
		// if no output then don't do anything else
		if(!stuff_to_render) {
			// take the output off the dom
			$("#cell_" + _this.id + " .output_cell").detach();
			return;
		}

		var $output_cell = $("#output_" + _this.id);
		if($output_cell.length === 0) {
			$output_cell = $("<div class=\"output_cell\" id=\"output_" + _this.id + "\"></div>")
				.appendTo("#cell_" + _this.id);
		}

		// Interact constants.  See interact.py and related files.
		// Present in wrapped output, forces re-evaluation of ambient cell.
		var INTERACT_RESTART = '<!--__SAGE_INTERACT_RESTART__-->';
		// Delimit updated markup.
		var INTERACT_START = '<!--__SAGE__START-->';
		var INTERACT_END = '<!--__SAGE__END-->';

		if(stuff_to_render.indexOf(INTERACT_RESTART) > -1) {
			_this.evaluate();
			return;
		}

		var istart = stuff_to_render.indexOf(INTERACT_START);
		var iend = stuff_to_render.indexOf(INTERACT_END);
		if(istart > -1 && iend > -1) {
			if(_this.has_interact()) {
				// update interact
				stuff_to_render = stuff_to_render.slice(istart + INTERACT_START.length, iend);
				$("#cell-interact-" + _this.id).html(stuff_to_render);
			}
			else {
				// start interact
				$output_cell.html(stuff_to_render);
				_this.evaluate_interact({}, 1);
			}
		}
		else {
			// create the .output_cell div
			$output_cell.html(stuff_to_render);
		}

		if($output_cell.find(".jmol_instance").length > 0) {
			// TODO Is it possible to have multiple Jmol's in a single cell output?
			// If so, we would need to call jmol_inline.init(), etc on all of them
			var jmol_instance = new sagenb.jmol.jmol_inline($output_cell.find(".jmol_instance"));
			jmol_instance.init();
		}
		
		if($output_cell.find(".math").length > 0 ||
		   $output_cell.find("script[type='math/tex']").length > 0) {
			/* \( \) is for inline and \[ \] is for block mathjax */
			
			if($output_cell.contents().length === 1) {
				// only one piece of math, make it big
				/* using contents instead of children guarantees that we
				 * get all other types of nodes including text and comments.
				 */
				$output_cell.html("\\[" + $output_cell.find("script[type='math/tex']").html() + "\\]");
			}
			else {
				fixOldjsMath($output_cell);
			}

			MathJax.Hub.Queue(["Typeset", MathJax.Hub, $output_cell[0]]);
		}
	};
	
	////// FOCUS/BLUR ///////
	_this.focus = function() {
		if(_this.codemirror) {
			_this.codemirror.focus();
		} else {
			// edit the tinyMCE
			$("#cell_" + _this.id).dblclick();
			tinyMCE.execCommand('mceFocus', false, "text_cell_textarea_" + _this.id);
		}
	}

	_this.blur = function() {
		if(_this.codemirror) {
			_this.codemirror.getInputField().blur();
		}
	}
	
	_this.is_focused = function() {
		return _this.worksheet.current_cell_id === _this.id;
	};
	_this.is_auto = function() {
		return (_this.percent_directives && $.inArray("auto", _this.percent_directives) >= 0);
	};
	_this.is_hide = function() {
		return (_this.percent_directives && $.inArray("hide", _this.percent_directives) >= 0);
	};

	_this.has_interact = function() {
		return $("#cell-interact-" + _this.id).length > 0;
	};

	///// POPOVER /////
	_this.hide_popover = function() {
		$(".tooltip_root").popover("hide");
		$(".tooltip_root").detach();
	}
	_this.show_popover = function(content) {
		_this.hide_popover();
		var tooltip_root = $("<div />").addClass("tooltip_root").appendTo("body");
		var pos = _this.codemirror.cursorCoords();
		tooltip_root.css({
			"position": "absolute",
			"left": pos.left,
			"top": pos.bottom
		});

		tooltip_root.popover({
			placement: "bottom",
			trigger: "manual",
			content: content
		});

		tooltip_root.popover("show");
		fixOldjsMath($(".popover"));
		MathJax.Hub.Queue(["Typeset", MathJax.Hub, $(".popover")[0]]);

		var safety = 50;
		var off = $(".popover-inner").offset();
		var pop_w = $(".popover-inner").width();
		var window_w = $(window).width();
		if(off.left < safety) {
			$(".popover-inner").offset({left: safety, top: off.top});
		}
		else if(off.left + pop_w > window_w - safety) {
			$(".popover-inner").offset({left: window_w - safety - pop_w, top: off.top});
		}

		$("body").click(function(e) {
			_this.cancel_introspect();
		}).keydown(function(e) {
			if(e.which === 27) {
				// Esc
				_this.cancel_introspect();
			}
		});
	}
	
	/////// EVALUATION //////
	var _evaluate_callback = sagenb.generic_callback(function(status, response) {
		var X = decode_response(response);
		
		// figure out whether or not we are interacting
		// seems like this is redundant
		// X.interact = X.interact ? true : false;
		
		// Something went wrong, e.g., cell id's don't match
		if (X.id !== _this.id) return;

		if (X.command && (X.command.slice(0, 5) === 'error')) {
			// TODO: use a bootstrap error message
			console.log(X, X.id, X.command, X.message);
			return;
		}
		
		// not sure about these commands
		if (X.command === 'insert_cell') {
			// Insert a new cell after the evaluated cell.
			_this.worksheet.new_cell_after(_this.id);
		}

		_this.is_evaluating = true;
		$("#cell_" + _this.id).addClass("running");	
		
		// start checking for output
		_this.check_for_output();
	});

	_this.send_input = function() {
		if(_this.worksheet.published_mode) return;

		// mark the cell as changed
		$("#cell_" + _this.id).addClass("input_changed");
		
		// update the local input property
		if(_this.is_evaluate_cell) {
			_this.input = _this.codemirror.getValue();
		} else {
			// get tinymce instance
			var ed = tinyMCE.get("text_cell_textarea_" + _this.id);
			
			// set input
			_this.input = ed.getContent();
		}
		
		// update the server input property
		sagenb.async_request(_this.worksheet.worksheet_command("eval"), sagenb.generic_callback, {
			save_only: 1,
			id: _this.id,
			input: _this.input
		});
	};
	_this.evaluate = function() {
		if(_this.worksheet.published_mode) return;
		if(!_this.is_evaluate_cell) {
			// we're a text cell
			_this.continue_evaluating_all();
			return;
		}
		
		if(_this.is_evaluating) {
			return;
		}

		_this.cancel_introspect();
		_this.set_output_loading();

		var nextcell = get_next_eval_cell();
		if(nextcell) {
			nextcell.focus();
		}

		// we're an evaluate cell
		sagenb.async_request(_this.worksheet.worksheet_command("eval"), _evaluate_callback, {
			// 0 = false, 1 = true this needs some conditional
			newcell: 0,
			
			id: toint(_this.id),
			
			/* It's necessary to get the codemirror value because the user
			 * may have made changes and not blurred the codemirror so the 
			 * changes haven't been put in _this.input
			 */
			input: _this.codemirror.getValue()
		});
	};
	_this.evaluate_interact = function(update, recompute) {
		if(_this.worksheet.published_mode) return;
		sagenb.async_request(_this.worksheet.worksheet_command("eval"), _evaluate_callback, {
			id: toint(_this.id),
			interact: 1,
			variable: update.variable || '',
			adapt_number: update.adapt_number || -1,
			value: update.value || '',
			recompute: recompute,
			newcell: 0
		});
	};
	_this.introspect = function() {
		/* Attempts to begin an introspection. Firstly, it splits the input 
		 * according to the cursor position. Then it matches the text before 
		 * the cursor to some regex expression to check which type of 
		 * introspection we are doing. Once it determines the type of introspection,
		 * it stores some properties in the introspect_state variable. If 
		 * there is nothing to introspect, it returns false. Otherwise, 
		 * it executes the introspection and returns true. Handling of the 
		 * introspection result is done in the check_for_output function.
		 */
		
		if(!_this.is_evaluate_cell) return false;
		if(_this.worksheet.published_mode) return false;
		if($(".tooltip_root").length > 0) return false;
		if(_this.is_evaluating) return false;
		// if(_this.introspect_state) return false;
		
		/* split up the text cell and get before and after */
		var before = "";
		var after = "";
		
		var pos = _this.codemirror.getCursor(false);
		var lines = _this.codemirror.getValue().split("\n");
		
		before += lines.slice(0, pos.line).join("\n");
		if(pos.ch > 0) {
			if(pos.line > 0) {
				before += "\n";
			}
			before += lines[pos.line].substring(0, pos.ch);
		}
		
		after += lines[pos.line].substring(pos.ch);
		if(pos.line < lines.length - 1) {
			after += "\n";
			after += lines.slice(pos.line + 1).join("\n");
		}
		
		
		/* set up introspection state */
		_this.introspect_state = {};
		_this.introspect_state.before_replacing_word = before;
		_this.introspect_state.after_cursor = after;
		_this.introspect_state.previous_value = _this.codemirror.getValue();
		
		/* Regexes */
		var command_pat = "([a-zA-Z_][a-zA-Z._0-9]*)$";
		var function_pat = "([a-zA-Z_][a-zA-Z._0-9]*)\\([^()]*$";
		try {
			command_pat = new RegExp(command_pat);
			function_pat = new RegExp(function_pat);
		} catch (e) {}
		
		m = command_pat.exec(before);
		f = function_pat.exec(before);
		
		if (before.slice(-1) === "?") {
			// We're starting with a docstring or source code.
			_this.introspect_state.docstring = true;
		} else if (m) {
			// We're starting with a list of completions.
			_this.introspect_state.code_completion = true;
			_this.introspect_state.replacing_word = m[1];
			_this.introspect_state.before_replacing_word = before.substring(0, before.length - m[1].length);
		} else if (f !== null) {
			// We're in an open function paren -- give info on the
			// function.
			before = f[1] + "?";
			// We're starting with a docstring or source code.
			_this.introspect_state.docstring = true;
		} else {
			// Just a tab.
			return false;
		}
		
		sagenb.async_request(_this.worksheet.worksheet_command("introspect"), sagenb.generic_callback(function(status, response) {
			/* INTROSPECT CALLBACK */
			_this.is_evaluating = true;
			
			// start checking for output
			_this.check_for_output();
		}),
		
		/* REQUEST OPTIONS */
		{
			id: toint(_this.id),
			before_cursor: before,
			after_cursor: after
		});
		
		return true;
	};
	_this.cancel_introspect = function() {
		_this.hide_popover();
		_this.introspect_state = null;
	};
	_this.check_for_output = function() {
		function stop_checking() {
			_this.is_evaluating = false;
			
			// mark the cell as done
			$("#cell_" + _this.id).removeClass("running");	
			
			// clear interval
			_this.output_check_interval_id = window.clearInterval(_this.output_check_interval_id);
		}
		
		function do_check() {
			sagenb.async_request(_this.worksheet.worksheet_command("cell_update"), sagenb.generic_callback(function(status, response) {
				if(response === "") {
					// empty response, try again after a little bit
					// setTimeout(_this.check_for_output, 500);
					return;
				}
				
				var X = decode_response(response);
				
				if(X.status === "e") {
					// there was an error, stop checking
					_this.worksheet.show_connection_error();
					stop_checking();
					return;
				}

				// check if introspect was cancelled
				if(X.introspect_output && $.trim(X.introspect_output).length > 0 && _this.introspect_state == null) {
					// introspect cancelled
					stop_checking();
					return;
				}
				
				if(X.status === "d") {
					// evaluation done
					stop_checking();
					
					if(X.new_input !== "") {
						// if the editor has changed, re-introspect
						// This is buggy
						/*if(_this.codemirror.getValue() !== _this.introspect_state.previous_value) {
							setTimeout(_this.introspect, 50);
							return;
						}*/
						
						// update the input
						_this.input = X.new_input;
						
						// update codemirror/tinymce
						if(_this.is_evaluate_cell) {
							_this.codemirror.setValue(_this.input);
							
							// here we need to set the new cursor position if 
							// we are in introspect
							if(_this.introspect_state) {
								var after_lines = _this.introspect_state.after_cursor.split("\n");
								var val_lines = _this.codemirror.getValue().split("\n");
								
								var pos = {};
								pos.line = val_lines.length - after_lines.length;
								pos.ch = val_lines[pos.line].length - after_lines[0].length;
								
								_this.codemirror.setCursor(pos);
							}
						}
					}
					
					// introspect
					if(X.introspect_output && $.trim(X.introspect_output).length > 0) {
						if(_this.introspect_state.code_completion) {
							// open codemirror simple hint
							var editor = _this.codemirror;

							/* stolen from simpleHint */
							// We want a single cursor position.
							// if (editor.somethingSelected()) return;
							
							//var result = getHints(editor);
							//if (!result || !result.list.length) return;
							var completions = $.trim(X.introspect_output).split("\n");
							
							/* Insert the given completion str into the input */
							function insert(str) {
								// we can take this out in the next release of CodeMirror
								str = str.replace("\r", "");

								var newpos = {};
								var lines = _this.introspect_state.before_replacing_word.split("\n");
								newpos.line = lines.length - 1;
								newpos.ch = lines[lines.length - 1].length + str.length;
								
								editor.setValue(_this.introspect_state.before_replacing_word + str + _this.introspect_state.after_cursor);
								
								editor.setCursor(newpos);
							}
							
							// When there is only one completion, use it directly.
							// TODO we can't do return here since more commands come after introspection stuff
							if (completions.length === 1) {insert(completions[0]); return true;}
							
							// Build the select widget
							/* Because this code is stolen directly from simple-hint.js
							* it does not use jQuery for any of the DOM manipulation.
							*/
							var complete = document.createElement("div");
							complete.className = "CodeMirror-completions";
							var sel = complete.appendChild(document.createElement("select"));
							// Opera doesn't move the selection when pressing up/down in a
							// multi-select, but it does properly support the size property on
							// single-selects, so no multi-select is necessary.
							if (!window.opera) sel.multiple = true;
							for (var i = 0; i < completions.length; ++i) {
								var opt = sel.appendChild(document.createElement("option"));
								opt.appendChild(document.createTextNode(completions[i]));
							}
							sel.firstChild.selected = true;
							sel.size = Math.min(10, completions.length);
							var pos = editor.cursorCoords();
							complete.style.left = pos.left + "px";
							complete.style.top = pos.bottom + "px";
							document.body.appendChild(complete);
							// If we're at the edge of the screen, then we want the menu to appear on the left of the cursor.
							var winW = window.innerWidth || Math.max(document.body.offsetWidth, document.documentElement.offsetWidth);
							if(winW - pos.left < sel.clientWidth)
								complete.style.left = (pos.left - sel.clientWidth) + "px";
							// Hack to hide the scrollbar.
							if (completions.length <= 10)
								complete.style.width = (sel.clientWidth - 1) + "px";

							
							/* Close the completions menu */
							var done = false;
							function close() {
								if (done) return;
								done = true;
								complete.parentNode.removeChild(complete);
							}
							
							/* Pick and insert the currently highlighted completion */
							function pick() {
								insert(completions[sel.selectedIndex]);
								close();
								setTimeout(function(){editor.focus();}, 50);
							}
							
							CodeMirror.on(sel, "blur", close);
							CodeMirror.on(sel, "keydown", function(event) {
								var code = event.keyCode;
								// Enter
								if (code === 13) {CodeMirror.e_stop(event); pick();}
								
								// Escape
								else if (code === 27) {CodeMirror.e_stop(event); close(); editor.focus();}
								
								// Backspace
								else if (code === 8) {
									close();
									editor.focus();
									editor.triggerOnKeyDown(event);
								}
								
								// Everything else besides up/down
								else if (code !== 38 && code !== 40) {
									close(); editor.focus();
									
									// Pass the event to the CodeMirror instance so that it can handle things like backspace properly.
									editor.triggerOnKeyDown(event);
									
									// This is buggy
									// setTimeout(_this.introspect, 50);
								}
							});
							CodeMirror.on(sel, "dblclick", pick);

							sel.focus();
							// Opera sometimes ignores focusing a freshly created node
							if (window.opera) setTimeout(function(){if (!done) sel.focus();}, 100);
							//return true;
						}
						else {
							// docstring
							_this.show_popover($.trim(X.introspect_output));
						}
					}

					// update the output
					_this.output = X.output;
					
					// it doesn't seem right to have a different property here
					// it seems like X.output is sufficient
					if($.trim(X.output_html) !== "") {
						_this.output = X.output_html;
					}
					if($.trim(X.output_wrapped) !== "") {
						_this.output = X.output_wrapped;
					}
					
					// render to the DOM
					_this.render_output();
					
					// EVALUATE ALL STUFF
					_this.continue_evaluating_all();
				}
			}),
				{
					id: _this.id
				}
			);
		}

		// clear interval
		_this.output_check_interval_id = window.clearInterval(_this.output_check_interval_id);
		
		// start checking
		_this.output_check_interval_id = window.setInterval(do_check, _this.output_check_interval);
	};
	
	_this.continue_evaluating_all = function() {
		if(_this.worksheet.published_mode) return;
		if(_this.worksheet.is_evaluating_all) {
			// get the next cell
			var nextcell = get_next_eval_cell();

			if(nextcell) {
				// evaluate
				nextcell.evaluate();
			} else {
				// we're the last cell -> stop evaluating all
				_this.worksheet.is_evaluating_all = false;
			}
		}
	}
	
	/////// OUTPUT ///////
	_this.delete_output = function() {
		if(_this.worksheet.published_mode) return;
		// TODO we should maybe interrupt the cell if its running here
		sagenb.async_request(_this.worksheet.worksheet_command('delete_cell_output'), sagenb.generic_callback(function(status, response) {
			_this.output = "";
			_this.render_output();
		}), {
			id: toint(_this.id)
		});
	};
	
	_this.set_output_loading = function() {
		if(_this.worksheet.published_mode) return;
		_this.render_output("<div class=\"progress progress-striped active\" style=\"width: 25%; margin: 0 auto;\">" + 
									"<div class=\"bar\" style=\"width: 100%;\"></div>" + 
								"</div>");
	};
	_this.set_output_hidden = function() {
		if(_this.worksheet.published_mode) return;
		if($("#cell_" + _this.id + " .output_cell").length > 0) {
			_this.render_output("<hr>");
		}
	}
	_this.set_output_visible = function() {
		if(_this.worksheet.published_mode) return;
		_this.render_output();
	}
	_this.has_input_hide = function() {
		// connect with Cell.percent_directives
		return _this.input.substring(0, 5) === "%hide";
	};
	
	_this.delete = function() {
		if(_this.worksheet.published_mode) return;
		if(_this.is_evaluating) {
			// interrupt
			sagenb.async_request(_this.worksheet.worksheet_command('interrupt'));
		}

		var prevcell = get_prev_eval_cell();
		if(prevcell) {
			prevcell.focus();
		}
		
		sagenb.async_request(_this.worksheet.worksheet_command('delete_cell'), sagenb.generic_callback(function(status, response) {
			X = decode_response(response);
			if(X.command === "ignore") return;
			
			delete _this.worksheet.cells[_this.id];
			
			$("#cell_" + _this.id).parent().next().detach();
			$("#cell_" + _this.id).parent().detach();
		}), {
			id: toint(_this.id)
		});
	};
};
